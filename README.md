**Descrizione**

Nell'ambito della riusabilità, sono stati selezionati cinque modelli di pagina e otto tipi di contenuto del portale del Comune di Cagliari da poter inserire in altri progetti per essere riutilizzati all'interno di altri portali.

**Installazione**

Installare ponmetroca-whitelabel dalla repository git di Entando nella propria macchina locale ed eseguire il comando maven dalla root del progetto
`mvn clean install -DskipTests`

Creare un progetto ex novo con il comando `mvn archetype:generate -Dfilter=entando`. Seguire le istruzioni di installazione e scegliere di creare una Entando web App (Entando sample web app Archetype: an agile, modern and user-centric open source web app like platform.) con versione 5.3.1 e nome 'miocomune'.

**Utilizzo**

Entrare nella cartella del proprio progetto ed eseguire il comando `mvn clean jetty:run` per testare che il portale vuoto che abbiamo appena istanziato stia funzionando. Se il portale funziona correttamente, possiamo installare gli elementi *white label* desiderati nel nostro progetto.

Entriamo nella cartella ‘ponmetroca-whitelabel’ ed incolliamo all’interno del file ‘POM.XML’ le *dependency* per i modelli di contenuto e modelli di pagina che desideriamo inserire nel proprio progetto. Di seguito il codice delle varie *dependency* disponibili.

Modelli di contenuto:
```
<dependency>
	<artifactId>entando-content-whitelabel-luogo</artifactId>
	<groupId>org.entando.entando.bundles.contents</groupId>
    <version>${entando.version}</version>
    <type>war</type>
</dependency>
<dependency>
	<artifactId>entando-content-whitelabel-news</artifactId>
	<groupId>org.entando.entando.bundles.contents</groupId>
	<version>${entando.version}</version>
    <type>war</type>
    </dependency>
<dependency>
	<artifactId>entando-content-whitelabel-argomento</artifactId>
	<groupId>org.entando.entando.bundles.contents</groupId>
    <version>${entando.version}</version>
    <type>war</type>
</dependency>
<dependency>
	<artifactId>entando-content-whitelabel-documento</artifactId>
	<groupId>org.entando.entando.bundles.contents</groupId>
    <version>${entando.version}</version>
    <type>war</type>
</dependency>
<dependency>
	<artifactId>entando-content-whitelabel-evento</artifactId>	
	<groupId>org.entando.entando.bundles.contents</groupId>
    <version>${entando.version}</version>
    <type>war</type>
</dependency>
<dependency>
	<artifactId>entando-content-whitelabel-organizzazione</artifactId>
	<groupId>org.entando.entando.bundles.contents</groupId>
	<version>${entando.version}</version>
	<type>war</type>
</dependency>
<dependency>
	<artifactId>entando-content-whitelabel-persona</artifactId>
	<groupId>org.entando.entando.bundles.contents</groupId>
	<version>${entando.version}</version>
	<type>war</type>
</dependency>
<dependency>
	<artifactId>entando-content-whitelabel-servizio</artifactId>
	<groupId>org.entando.entando.bundles.contents</groupId>
	<version>${entando.version}</version>
	<type>war</type>
</dependency>
```
Modelli di pagina:
```
<dependency>
	<artifactId>entando-page-whitelabel-dettaglio</artifactId>
	<groupId>org.entando.entando.bundles.page</groupId>
	<version>${entando.version}</version>
	<type>war</type>
</dependency>
<dependency>
	<artifactId>entando-page-whitelabel-argomento</artifactId>
	<groupId>org.entando.entando.bundles.page</groupId>
	<version>${entando.version}</version>
	<type>war</type>
</dependency>
<dependency>
	<artifactId>entando-page-whitelabel-areapersonale</artifactId>
	<groupId>org.entando.entando.bundles.page</groupId>
	<version>${entando.version}</version>
	<type>war</type>
</dependency>
<dependency>
	<artifactId>entando-page-whitelabel-sezioniprincipali</artifactId>
	<groupId>org.entando.entando.bundles.page</groupId>
	<version>${entando.version}</version>
	<type>war</type>
</dependency>
```

I nuovi elementi installati saranno ora visibili sul nostro progetto. I modelli di contenuto saranno visibili dell'admin console del progetto nel tab CMS sotto la voce ‘content types’. Mentre sarà possibile vedere i modelli di pagina dal tab UX Patten sotto la voce ‘page models’.

Una volta completata l’inclusione dei nostri elementi *white label*, possiamo avere a che fare con due tipi di scenari:

1. il nuovo progetto ‘miocomune’ è vuoto ed è senza pagine. In questo caso si creeranno le nuove pagine direttamente con gli id che il footer si aspetta di default:
		 
	 - 'amministrazione_trasparente'
	 - 'posta_elettronica_certificata'
	 - 'organizzazione'
	 - 'scrivi_comune'
	 - 'rubrica'
	 - 'rss'

2.  le pagine referenziate nel *footer* esistono già, ma con altro codice. Qualora si stiano utilizzando degli elementi già esistenti, sarà necessario modificare il fragment di riferimento (‘cagliari_template_footer_info’) variando gli assign con i codici delle pagine esistenti nel proprio progetto. 
	L’elenco dei codici di pagina referenziati nel footer sono:
	```
	<#assign ammTraspPageIdVar='amministrazione_trasparente'>
	<#assign certEmailPageIdVar='posta_elettronica_certificata'>
	<#assign organizzazionePageIdVar='organizzazione'>
	<#assign scriviComunePageIdVar='scrivi_comune'>
	<#assign rubricaDipendentiPageIdVar='rubrica'>
	<#assign rssPageIdVar='rss'>
	```

	Se, ad esempio, nel progetto locale la pagina di amministrazione trasparente ha codice 'ammTrasp', la relativa dichiarazione deve essere modificata in `<#assign ammTraspPageIdVar='ammTrasp'>`

Nel caso la pagina di organizzazione visualizzi i dati tramite contenuto del CMS, bisogna specificare qual è l'id del contenuto (l’assegnamento è al momento commentato, sarà necessario rimuovere il commento per renderlo funzionante). Ad esempio se il contenuto relativo all’organizzazione è CNG117, la riga deve diventare: 
`<#assign organizzazioneContentIdVar='CNG117'>`

Lo stesso procedimento si deve applicare nel ‘cagliari_template_head’. Una volta ricercato il fragment è obbligatorio cambiare il valore di default della variabile con il nome della propria webApp: `<#assign webappNameVar='miocomune'>`. Ad esempio, se il mio progetto è raggiungibile all’indirizzo "localhost:8080/villamarina", la variabile deve essere così definita: `<#assign webappNameVar='villamarina'>`.

Un ultimo *fragment* da ricercare e modificare è 'cagliari_template_search_modal'. Questo *fragment* in uso all’*header* costruisce un link alla pagina individuata tramite il codice del widget che mostra i risultati di una ricerca; il codice di default del widget è 'search_result', quindi, qualora si volesse personalizzare il codice del widget, è necessario modificare il seguente assigment `<#assign searchResultWidgetCode='search_result'>` in `<#assign searchResultWidgetCode='custom_search_result'>` dove 'custom_search_result' è il codice del widget modificato.


Per maggiori approfondimenti sulla piattaforma Entando visitate: https://dev.entando.org che contiene tutte le risorse ed informazioni dettagliate per gli sviluppatori.