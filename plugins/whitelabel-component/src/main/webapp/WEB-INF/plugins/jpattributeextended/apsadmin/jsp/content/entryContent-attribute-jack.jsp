<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="/aps-core" prefix="wp" %>
<%@ taglib uri="/apsadmin-core" prefix="wpsa" %>
<%@ taglib prefix="wpsf" uri="/apsadmin-form" %>
<%@ taglib prefix="wlwpsa" uri="/comuneCagliari-whitelable-apsadmin" %>

<s:if test="%{#attribute.type == 'EnumeratorMapPage' || #attribute.type == 'EnumeratorMapCategory' || #attribute.type == 'MultiEnumeratorMapCategory'}">
    <s:if test="null != #attribute.description">
        <s:set var="attributeLabelVar" value="#attribute.description" />
    </s:if>
    <s:else>
        <s:set var="attributeLabelVar" value="#attribute.name" />
    </s:else>
    
    <label class="col-sm-2 control-label">
        <s:property value="#attributeLabelVar" />
        <s:include value="/WEB-INF/apsadmin/jsp/entity/modules/include/attributeInfo.jsp" />
    </label>
    <div class="col-sm-10">
        <div class="input-group">            
            <s:include value="/WEB-INF/apsadmin/jsp/entity/modules/enumeratorMapAttribute.jsp" />
            <s:if test="%{#attribute.type == 'MultiEnumeratorMapCategory'}">
                <span class="input-group-btn" style="vertical-align: top">
                    <s:set var="currentValueVar" value="%{#attributeTracer.getFormFieldName(#attribute)}" />
                    <wpsa:actionParam action="addMultiEnumMapCategory" var="actionNameVar" >
                        <wpsa:actionSubParam name="categoryParamName" value="%{#currentValueVar}" />
                        <wpsa:actionSubParam name="parentAttributeName" value="%{#parentAttribute.name}" />
                        <wpsa:actionSubParam name="attributeName" value="%{#attribute.name}" />
                        <wpsa:actionSubParam name="elementIndex" value="%{#elementIndex}" />
                    </wpsa:actionParam>
                    <wpsf:submit type="button" action="%{#actionNameVar}" cssClass="btn btn-primary" >&#32;<s:text name="label.add" /></wpsf:submit>
                    </span>
            </s:if>
        </div>
        <s:if test="%{#attribute.type == 'MultiEnumeratorMapCategory'}">

            <s:set var="contentCategoryCodesVar" value="#attribute.values" />
            <s:if test="%{#contentCategoryCodesVar != null && #contentCategoryCodesVar.size() > 0}">
                <ul class="list-inline mt-20">
                    <s:iterator value="#contentCategoryCodesVar" var="contentCategoryCodeVar">
                        <wlwpsa:category key="%{#contentCategoryCodeVar}" var="contentCategoryVar" />
                        <li>
                            <span class="label label-info">
                                <span class="icon fa fa-tag"></span>&#32;
                                <abbr title="<s:property value="%{getFullTitle(#contentCategoryVar, #lang.code)}"/>"><s:property value="%{getShortFullTitle(#contentCategoryVar, #lang.code)}"/></abbr>&#32;
                                <wpsa:actionParam action="removeMultiEnumMapCategory" var="actionName" >
                                    <wpsa:actionSubParam name="parentAttributeName" value="%{#parentAttribute.name}" />
                                    <wpsa:actionSubParam name="attributeName" value="%{#attribute.name}" />
                                    <wpsa:actionSubParam name="elementIndex" value="%{#elementIndex}" />
                                    <wpsa:actionSubParam name="categoryCode" value="%{#contentCategoryCodeVar}" />
                                </wpsa:actionParam>
                                <wpsf:submit type="button" action="%{#actionName}" title="%{getText('label.remove') + ' \"' + getDefaultFullTitle(#contentCategoryVar) + '\"'}" cssClass="btn btn-link">
                                    <span class="pficon pficon-close white"></span>
                                    <span class="sr-only">x</span>
                                </wpsf:submit>
                            </span>
                        </li>
                    </s:iterator>
                </ul>
            </s:if>
        </s:if>
    </div>
</s:if>
<s:if test="%{#attribute.type == 'LinkSearchable'}">    
    <s:if test="%{#parentAttribute != null && #parentAttribute.type == 'Composite'}">        
        <s:include value="/WEB-INF/plugins/jacms/apsadmin/jsp/content/modules/linkAttribute.jsp" />
    </s:if>
    <s:elseif test="%{#masterListAttribute != null && (#masterListAttribute.type == 'Monolist' || #masterListAttribute.type == 'List')}">
        <s:include value="/WEB-INF/plugins/jacms/apsadmin/jsp/content/modules/linkAttribute.jsp" />    
    </s:elseif>    
    <s:else>   
        <label class="col-sm-2 control-label" for="<s:property value="%{#attributeTracer.getFormFieldName(#attribute)}" />">
            <s:property value="#attributeLabelVar" />&#32;
            <s:include value="/WEB-INF/apsadmin/jsp/entity/modules/include/attributeInfo.jsp" />
        </label>
        <div class="col-sm-10">
            <s:include value="/WEB-INF/plugins/jacms/apsadmin/jsp/content/modules/linkAttribute.jsp" />
        </div>
    </s:else>
</s:if>

