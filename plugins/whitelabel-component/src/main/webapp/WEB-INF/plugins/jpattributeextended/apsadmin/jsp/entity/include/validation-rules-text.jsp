<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="wpsf" uri="/apsadmin-form" %>

<fieldset class="col-xs-12">
    <legend><s:text name="label.settings" /></legend>
    <s:if test="#attribute.type == 'EnumeratorMapPage'">
        <s:set var="enumeratorExtractorBeansVar" value="enumeratorMapPageExtractorBeans" />
        <p><s:text name="attribute.setting.enumeratorMapPage.help" /></p>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="codePageItems"><s:text name="label.entity.enumeratorMapPage.codePageItems" />
                <i class="fa fa-asterisk required-icon" style="position: relative; top: -4px; right: 0px"></i>
            </label>
            <div class="col-sm-10">                
                <s:set var="enumeratorItemsFieldTitle" value="'label.entity.enumeratorMapPage.itemsField.title'" />
                <s:set var="enumeratorItemsFieldPlaceholder" value="'label.entity.enumeratorMapPage.itemsField.placeholder'" />
                <wpsf:textfield
                    name="codePageItems" id="codePageItems" cssClass="form-control"
                    title="%{getText(#enumeratorItemsFieldTitle)}"
                    placeholder="%{getText(#enumeratorItemsFieldPlaceholder)}" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="showChildsPage"><s:text name="label.entity.enumeratorMapPage.showChildsPage" /></label>
            <div class="col-xs-10">
                <wpsf:checkbox name="showChildsPage" id="showChildsPage" cssClass="bootstrap-switch" />
            </div>
        </div>
        <wpsf:hidden name="enumeratorExtractorBean" id="enumeratorExtractorBean" value="enumeratorMapPageAttributeExtractorBeans" />
    </s:if>
    <s:elseif test="%{#attribute.type == 'EnumeratorMapCategory' || #attribute.type == 'MultiEnumeratorMapCategory'}">
        <p><s:text name="attribute.setting.enumeratorMapCategory.help" /></p>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="codeCategoryItems"><s:text name="label.entity.enumeratorMapCategory.codeCategoryItems" />
                <i class="fa fa-asterisk required-icon" style="position: relative; top: -4px; right: 0px"></i>
            </label>
            <div class="col-sm-10">                
                <s:set var="enumeratorItemsFieldTitle" value="'label.entity.enumeratorMapCategory.itemsField.title'" />
                <s:set var="enumeratorItemsFieldPlaceholder" value="'label.entity.enumeratorMapCategory.itemsField.placeholder'" />
                <wpsf:textfield
                    name="codeCategoryItems" id="codeCategoryItems" cssClass="form-control"
                    title="%{getText(#enumeratorItemsFieldTitle)}"
                    placeholder="%{getText(#enumeratorItemsFieldPlaceholder)}" />
            </div>
        </div>
        <wpsf:hidden name="enumeratorExtractorBean" id="enumeratorExtractorBean" value="enumeratorMapCategoryAttributeExtractorBeans" />
    </s:elseif>
</fieldset>
