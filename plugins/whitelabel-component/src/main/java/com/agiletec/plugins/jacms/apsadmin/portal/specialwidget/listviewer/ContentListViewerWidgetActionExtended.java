package com.agiletec.plugins.jacms.apsadmin.portal.specialwidget.listviewer;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.agiletec.plugins.jacms.apsadmin.portal.util.LayoutList;

/**
 *
 * @author emarcias
 */
public class ContentListViewerWidgetActionExtended extends ContentListViewerWidgetAction {

    private static final Logger _logger = LoggerFactory.getLogger(ContentListViewerWidgetActionExtended.class);

    public String getModelId2() {
        return _modelId2;
    }

    public void setModelId2(String _modelId2) {
        this._modelId2 = _modelId2;
    }

    public String getLayout() {
        return _layout;
    }

    public void setLayout(String _layout) {
        this._layout = _layout;
    }

    /**
     * Restituisce la lista di Modelli di layout
     * @return La lista dei layout disponibili per il widget lista di contenuti.
     */
    public List<LayoutList> getLayouts() {
        List<LayoutList> lista = new ArrayList<>();
        lista.add(new LayoutList (1,"1 - 1 - 1 / 1 - 1 multiriga"));
        lista.add(new LayoutList (2,"1 - 1 - 1 multiriga"));
        lista.add(new LayoutList (3,"1 - 1 - 1 monoriga"));
        lista.add(new LayoutList (4,"1 - 1 - 2 monoriga"));
        lista.add(new LayoutList (5,"1 - 2 - 2 monoriga"));
        return lista;
    }
    
    private String _modelId2;
    private String _layout;

}
