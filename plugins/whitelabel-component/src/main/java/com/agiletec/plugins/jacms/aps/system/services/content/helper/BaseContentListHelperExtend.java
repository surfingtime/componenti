
package com.agiletec.plugins.jacms.aps.system.services.content.helper;

import com.agiletec.aps.system.common.entity.model.EntitySearchFilter;
import com.agiletec.plugins.jacms.aps.system.services.content.model.Content;

/**
 * Classe che estende BaseContentListHelper
 * In questa estensione è stato modificato solo il metodo getFilters()
 * Tale estensione è stata necessaria per estendere la ricerca all'attributo Monolist il cui NestedAttribute è LinkSearchable
 * @author emarcias
 */
public class BaseContentListHelperExtend extends BaseContentListHelper{
    
    @Override
    public EntitySearchFilter[] getFilters(String contentType, String filtersShowletParam, String langCode) {
        Content contentPrototype = this.getContentManager().createContentType(contentType);
        if (null == filtersShowletParam || filtersShowletParam.trim().length() == 0 || null == contentPrototype) {
            return null;
        }
        
        BaseFilterUtilsExtended dom = new BaseFilterUtilsExtended();        
        return dom.getFilters(contentPrototype, filtersShowletParam, langCode);
    }    
}
