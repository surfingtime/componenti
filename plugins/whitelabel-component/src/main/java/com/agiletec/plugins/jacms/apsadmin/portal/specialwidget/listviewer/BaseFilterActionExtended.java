package com.agiletec.plugins.jacms.apsadmin.portal.specialwidget.listviewer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author emarcias
 */
public class BaseFilterActionExtended extends BaseFilterAction{
    
    private static final Logger _logger = LoggerFactory.getLogger(BaseFilterActionExtended.class);
    
    public String getModelId2() {
        return _modelId2;
    }

    public void setModelId2(String _modelId2) {
        this._modelId2 = _modelId2;
    }

    public String getLayout() {
        return _layout;
    }

    public void setLayout(String _layout) {
        this._layout = _layout;
    }
    
    private String _modelId2;
    
    private String _layout;
    
}
