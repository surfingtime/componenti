package com.agiletec.plugins.jacms.aps.system.services.content.helper;

import com.agiletec.aps.system.common.entity.helper.BaseFilterUtils;
import com.agiletec.aps.system.common.entity.model.EntitySearchFilter;
import com.agiletec.aps.system.common.entity.model.IApsEntity;
import com.agiletec.aps.system.common.entity.model.attribute.AttributeInterface;
import java.util.List;
import java.util.Properties;

/**
 * Classe che estende BaseFilterUtils
 * Questa estensione è stata necessaria per estendere la ricerca all'attributo Monolist il cui NestedAttribute è LinkSearchable
 * @author emarcias
 */
public class BaseFilterUtilsExtended extends BaseFilterUtils {

    @Override
    public EntitySearchFilter[] getFilters(IApsEntity entityPrototype, String filtersString, String langCode) {
        if (null == entityPrototype) {
            return null;
        }
        List<Properties> properties = getFiltersProperties(filtersString);
        EntitySearchFilter[] filters = new EntitySearchFilter[properties.size()];
        for (int i = 0; i < properties.size(); i++) {
            Properties props = properties.get(i);
            EntitySearchFilterExtend filter = (EntitySearchFilterExtend) EntitySearchFilterExtend.getInstance(entityPrototype, props);
            this.attachLangFilter(entityPrototype, (EntitySearchFilter) filter, props, langCode);
            filters[i] = (EntitySearchFilter) filter;
        }
        return filters;
    }

    private void attachLangFilter(IApsEntity entityPrototype, EntitySearchFilter filter, Properties props, String langCode) {
        String filterType = (String) props.get(EntitySearchFilter.FILTER_TYPE_PARAM);
        boolean isAttributeFilter = Boolean.parseBoolean(filterType);
        if (isAttributeFilter) {
            String attributeName = (String) props.get(EntitySearchFilter.KEY_PARAM);
            AttributeInterface attribute = (AttributeInterface) entityPrototype.getAttribute(attributeName);
            if (attribute.isMultilingual()) {
                filter.setLangCode(langCode);
            }
        }
    }
}
