package com.agiletec.plugins.jacms.aps.system.services.content.helper;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.agiletec.aps.system.common.entity.model.EntitySearchFilter;
import com.agiletec.aps.system.common.entity.model.IApsEntity;
import com.agiletec.aps.system.common.entity.model.attribute.AttributeInterface;
import com.agiletec.aps.system.common.entity.model.attribute.BooleanAttribute;
import com.agiletec.aps.system.common.entity.model.attribute.DateAttribute;
import com.agiletec.aps.system.common.entity.model.attribute.ITextAttribute;
import com.agiletec.aps.system.common.entity.model.attribute.MonoListAttribute;
import com.agiletec.aps.system.common.entity.model.attribute.NumberAttribute;
import com.agiletec.aps.system.exception.ApsSystemException;
import org.entando.entando.plugins.jpattributeextended.aps.system.entity.model.LinkSearchableAttribute;

/**
 * Estensione della classe EntitySearchFilter
 * Questa estensione permette di effettuare delle ricerche anche per l'attributo Monolist avente settato al suo interno l'attributo LinkSearchable.
 * @author emarcias
 */
public class EntitySearchFilterExtend<T> extends EntitySearchFilter<T> {

    private static final Logger _logger = LoggerFactory.getLogger(EntitySearchFilterExtend.class);

    protected EntitySearchFilterExtend() {
    }

    public static EntitySearchFilterExtend getInstance(IApsEntity prototype, Properties props) {
        EntitySearchFilterExtend filter = null;
        try {
            String key = props.getProperty(KEY_PARAM);
            String roleName = props.getProperty(ROLE_PARAM);
            if (null == key && null == roleName) {
                return null;
            }
            boolean isAttributeFilter = Boolean.parseBoolean(props.getProperty(FILTER_TYPE_PARAM));
            filter = new EntitySearchFilterExtend();
            boolean isDateAttribute = false;
            if (!isAttributeFilter) {
                filter.setKey(key);
                String dataType = props.getProperty(DATA_TYPE_PARAM);
                if (null == dataType) {
                    dataType = DATA_TYPE_STRING;
                }
                setValues(filter, props, dataType);
            } else {
                AttributeInterface attr = null;
                if (null != key) {
                    attr = (AttributeInterface) prototype.getAttribute(key);
                    filter.setKey(key);
                } else {
                    attr = (AttributeInterface) prototype.getAttributeByRole(roleName);
                    filter.setRoleName(roleName);
                }
                filter.setAttributeFilter(true);
                if (null != attr) {
                    String dataType = null;
                    if (attr instanceof DateAttribute) {
                        dataType = DATA_TYPE_DATE;
                        isDateAttribute = true;
                    } else if (attr instanceof ITextAttribute || attr instanceof BooleanAttribute) {
                        dataType = DATA_TYPE_STRING;
                    } else if (attr instanceof NumberAttribute) {
                        dataType = DATA_TYPE_NUMBER;
                    } else if (attr.getType().equalsIgnoreCase("Monolist") && ((MonoListAttribute) attr).getNestedAttributeType().getType().equalsIgnoreCase("LinkSearchable")) {
                        dataType = DATA_TYPE_STRING;
                    }
                    setValues(filter, props, dataType);
                } else {
                    throw new ApsSystemException("ERROR: Entity type '" + prototype.getTypeCode()
                            + "' and attribute '" + key + "' not recognized");
                }
            }
            if (isDateAttribute) {
                String valueDateDelay = props.getProperty(EntitySearchFilterExtend.VALUE_DATE_DELAY_PARAM);
                filter.setValueDateDelay(null != valueDateDelay ? Integer.valueOf(valueDateDelay) : null);

                String endDateDelay = props.getProperty(EntitySearchFilterExtend.END_DATE_DELAY_PARAM);
                filter.setEndDateDelay(null != endDateDelay ? Integer.valueOf(endDateDelay) : null);

                String startDateDelay = props.getProperty(EntitySearchFilterExtend.START_DATE_DELAY_PARAM);
                filter.setStartDateDelay(null != startDateDelay ? Integer.valueOf(startDateDelay) : null);
            }
            String order = props.getProperty(EntitySearchFilterExtend.ORDER_PARAM);
            filter.setOrder(order);
        } catch (ApsSystemException | NumberFormatException t) {
            _logger.error("Error on creation of filter instance", t);
            throw new RuntimeException("Error on creation of filter instance", t);
        }
        return filter;
    }

    private static void setValues(EntitySearchFilterExtend filter, Properties props, String dataType) {
        if (null == dataType) {
            return;
        }
        String value = props.getProperty(VALUE_PARAM);
        String allowedValues = props.getProperty(ALLOWED_VALUES_PARAM);
        String start = props.getProperty(START_PARAM);
        String end = props.getProperty(END_PARAM);
        Object objectValue = getDataObject(value, dataType);
        List<Object> objectAllowedValues = buildAllowedValues(allowedValues, dataType);
        Object objectStart = getDataObject(start, dataType);
        Object objectEnd = getDataObject(end, dataType);
        String likeOptionString = props.getProperty(LIKE_OPTION_PARAM);
        boolean likeOption = (null != likeOptionString) ? Boolean.parseBoolean(likeOptionString) : false;
        String likeOptionTypeString = props.getProperty(LIKE_OPTION_TYPE_PARAM);
        LikeOptionType likeOptionType = LikeOptionType.COMPLETE;
        if (null != likeOptionTypeString) {
            try {
                likeOptionType = Enum.valueOf(LikeOptionType.class, likeOptionTypeString.trim().toUpperCase());
            } catch (Throwable t) {
                _logger.error("Error parsing 'like option type' parameter", t);
            }
        }
        if (objectValue != null) {
            filter.setValue(objectValue);
            filter.setLikeOption(likeOption);
            if (filter.isLikeOption()) {
                filter.setLikeOptionType(likeOptionType);
            }
        } else if (objectAllowedValues != null) {
            filter.setAllowedValues(objectAllowedValues);
            filter.setLikeOption(likeOption);
            if (filter.isLikeOption()) {
                filter.setLikeOptionType(likeOptionType);
            }
        } else if ((null != objectStart) || (null != objectEnd)) {
            filter.setStart(objectStart);
            filter.setEnd(objectEnd);
        } else {
            String nullValue = props.getProperty(NULL_VALUE_PARAM);
            boolean nullOption = (null != nullValue && nullValue.equalsIgnoreCase("true"));
            filter.setNullOption(nullOption);
        }
        String langCode = props.getProperty(LANG_PARAM);
        filter.setLangCode(langCode);
    }

    private static List<Object> buildAllowedValues(String allowedValues, String dataType) {
        if (null == allowedValues) {
            return null;
        }
        List<Object> values = null;
        String[] stringValues = allowedValues.split(ALLOWED_VALUES_SEPARATOR);
        if (null != stringValues && stringValues.length > 0) {
            values = new ArrayList<>();
            for (int i = 0; i < stringValues.length; i++) {
                String stringValue = stringValues[i];
                Object object = getDataObject(stringValue, dataType);
                if (null != object) {
                    values.add(object);
                }
            }
        }
        if (null == values || values.isEmpty()) {
            return null;
        }
        return values;
    }

    private static Object getDataObject(String stringValue, String dataType) {
        if (null == stringValue) {
            return null;
        }
        Object object = null;
        if (dataType.equals(DATA_TYPE_DATE)) {
            object = buildDate(stringValue);
        } else if (dataType.equals(DATA_TYPE_STRING)) {
            object = stringValue;
        } else if (dataType.equals(DATA_TYPE_NUMBER)) {
            object = buildNumber(stringValue);
        }
        return object;
    }

    private static Date buildDate(String dateString) {
        String today = "today, oggi, odierna";
        Date data = null;
        try {
            if (today.contains(dateString)) {
                data = new java.util.Date();
            } else {
                SimpleDateFormat dataF = new SimpleDateFormat(EntitySearchFilterExtend.DATE_PATTERN);
                data = dataF.parse(dateString);
            }
        } catch (ParseException ex) {
            _logger.error("Invalid string - '{}'", dateString);
        }
        return data;
    }

    private static BigDecimal buildNumber(String numberString) {
        BigDecimal number = null;
        try {
            number = new BigDecimal(numberString);
        } catch (NumberFormatException e) {
            _logger.error("Invalid string - '{}'", numberString);
        }
        return number;
    }

}
