package com.agiletec.plugins.jacms.apsadmin.portal.util;

/**
 *
 * @author emarcias
 */
public class LayoutList {
    
    private String _description;
    private int _id;

    public LayoutList(int _id, String _description) {
        this._description = _description;
        this._id = _id;
    }

    public String getDescription() {
        return _description;
    }

    public void setDescription(String _description) {
        this._description = _description;
    }

    public int getId() {
        return _id;
    }

    public void setId(int _id) {
        this._id = _id;
    }
    
    
    
}
