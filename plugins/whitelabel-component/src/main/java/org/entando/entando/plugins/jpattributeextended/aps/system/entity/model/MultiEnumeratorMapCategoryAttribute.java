/*
* The MIT License
*
* Copyright 2019 Entando Inc..
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
package org.entando.entando.plugins.jpattributeextended.aps.system.entity.model;

import com.agiletec.aps.system.common.entity.model.AttributeSearchInfo;
import com.agiletec.aps.system.common.entity.model.attribute.AbstractJAXBAttribute;
import com.agiletec.aps.system.common.entity.model.attribute.AttributeInterface.Status;
import com.agiletec.aps.system.common.entity.model.attribute.DefaultJAXBAttributeType;
import com.agiletec.aps.system.exception.ApsSystemException;
import com.agiletec.aps.system.services.lang.Lang;
import com.agiletec.aps.util.SelectItem;
import java.util.ArrayList;
import java.util.List;
import org.entando.entando.aps.system.common.entity.model.attribute.EnumeratorMapAttribute;
import org.entando.entando.aps.system.common.entity.model.attribute.JAXBEnumeratorMapValue;
import org.entando.entando.plugins.jpattributeextended.aps.system.entity.model.attribute.JAXBEnumeratorMapCategoryAttribute;
import org.entando.entando.plugins.jpattributeextended.aps.system.entity.model.attribute.JAXBMultiEnumeratorMapCategoryAttribute;
import org.entando.entando.plugins.jpattributeextended.aps.system.entity.model.attribute.JAXBMultiEnumeratorMapCategoryAttributeType;
import org.entando.entando.plugins.jpattributeextended.aps.system.entity.model.util.EnumeratorMapCategoryAttributeItemsExtractor;
import org.jdom.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author E.Santoboni
 */
public class MultiEnumeratorMapCategoryAttribute extends EnumeratorMapAttribute {
    
    private static final Logger _logger = LoggerFactory.getLogger(MultiEnumeratorMapCategoryAttribute.class);
    
    @Override
    public Element getJDOMElement() {
        Element attributeElement = this.createRootElement("attribute");
        if (null != this.getValues()) {
            for (int i = 0; i < this.getValues().size(); i++) {
                Element element = new Element("value");
                element.setText(this.getValues().get(i));
                attributeElement.addContent(element);
            }
        }
        return attributeElement;
    }
    
    @Override
    public List<AttributeSearchInfo> getSearchInfos(List<Lang> systemLangs) {
        if (this.getValues() != null && this.getValues().size() > 0) {
            List<AttributeSearchInfo> searchInfos = new ArrayList<>();
            for (int i = 0; i < this.getValues().size(); i++) {
                AttributeSearchInfo info = new AttributeSearchInfo(this.getValues().get(i), null, null, null);
                searchInfos.add(info);
            }
            return searchInfos;
        }
        return null;
    }
    
    
    @Override
    public Status getStatus() {
        if (this.getValues() != null && this.getValues().size() > 0) {
            return Status.VALUED;
        }
        return Status.EMPTY;
    }
    
    
    @Override
    public Object getAttributePrototype() {
        MultiEnumeratorMapCategoryAttribute prototype = 
                (MultiEnumeratorMapCategoryAttribute) super.getAttributePrototype();
        prototype.setBeanFactory(this.getBeanFactory());
        prototype.setItems(this.getItems());
        prototype.setExtractorBeanName(this.getExtractorBeanName());
        prototype.setCodeCategoryItems(this.getCodeCategoryItems());
        prototype.setParentEntity(this.getParentEntity());
        return prototype;
    }
    
    @Override
    public Element getJDOMConfigElement() {
        Element configElement = super.getJDOMConfigElement();
        this.setConfig(configElement);
        return configElement;
    }
    
    private void setConfig(Element configElement) {
        if (null != this.getExtractorBeanName()) {
            configElement.setAttribute("extractorBean", this.getExtractorBeanName());
        }
        if (null != this.getCodeCategoryItems()) {
            configElement.setAttribute("codeCategoryItems", this.getCodeCategoryItems());
        }
    }
    
    @Override
    protected void initItems() {
        if (null != this.getExtractorBeanName()) {
            try {
                EnumeratorMapCategoryAttributeItemsExtractor extractor = 
                        (EnumeratorMapCategoryAttributeItemsExtractor) this.getBeanFactory().getBean(this.getExtractorBeanName(), EnumeratorMapCategoryAttributeItemsExtractor.class);
                if (null != extractor) {
                    extractor.setCodeCategoryItems(this.getCodeCategoryItems());
                    List<SelectItem> items = extractor.getMapItems();
                    if (items != null && items.size() > 0) {
                        this.addExtractedItems(items);
                    }
                }
            } catch (Throwable t) {
                _logger.error("Error while extract items from bean extractor '{}'", this.getExtractorBeanName(), t);
            }
        }
    }
    
    private void addExtractedItems(List<SelectItem> items) {
        SelectItem[] values = null;
        if (null == this.getMapItems() || this.getMapItems().length == 0) {
            values = new SelectItem[items.size()];
            for (int i = 0; i < items.size(); i++) {
                SelectItem item = items.get(i);
                values[i] = item;
            }
        } else {
            values = new SelectItem[this.getMapItems().length + items.size()];
            for (int i = 0; i < this.getMapItems().length; i++) {
                SelectItem item = this.getMapItems()[i];
                values[i] = item;
            }
            for (int i = 0; i < items.size(); i++) {
                SelectItem item = items.get(i);
                values[i + this.getMapItems().length] = item;
            }
        }
        this.setMapItems(values);
    }
    
    
    @Override
    public void setAttributeConfig(Element attributeElement) throws ApsSystemException {
        super.setAttributeConfig(attributeElement);
        String codeCategory = this.extractXmlAttribute(attributeElement, "codeCategoryItems", false);
        if (null != codeCategory) {
            this.setCodeCategoryItems(codeCategory.trim());
        }
        String extractorBeanName = this.extractXmlAttribute(attributeElement, "extractorBean", false);
        this.setExtractorBeanName(extractorBeanName);
        this.initItems();
    }
    
    @Override
    protected AbstractJAXBAttribute getJAXBAttributeInstance() {
        return new JAXBEnumeratorMapCategoryAttribute();
    }
    
    @Override
    public AbstractJAXBAttribute getJAXBAttribute(String langCode) {
        JAXBMultiEnumeratorMapCategoryAttribute jaxbAttribute =
                (JAXBMultiEnumeratorMapCategoryAttribute)  super.createJAXBAttribute(langCode);
        
        JAXBEnumeratorMapValue value = new JAXBEnumeratorMapValue();
        value.setKey(this.getMapKey());
        value.setValue(this.getMapValue());
        jaxbAttribute.setMapValue(value);
        jaxbAttribute.setCodeCategoryItems(getCodeCategoryItems());
        jaxbAttribute.setValues(getValues());
        return jaxbAttribute;
    }
    
    
    @Override
    public JAXBMultiEnumeratorMapCategoryAttributeType getJAXBAttributeType() {
        JAXBMultiEnumeratorMapCategoryAttributeType jaxbAttribute =
                (JAXBMultiEnumeratorMapCategoryAttributeType) super.getJAXBAttributeType();
        jaxbAttribute.setCustomSeparator(this.getCustomSeparator());
        jaxbAttribute.setExtractorBeanName(this.getExtractorBeanName());
        jaxbAttribute.setStaticItems(this.getStaticItems());
        jaxbAttribute.setCodeCategoryItems(this.getCodeCategoryItems());
        jaxbAttribute.setValues(this.getValues());
        return jaxbAttribute;
    }
    
    @Override
    protected DefaultJAXBAttributeType getJAXBAttributeTypeInstance() {
        return new JAXBMultiEnumeratorMapCategoryAttributeType();
    }
    
   
    public String getCodeCategoryItems() {
        return _codeCategoryItems;
    }
    
    public void setCodeCategoryItems(String _codeCategoryItems) {
        this._codeCategoryItems = _codeCategoryItems;
    }
    
    private String _codeCategoryItems;
    
    public List<String> getValues() {
        return values;
    }
    
    public void setValues(List<String> values) {
        this.values = values;
    }
      
    private List<String> values = new ArrayList<>();
}
