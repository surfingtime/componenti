package org.entando.entando.plugins.jpattributeextended.aps.system.entity.model;

import com.agiletec.aps.system.common.entity.model.attribute.DefaultJAXBAttributeType;
import com.agiletec.aps.system.exception.ApsSystemException;
import com.agiletec.aps.system.services.lang.Lang;
import com.agiletec.aps.util.SelectItem;
import com.agiletec.plugins.jacms.aps.system.services.content.model.CmsAttributeReference;
import com.agiletec.plugins.jacms.aps.system.services.content.model.attribute.IReferenceableAttribute;
import java.util.ArrayList;
import java.util.List;
import org.entando.entando.aps.system.common.entity.model.attribute.EnumeratorMapAttribute;
import org.entando.entando.plugins.jpattributeextended.aps.system.entity.model.attribute.JAXBEnumeratorMapPageAttributeType;
import org.jdom.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.entando.entando.plugins.jpattributeextended.aps.system.entity.model.util.EnumeratorMapPageAttributeItemsExtractor;

/**
 *
 * @author emarcias
 */
public class EnumeratorMapPageAttribute extends EnumeratorMapAttribute implements IReferenceableAttribute{
    
    private static final Logger _logger = LoggerFactory.getLogger(EnumeratorMapPageAttribute.class);
    
    @Override
    public Object getAttributePrototype() {
        EnumeratorMapPageAttribute prototype = (EnumeratorMapPageAttribute) super.getAttributePrototype();
        prototype.setBeanFactory(this.getBeanFactory());
        prototype.setItems(this.getItems());        
        prototype.setExtractorBeanName(this.getExtractorBeanName());
        prototype.setCodePageItems(this.getCodePageItems());
        prototype.setShowChildsPage(this.isShowChildsPage());
        return prototype;
    }
        
    @Override
    public Element getJDOMConfigElement() {
        Element configElement = super.getJDOMConfigElement();
        this.setConfig(configElement);
        return configElement;
    }

    private void setConfig(Element configElement) {        
        if (null != this.getExtractorBeanName()) {
            configElement.setAttribute("extractorBean", this.getExtractorBeanName());
        }
        if (null != this.getCodePageItems()) {
            configElement.setAttribute("codePageItems", this.getCodePageItems());
        } 
        if (null != String.valueOf(this.isShowChildsPage())) {
            configElement.setAttribute("showChildsPage", String.valueOf(this.isShowChildsPage()));
        } 
        configElement.setAttribute("separator", ";");
    }
    
    
    @Override
    protected void initItems() {        
        if (null != this.getExtractorBeanName()) {
            try {
                EnumeratorMapPageAttributeItemsExtractor extractor = (EnumeratorMapPageAttributeItemsExtractor) this.getBeanFactory().getBean(this.getExtractorBeanName(), EnumeratorMapPageAttributeItemsExtractor.class);
                extractor.setCodePageItems(this.getCodePageItems());
                extractor.setShowChildsPage(this.isShowChildsPage());
                if (null != extractor) {
                    List<SelectItem> items = extractor.getMapItems();
                    if (items != null && items.size() > 0) {
                        this.addExtractedItems(items);
                    }
                }
            } catch (Throwable t) {
                _logger.error("Error while extract items from bean extractor '{}'", this.getExtractorBeanName(), t);
            }
        }
    }
    
    private void addExtractedItems(List<SelectItem> items) {
        SelectItem[] values = null;
        if (null == this.getMapItems() || this.getMapItems().length == 0) {
            values = new SelectItem[items.size()];
            for (int i = 0; i < items.size(); i++) {
                SelectItem item = items.get(i);
                values[i] = item;
            }
        } else {
            values = new SelectItem[this.getMapItems().length + items.size()];
            for (int i = 0; i < this.getMapItems().length; i++) {
                SelectItem item = this.getMapItems()[i];
                values[i] = item;
            }
            for (int i = 0; i < items.size(); i++) {
                SelectItem item = items.get(i);
                values[i + this.getMapItems().length] = item;
            }
        }
        this.setMapItems(values);
    }
    
    
    @Override
    public void setAttributeConfig(Element attributeElement) throws ApsSystemException {
        super.setAttributeConfig(attributeElement);
        String codePages = this.extractXmlAttribute(attributeElement, "codePageItems", false);
        if (null != codePages) {
            this.setCodePageItems(codePages.trim());
        }
        String showChildsPage = this.extractXmlAttribute(attributeElement, "showChildsPage", false);
        if (null != showChildsPage) {
            this.setShowChildsPage(Boolean.valueOf(showChildsPage));
        }
        String extractorBeanName = this.extractXmlAttribute(attributeElement, "extractorBean", false);
        this.setExtractorBeanName(extractorBeanName);
        this.initItems();
    }
    
    @Override
    public List<CmsAttributeReference> getReferences(List<Lang> systemLangs) {
        List<CmsAttributeReference> refs = new ArrayList<CmsAttributeReference>();       
        if (null != this.getMapKey() && this.getMapKey().trim().length() != 0) {
            CmsAttributeReference ref = new CmsAttributeReference(this.getMapKey(), null, null);            
            refs.add(ref);
        }
        return refs;
    }

    
     @Override
    public JAXBEnumeratorMapPageAttributeType getJAXBAttributeType() {
        JAXBEnumeratorMapPageAttributeType jaxbAttribute = 
                (JAXBEnumeratorMapPageAttributeType) super.getJAXBAttributeType();
        jaxbAttribute.setCustomSeparator(this.getCustomSeparator());
        jaxbAttribute.setExtractorBeanName(this.getExtractorBeanName());
        jaxbAttribute.setStaticItems(this.getStaticItems());
        jaxbAttribute.setCodePageItems(this.getCodePageItems());
        return jaxbAttribute;
    }

    @Override
    protected DefaultJAXBAttributeType getJAXBAttributeTypeInstance() {
        return new JAXBEnumeratorMapPageAttributeType();
    }
    
    public String getCodePageItems() {
        return _codePageItems;
    }

    public void setCodePageItems(String _codePageItems) {
        this._codePageItems = _codePageItems;
    }

    public boolean isShowChildsPage() {
        return _showChildsPage;
    }

    public void setShowChildsPage(boolean _showChildsPage) {
        this._showChildsPage = _showChildsPage;
    }
    
    private String _codePageItems;
    
    private boolean _showChildsPage;
    
}
