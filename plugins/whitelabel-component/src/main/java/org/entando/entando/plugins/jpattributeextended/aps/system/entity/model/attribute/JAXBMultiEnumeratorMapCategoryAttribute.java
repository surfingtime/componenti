/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.entando.entando.plugins.jpattributeextended.aps.system.entity.model.attribute;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Entando
 */
@XmlType(propOrder = {"value", "codeCategoryItems"})
public class JAXBMultiEnumeratorMapCategoryAttribute extends JAXBEnumeratorMapCategoryAttribute {
    
    @XmlElement(name = "value", required = false)
    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }    
    
    private List<String> values = new ArrayList<>();

}
