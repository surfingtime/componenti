/*
 * The MIT License
 *
 * Copyright 2019 Entando Inc..
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.entando.entando.plugins.jpattributeextended.aps.system.entity.parse;

import com.agiletec.aps.system.common.entity.parse.attribute.AbstractAttributeHandler;
import org.entando.entando.plugins.jpattributeextended.aps.system.entity.model.MultiEnumeratorMapCategoryAttribute;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

/**
 *
 * @author eu
 */
public class MultiEnumeratorMapCategoryHandler extends AbstractAttributeHandler {

    @Override
    public void startAttribute(Attributes attributes, String qName) throws SAXException {
        if (qName.equals("value")) {
            this.startValue(attributes, qName);
        }
    }

    private void startValue(Attributes attributes, String qName) throws SAXException {
        //nothing to do
    }

    @Override
    public void endAttribute(String qName, StringBuffer textBuffer) {
        if (qName.equals("value")) {
            this.endValue(textBuffer);
        }
    }

    private void endValue(StringBuffer textBuffer) {
        if (null != textBuffer && null != this.getCurrentAttr()) {
            ((MultiEnumeratorMapCategoryAttribute) this.getCurrentAttr()).getValues().add(textBuffer.toString());
        }
    }

}
