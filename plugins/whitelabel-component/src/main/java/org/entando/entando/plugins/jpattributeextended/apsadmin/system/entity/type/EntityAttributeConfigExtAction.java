package org.entando.entando.plugins.jpattributeextended.apsadmin.system.entity.type;

import com.agiletec.aps.system.common.entity.model.attribute.AbstractListAttribute;
import com.agiletec.aps.system.common.entity.model.attribute.AttributeInterface;
import com.agiletec.aps.system.common.entity.model.attribute.CompositeAttribute;
import com.agiletec.aps.system.common.entity.model.attribute.DateAttribute;
import com.agiletec.aps.system.common.entity.model.attribute.EnumeratorAttribute;
import com.agiletec.aps.system.common.entity.model.attribute.ListAttribute;
import com.agiletec.aps.system.common.entity.model.attribute.MonoListAttribute;
import com.agiletec.aps.system.common.entity.model.attribute.NumberAttribute;
import com.agiletec.aps.system.common.entity.model.attribute.util.DateAttributeValidationRules;
import com.agiletec.aps.system.common.entity.model.attribute.util.IAttributeValidationRules;
import com.agiletec.aps.system.common.entity.model.attribute.util.NumberAttributeValidationRules;
import com.agiletec.aps.system.common.entity.model.attribute.util.TextAttributeValidationRules;
import com.agiletec.aps.system.common.searchengine.IndexableAttributeInterface;
import com.agiletec.aps.system.services.category.Category;
import com.agiletec.aps.system.services.category.ICategoryManager;
import com.agiletec.aps.system.services.lang.Lang;
import com.agiletec.aps.system.services.page.IPage;
import com.agiletec.aps.system.services.page.IPageManager;
import com.agiletec.apsadmin.system.ApsAdminSystemConstants;
import com.agiletec.apsadmin.system.entity.type.EntityAttributeConfigAction;
import com.agiletec.apsadmin.system.entity.type.ICompositeAttributeConfigAction;
import com.agiletec.apsadmin.system.entity.type.IListElementAttributeConfigAction;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.entando.entando.aps.system.common.entity.model.attribute.util.EnumeratorMapAttributeItemsExtractor;
import org.entando.entando.plugins.jpattributeextended.aps.system.entity.model.EnumeratorMapCategoryAttribute;
import org.entando.entando.plugins.jpattributeextended.aps.system.entity.model.EnumeratorMapPageAttribute;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author emarcias
 */
public class EntityAttributeConfigExtAction extends EntityAttributeConfigAction {

    private static final Logger _logger = LoggerFactory.getLogger(EntityAttributeConfigAction.class);

    private IPageManager pageManager;
    private ICategoryManager categoryManager;

    public IPageManager getPageManager() {
        return pageManager;
    }

    public void setPageManager(IPageManager pageManager) {
        this.pageManager = pageManager;
    }

    public ICategoryManager getCategoryManager() {
        return categoryManager;
    }

    public void setCategoryManager(ICategoryManager categoryManager) {
        this.categoryManager = categoryManager;
    }

    @Override
    public void validate() {
        if (this.getAttributeTypeCode().trim().equals("EnumeratorMapPage")) {
            if (null == this.getCodePageItems() || this.getCodePageItems().trim().length() == 0) {
                String[] args = {this.getAttributeTypeCode()};
                this.addFieldError("codePageItems", this.getText("error.entity.attribute.enumeratormappage.items.missing", args));
            } else {
                String[] pagine = this.getCodePageItems().split(";");
                for (String code : pagine) {
                    IPage page = this.getPageManager().getDraftPage(code);
                    if (page == null) {
                        String[] args = {code};
                        this.addFieldError("codePageItems", this.getText("error.entity.attribute.enumeratormappage.items.notexist", args));
                    } else {
                        IPage pageP = this.getPageManager().getOnlinePage(code);
                        if (pageP == null) {
                            String[] args = {code};
                            this.addFieldError("codePageItems", this.getText("error.entity.attribute.enumeratormappage.items.notpublish", args));
                        } else {
                            if (!pageP.isShowable()) {
                                String[] args = {code};
                                this.addFieldError("codePageItems", this.getText("error.entity.attribute.enumeratormappage.items.notshow", args));
                            }
                        }
                    }
                }
            }
        } else if (this.getAttributeTypeCode().trim().equals("EnumeratorMapCategory") || this.getAttributeTypeCode().trim().equals("MultiEnumeratorMapCategory")) {
            if (null == this.getCodeCategoryItems() || this.getCodeCategoryItems().trim().length() == 0) {
                String[] args = {this.getAttributeTypeCode()};
                this.addFieldError("codeCategoryItems", this.getText("error.entity.attribute.enumeratormapcategory.items.missing", args));
            } else {
                Category categoria = this.getCategoryManager().getCategory(this.getCodeCategoryItems());
                if (categoria == null) {
                    String[] args = {this.getCodeCategoryItems()};
                    this.addFieldError("codeCategoryItems", this.getText("error.entity.attribute.enumeratormapcategory.items.notexist", args));
                } else {
                    if (categoria.getChildrenCodes().length == 0) {
                        String[] args = {this.getCodeCategoryItems()};
                        this.addFieldError("codeCategoryItems", this.getText("error.entity.attribute.enumeratormapcategory.items.notchilds", args));
                    }
                }
            }
        } else {
            super.validate();
        }
    }

    /**
     * Fill form fields.
     *
     * @param attribute
     */
    @Override
    protected void valueFormFields(AttributeInterface attribute) {
        this.setAttributeName(attribute.getName());
        if (null != attribute.getDescription() && attribute.getDescription().trim().length() > 0) {
            this.setAttributeDescription(attribute.getDescription());
        }
        this.setAttributeTypeCode(attribute.getType());
        if (null != attribute.getRoles()) {
            this.setAttributeRoles(Arrays.asList(attribute.getRoles()));
        }
        if (null != attribute.getDisablingCodes()) {
            this.setDisablingCodes(Arrays.asList(attribute.getDisablingCodes()));
        }
        IAttributeValidationRules valRule = attribute.getValidationRules();
        this.setRequired(valRule.isRequired());
        this.setOgnlValidationRule(valRule.getOgnlValidationRule());
        this.setSearchable(attribute.isSearchable());
        String indexingType = attribute.getIndexingType();
        if (null != indexingType) {
            this.setIndexable(indexingType.equalsIgnoreCase(IndexableAttributeInterface.INDEXING_TYPE_TEXT));
        }
        if (attribute.isTextAttribute()) {
            TextAttributeValidationRules textValRule = (TextAttributeValidationRules) valRule;
            if (null != textValRule.getMaxLength() && textValRule.getMaxLength() > -1) {
                this.setMaxLength(textValRule.getMaxLength());
            }
            if (null != textValRule.getMinLength() && textValRule.getMinLength() > -1) {
                this.setMinLength(textValRule.getMinLength());
            }
            this.setRegexp(textValRule.getRegexp());
            this.setRangeEndString((String) textValRule.getRangeEnd());
            this.setRangeStartString((String) textValRule.getRangeStart());
            this.setEqualString((String) textValRule.getValue());
            this.setRangeEndStringAttribute(textValRule.getRangeEndAttribute());
            this.setRangeStartStringAttribute(textValRule.getRangeStartAttribute());
            this.setEqualStringAttribute(textValRule.getValueAttribute());
            if (attribute instanceof EnumeratorAttribute) {
                if (this.getAttributeTypeCode().trim().equals("EnumeratorMapPage")) {
                    EnumeratorMapPageAttribute enumeratorPageAttribute = (EnumeratorMapPageAttribute) attribute;
                    this.setCodePageItems(enumeratorPageAttribute.getCodePageItems());
                    this.setEnumeratorExtractorBean(enumeratorPageAttribute.getExtractorBeanName());
                    this.setShowChildsPage(enumeratorPageAttribute.isShowChildsPage());
                } else if (this.getAttributeTypeCode().trim().equals("EnumeratorMapCategory") || this.getAttributeTypeCode().trim().equals("MultiEnumeratorMapCategory")) {
                    EnumeratorMapCategoryAttribute enumeratorCategoryAttribute = (EnumeratorMapCategoryAttribute) attribute;
                    this.setCodeCategoryItems(enumeratorCategoryAttribute.getCodeCategoryItems());
                    this.setEnumeratorExtractorBean(enumeratorCategoryAttribute.getExtractorBeanName());
                } else {
                    EnumeratorAttribute enumeratorAttribute = (EnumeratorAttribute) attribute;
                    this.setEnumeratorStaticItems(enumeratorAttribute.getStaticItems());
                    this.setEnumeratorStaticItemsSeparator(enumeratorAttribute.getCustomSeparator());
                    this.setEnumeratorExtractorBean(enumeratorAttribute.getExtractorBeanName());
                }
            }
        }
        if (attribute instanceof DateAttribute) {
            DateAttributeValidationRules dateValRule = (DateAttributeValidationRules) valRule;
            this.setRangeEndDate((Date) dateValRule.getRangeEnd());
            this.setRangeStartDate((Date) dateValRule.getRangeStart());
            this.setEqualDate((Date) dateValRule.getValue());
            this.setRangeEndDateAttribute(dateValRule.getRangeEndAttribute());
            this.setRangeStartDateAttribute(dateValRule.getRangeStartAttribute());
            this.setEqualDateAttribute(dateValRule.getValueAttribute());
        }
        if (attribute instanceof NumberAttribute) {
            NumberAttributeValidationRules nulValRule = (NumberAttributeValidationRules) valRule;
            this.setRangeEndNumber((Integer) nulValRule.getRangeEnd());
            this.setRangeStartNumber((Integer) nulValRule.getRangeStart());
            this.setEqualNumber((Integer) nulValRule.getValue());
            this.setRangeEndNumberAttribute(nulValRule.getRangeEndAttribute());
            this.setRangeStartNumberAttribute(nulValRule.getRangeStartAttribute());
            this.setEqualNumberAttribute(nulValRule.getValueAttribute());
        }
    }

    /**
     * Fill attribute fields.
     *
     * @param attribute The attribute to edit with the form values.
     * @return A customized return code in the attribute needs a extra
     * configuration, else null.
     */
    @Override
    protected String fillAttributeFields(AttributeInterface attribute) {
        super.fillAttributeFields(attribute);
        AttributeInterface nestedType = null;
        if (attribute instanceof AbstractListAttribute) {
            AbstractListAttribute listAttribute = (AbstractListAttribute) attribute;
            if (this.getStrutsAction() == ApsAdminSystemConstants.EDIT
                    && listAttribute.getNestedAttributeTypeCode().equals(this.getListNestedType())) {
                if (listAttribute instanceof ListAttribute) {
                    Lang defaultLang = this.getLangManager().getDefaultLang();
                    nestedType = ((ListAttribute) listAttribute).addAttribute(defaultLang.getCode());//Composite Element
                    ((ListAttribute) listAttribute).getAttributeList(defaultLang.getCode()).clear();
                } else {
                    nestedType = ((MonoListAttribute) listAttribute).addAttribute();//Composite Element
                    ((MonoListAttribute) listAttribute).getAttributes().clear();
                }
            } else {
                nestedType = this.getAttributePrototype(this.getListNestedType());
                if (nestedType != null) {
                    nestedType.setName(this.getAttributeName());
                } else {
                    _logger.info("******** List Type NULL!!!!");
                }
            }
            listAttribute.setNestedAttributeType(nestedType);
            nestedType.setName(attribute.getName());
        }
        if ((attribute instanceof CompositeAttribute) || (nestedType != null && nestedType instanceof CompositeAttribute)) {
            CompositeAttribute composite = ((attribute instanceof CompositeAttribute) ? (CompositeAttribute) attribute : (CompositeAttribute) nestedType);
            if (null != nestedType) {
                this.getRequest().getSession().setAttribute(IListElementAttributeConfigAction.LIST_ATTRIBUTE_ON_EDIT_SESSION_PARAM, (AbstractListAttribute) attribute);
            }
            this.getRequest().getSession().setAttribute(ICompositeAttributeConfigAction.COMPOSITE_ATTRIBUTE_ON_EDIT_SESSION_PARAM, composite);
            return "configureCompositeAttribute";
        }
        if (nestedType != null) {
            this.getRequest().getSession().setAttribute(IListElementAttributeConfigAction.LIST_ATTRIBUTE_ON_EDIT_SESSION_PARAM, (AbstractListAttribute) attribute);
            this.getRequest().getSession().setAttribute(IListElementAttributeConfigAction.LIST_ELEMENT_ON_EDIT_SESSION_PARAM, nestedType);
            return "configureListElementAttribute";
        }

        if (attribute instanceof EnumeratorAttribute) {
            if (this.getAttributeTypeCode().trim().equals("EnumeratorMapPage")) {
                EnumeratorMapPageAttribute enumeratorAttribute = (EnumeratorMapPageAttribute) attribute;
                enumeratorAttribute.setCodePageItems(this.getCodePageItems());
                enumeratorAttribute.setExtractorBeanName(this.getEnumeratorExtractorBean());
                enumeratorAttribute.setShowChildsPage(this.isShowChildsPage());
            } else if (this.getAttributeTypeCode().trim().equals("EnumeratorMapCategory") || this.getAttributeTypeCode().trim().equals("MultiEnumeratorMapCategory")) {
                EnumeratorMapCategoryAttribute enumeratorAttribute = (EnumeratorMapCategoryAttribute) attribute;
                enumeratorAttribute.setCodeCategoryItems(this.getCodeCategoryItems());
                enumeratorAttribute.setExtractorBeanName(this.getEnumeratorExtractorBean());
            } else {
                EnumeratorAttribute enumeratorAttribute = (EnumeratorAttribute) attribute;
                enumeratorAttribute.setStaticItems(this.getEnumeratorStaticItems());
                if (null != this.getEnumeratorStaticItemsSeparator() && this.getEnumeratorStaticItemsSeparator().length() > 0) {
                    enumeratorAttribute.setCustomSeparator(this.getEnumeratorStaticItemsSeparator());
                }
                if (null != this.getEnumeratorExtractorBean() && this.getEnumeratorExtractorBean().trim().length() > 0) {
                    enumeratorAttribute.setExtractorBeanName(this.getEnumeratorExtractorBean());
                } else {
                    enumeratorAttribute.setExtractorBeanName(null);
                }
            }
        }

        return null;
    }

    private String[] createStringArray(List<String> list) {
        if (null == list || list.isEmpty()) {
            return null;
        }
        String[] array = new String[list.size()];
        for (int i = 0; i < list.size(); i++) {
            array[i] = list.get(i);
        }
        return array;
    }

    public List<String> getEnumeratorMapPageExtractorBeans() {
        return this.getEnumeratorExtractorBeans(EnumeratorMapAttributeItemsExtractor.class);
    }

    public String getCodePageItems() {
        return _codePageItems;
    }

    public void setCodePageItems(String _codePageItems) {
        this._codePageItems = _codePageItems;
    }

    public String getCodeCategoryItems() {
        return _codeCategoryItems;
    }

    public void setCodeCategoryItems(String _codeCategoryItems) {
        this._codeCategoryItems = _codeCategoryItems;
    }

    public boolean isShowChildsPage() {
        return _showChildsPage;
    }

    public void setShowChildsPage(boolean _showChildsPage) {
        this._showChildsPage = _showChildsPage;
    }

    private String _codePageItems;
    private String _codeCategoryItems;
    private boolean _showChildsPage;

}
