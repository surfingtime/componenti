/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.entando.entando.plugins.jpattributeextended.aps.system.entity.model.attribute;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.entando.entando.aps.system.common.entity.model.attribute.JAXBEnumeratorMapAttribute;

/**
 *
 * @author Entando
 */
@XmlType(propOrder = {"codeCategoryItems"})
public class JAXBEnumeratorMapCategoryAttribute extends JAXBEnumeratorMapAttribute implements Serializable {
    
    @XmlElement(name = "codeCategoryItems", required = false)
    public String getCodeCategoryItems() {
        return _codeCategoryItems;
    }

    public void setCodeCategoryItems(String _codeCategoryItems) {
        this._codeCategoryItems = _codeCategoryItems;
    }
    
    private String _codeCategoryItems;
    
}
