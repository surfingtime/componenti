
package org.entando.entando.plugins.jpattributeextended.aps.system.entity.model;

import com.agiletec.aps.system.common.tree.ITreeNode;
import com.agiletec.aps.system.services.category.Category;
import com.agiletec.aps.system.services.category.ICategoryManager;
import com.agiletec.aps.system.services.lang.ILangManager;
import com.agiletec.aps.system.services.lang.Lang;
import com.agiletec.aps.util.SelectItem;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.commons.beanutils.BeanComparator;
import org.entando.entando.plugins.jpattributeextended.aps.system.entity.model.util.EnumeratorMapCategoryAttributeItemsExtractor;

/**
 *
 * @author emarcias
 */
public class EnumeratorMapCategoryAttributeExtractor implements EnumeratorMapCategoryAttributeItemsExtractor{

    private ICategoryManager categoryManager;
    private ILangManager langManager;
    

    protected ICategoryManager getCategoryManager() {
        return categoryManager;
    }

    public void setCategoryManager(ICategoryManager categoryManager) {
        this.categoryManager = categoryManager;
    }
    
    protected ILangManager getLangManager() {
        return langManager;
    }

    public void setLangManager(ILangManager langManager) {
        this.langManager = langManager;
    }
            
    @Override
    public List<SelectItem> getMapItems() {
        List<SelectItem> items = new ArrayList<SelectItem>();
        if (this.getCodeCategoryItems()!= null) {
            Lang lang = this.getLangManager().getDefaultLang();
            if (this.getCodeCategoryItems() != null && !this.getCodeCategoryItems().isEmpty()) { 
                String[] categorie = null;
                ITreeNode nodo = this.getCategoryManager().getNode(this.getCodeCategoryItems());
                if (nodo != null && nodo.getChildrenCodes() != null && nodo.getChildrenCodes().length > 0) {
                   categorie = nodo.getChildrenCodes();
                }
                
                if (categorie != null){
                    for (String code : categorie) {
                        Category categoria = this.getCategoryManager().getCategory(code);
                        String titolo = categoria.getTitle(lang.getCode());
                        items.add(new SelectItem(code, titolo));
                    }
                    BeanComparator c = new BeanComparator("value");
                    Collections.sort(items, c);
                }
            }
        }           
        return items;
    }

    @Override
    public void setCodeCategoryItems(String codeCategory) {
        this._codeCategoryItems = codeCategory;
    }

    @Override
    public String getCodeCategoryItems() {
        return this._codeCategoryItems;
    }
    
    private String _codeCategoryItems;
    
}
