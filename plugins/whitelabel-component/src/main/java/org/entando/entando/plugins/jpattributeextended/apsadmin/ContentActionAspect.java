package org.entando.entando.plugins.jpattributeextended.apsadmin;

import com.agiletec.aps.system.common.entity.model.attribute.AttributeInterface;
import com.agiletec.aps.system.services.category.Category;
import com.agiletec.aps.system.services.category.ICategoryManager;
import com.agiletec.plugins.jacms.aps.system.services.content.IContentManager;
import com.agiletec.plugins.jacms.aps.system.services.content.model.Content;
import com.agiletec.plugins.jacms.apsadmin.content.ContentAction;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.entando.entando.plugins.jpattributeextended.aps.system.entity.model.EnumeratorMapCategoryAttribute;
import org.entando.entando.plugins.jpattributeextended.aps.system.entity.model.MultiEnumeratorMapCategoryAttribute;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 *
 * @author emarcias
 */
@Aspect
public class ContentActionAspect {
    private static final Logger _logger = LoggerFactory.getLogger(ContentActionAspect.class);
    
    private IContentManager _contentManager;
    private ICategoryManager _categoryManager;
    
    /**
     * Esegue l'azione di salvataggio del contenuto in fase di redazione.
     */
    @Before("execution(* com.agiletec.plugins.jacms.apsadmin.content.ContentAction.saveContent())")
    public void saveContentApprovation(JoinPoint joinPoint) {
        ContentAction action = (ContentAction) joinPoint.getTarget();
        Content currentContent = action.getContent();
        if (null != currentContent) {
            addUpdateCategory(currentContent);
        }
    }

    /**
     * Esegue l'azione di salvataggio e pubblicazione del contenuto in fase di
     * redazione.
     */
    @Before("execution(* com.agiletec.plugins.jacms.apsadmin.content.ContentAction.saveAndApprove())")
    public void saveAndApproveBefore(JoinPoint joinPoint) {
        ContentAction action = (ContentAction) joinPoint.getTarget();
        Content currentContent = action.getContent();
        if (null != currentContent) {
            addUpdateCategory(currentContent);
            getSession().setAttribute("contentOnSession", currentContent.getId());
        }
    }
    
    private void addUpdateCategory(Content content) {
        List<AttributeInterface> listaAttributi = content.getAttributeList();
        for (AttributeInterface attributo : listaAttributi) {
            if (attributo instanceof EnumeratorMapCategoryAttribute) {
                String codeCategoryItems = ((EnumeratorMapCategoryAttribute) attributo).getCodeCategoryItems();
                List<Category> listaCategorieContent = new ArrayList<>();
                listaCategorieContent.addAll(content.getCategories());
                for (Category cat : listaCategorieContent) {
                    if (cat.getParentCode().equalsIgnoreCase(codeCategoryItems)) {
                        content.removeCategory(cat);
                    }
                }
                if (attributo instanceof MultiEnumeratorMapCategoryAttribute) {
                    List<String> values = ((MultiEnumeratorMapCategoryAttribute) attributo).getValues();
                    if (null == values) {
                        break;
                    }
                    for (int i = 0; i < values.size(); i++) {
                        String categoryCode = values.get(i);
                        this.checkCategory(categoryCode, content);
                    }
                } else {
                    String categoryCode = attributo.getValue().toString();
                    if (StringUtils.isBlank(categoryCode)) {
                        break;
                    }
                    this.checkCategory(categoryCode, content);
                }
            }
        }
    }
    
    private void checkCategory(String categoryCode, Content content) {
        boolean found = false;
        for (Category cat : content.getCategories()) {
            if (cat.getCode().equals(categoryCode)) {
                found = true;
                break;
            }
        }
        if (!found) {
            Category toAdd = this.getCategoryManager().getCategory(categoryCode);
            if (null != toAdd && !toAdd.getCode().equals(toAdd.getParentCode())
                    && !content.getCategories().contains(toAdd)) {
                content.addCategory(toAdd);
            }
        }
    }

    public IContentManager getContentManager() {
        return _contentManager;
    }

    public void setContentManager(IContentManager _contentManager) {
        this._contentManager = _contentManager;
    }

    public ICategoryManager getCategoryManager() {
        return _categoryManager;
    }

    public void setCategoryManager(ICategoryManager _categoryManager) {
        this._categoryManager = _categoryManager;
    }

    protected HttpSession getSession() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        return request.getSession();
    }
    
}
