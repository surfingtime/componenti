package org.entando.entando.plugins.jpattributeextended.aps.system.entity.model;

import com.agiletec.aps.system.common.entity.model.AttributeSearchInfo;
import com.agiletec.aps.system.services.lang.Lang;
import com.agiletec.plugins.jacms.aps.system.services.content.model.SymbolicLink;
import com.agiletec.plugins.jacms.aps.system.services.content.model.attribute.LinkAttribute;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author emarcias
 */
public class LinkSearchableAttribute extends LinkAttribute {

    
    @Override
    public boolean isSearcheable() {
        return true;
    }

    
    @Override
    public List<AttributeSearchInfo> getSearchInfos(List<Lang> systemLangs) {
        
        List<AttributeSearchInfo> infos = null;
        infos = super.getSearchInfos(systemLangs); 
        
        if (null != this.getSymbolicLink()) {
            if(infos == null){
                infos = new ArrayList<>(); 
            }
            
            for (int i = 0; i < systemLangs.size(); i++) {
                Lang lang = systemLangs.get(i); 
                AttributeSearchInfo info = null;
                if(this.getSymbolicLink().getDestType() == SymbolicLink.CONTENT_TYPE){
                    info = new AttributeSearchInfo(this.getSymbolicLink().getContentDest(), null, null, lang.getCode());
                }                                  
                infos.add(info);
            }
        }
        
        return infos;         
    }
    
    
}
