/*
 * The MIT License
 *
 * Copyright 2019 Entando Inc..
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.entando.entando.plugins.jpattributeextended.apsadmin.content;

import com.agiletec.aps.system.common.entity.model.attribute.AttributeInterface;
import com.agiletec.aps.system.common.entity.model.attribute.ListAttribute;
import com.agiletec.aps.system.common.entity.model.attribute.MonoListAttribute;
import com.agiletec.aps.system.services.category.Category;
import com.agiletec.aps.system.services.category.ICategoryManager;
import com.agiletec.apsadmin.system.BaseAction;
import com.agiletec.plugins.jacms.aps.system.services.content.model.Content;
import com.agiletec.plugins.jacms.apsadmin.content.ContentActionConstants;
import java.util.List;
import org.entando.entando.plugins.jpattributeextended.aps.system.entity.model.MultiEnumeratorMapCategoryAttribute;

/**
 * @author E.Santoboni
 */
public class MultiEnumeratorMapCategoryAction extends BaseAction {

    private String categoryCode;
    private String categoryParamName;
    private String attributeName;
    private int elementIndex = -1;
    private String contentOnSessionMarker;
    
    private ICategoryManager categoryManager;
    
    public String addCategory() {
        AttributeInterface attribute = this.getContent().getAttribute(this.getAttributeName());
        MultiEnumeratorMapCategoryAttribute enumMap = this.extractAttribute(attribute);
        if (null != enumMap) {
            String extractedCategoryCode = this.getRequest().getParameter(this.getCategoryParamName());
            Category category = this.getCategoryManager().getCategory(extractedCategoryCode);
            if (null != category && !enumMap.getValues().contains(extractedCategoryCode)) {
                enumMap.getValues().add(extractedCategoryCode);
            }
        }
        return SUCCESS;
    }
    
    public String removeCategory() {
        AttributeInterface attribute = this.getContent().getAttribute(this.getAttributeName());
        MultiEnumeratorMapCategoryAttribute enumMap = this.extractAttribute(attribute);
        if (null != enumMap) {
            enumMap.getValues().remove(this.getCategoryCode());
        }
        return SUCCESS;
    }
    
    private MultiEnumeratorMapCategoryAttribute extractAttribute(AttributeInterface attribute) {
        if (attribute instanceof MultiEnumeratorMapCategoryAttribute) {
            return (MultiEnumeratorMapCategoryAttribute) attribute;
        } else if (attribute instanceof MonoListAttribute) {
            AttributeInterface attributeElement = ((MonoListAttribute) attribute).getAttribute(this.getElementIndex());
            return this.extractAttribute(attributeElement);
        } else if (attribute instanceof ListAttribute) {
            ListAttribute listAttribute = (ListAttribute) attribute;
            String defaultLang = listAttribute.getDefaultLangCode();
            List<AttributeInterface> attributeList = listAttribute.getAttributeList(defaultLang);
            AttributeInterface attributeElement = attributeList.get(this.getElementIndex());
            return this.extractAttribute(attributeElement);
        }
        return null;
    }
    
    public String getEntryContentAnchorDest() {
        StringBuilder buffer = new StringBuilder("contentedit_");
        buffer.append(this.getLangManager().getDefaultLang().getCode());
        buffer.append("_");
        buffer.append(this.getAttributeName());
        return buffer.toString();
    }

    /**
     * Restituisce il contenuto in sesione.
     *
     * @return Il contenuto in sessione.
     */
    public Content getContent() {
        return (Content) this.getRequest().getSession().getAttribute(ContentActionConstants.SESSION_PARAM_NAME_CURRENT_CONTENT_PREXIX + this.getContentOnSessionMarker());
    }

    public String getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    public String getCategoryParamName() {
        return categoryParamName;
    }

    public void setCategoryParamName(String categoryParamName) {
        this.categoryParamName = categoryParamName;
    }
    
    public String getContentOnSessionMarker() {
        return contentOnSessionMarker;
    }

    public void setContentOnSessionMarker(String contentOnSessionMarker) {
        this.contentOnSessionMarker = contentOnSessionMarker;
    }

    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }
    
    public int getElementIndex() {
        return elementIndex;
    }

    public void setElementIndex(int elementIndex) {
        this.elementIndex = elementIndex;
    }

    protected ICategoryManager getCategoryManager() {
        return categoryManager;
    }
    public void setCategoryManager(ICategoryManager categoryManager) {
        this.categoryManager = categoryManager;
    }
    
}
