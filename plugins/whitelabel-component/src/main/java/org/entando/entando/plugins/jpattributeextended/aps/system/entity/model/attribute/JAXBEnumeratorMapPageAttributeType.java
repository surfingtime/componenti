/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.entando.entando.plugins.jpattributeextended.aps.system.entity.model.attribute;

import com.agiletec.aps.system.common.entity.model.attribute.AttributeInterface;
import com.agiletec.aps.system.common.entity.model.attribute.JAXBEnumeratorAttributeType;
import java.util.Map;
import javax.xml.bind.annotation.XmlRootElement;
import org.entando.entando.aps.system.services.api.model.ApiException;
import org.entando.entando.plugins.jpattributeextended.aps.system.entity.model.EnumeratorMapPageAttribute;

/**
 *
 * @author Entando
 */
@XmlRootElement(name = "attributeType")
public class JAXBEnumeratorMapPageAttributeType extends JAXBEnumeratorAttributeType {

    @Override
    public AttributeInterface createAttribute(Map<String, AttributeInterface> attributes) throws ApiException {
        EnumeratorMapPageAttribute attribute = 
                (EnumeratorMapPageAttribute) super.createAttribute(attributes);
        attribute.setCodePageItems(this.getCodePageItems());
        return attribute;
    }
    
    public String getCodePageItems() {
        return _codePageItems;
    }

    public void setCodePageItems(String codePageItems) {
        this._codePageItems = codePageItems;
    }
    
    private String _codePageItems;
    
}
