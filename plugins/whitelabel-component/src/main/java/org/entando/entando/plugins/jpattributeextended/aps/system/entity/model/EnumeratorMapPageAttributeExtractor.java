package org.entando.entando.plugins.jpattributeextended.aps.system.entity.model;

import com.agiletec.aps.system.common.tree.ITreeNode;
import com.agiletec.aps.system.services.lang.ILangManager;
import com.agiletec.aps.system.services.lang.Lang;
import com.agiletec.aps.system.services.page.IPage;
import com.agiletec.aps.system.services.page.IPageManager;
import com.agiletec.aps.util.SelectItem;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.entando.entando.plugins.jpattributeextended.aps.system.entity.model.util.EnumeratorMapPageAttributeItemsExtractor;

/**
 *
 * @author emarcias
 */
public class EnumeratorMapPageAttributeExtractor implements EnumeratorMapPageAttributeItemsExtractor {

    private IPageManager pageManager;
    private ILangManager langManager;

    protected IPageManager getPageManager() {
        return pageManager;
    }

    public void setPageManager(IPageManager pageManager) {
        this.pageManager = pageManager;
    }

    protected ILangManager getLangManager() {
        return langManager;
    }

    public void setLangManager(ILangManager langManager) {
        this.langManager = langManager;
    }

    @Override
    public List<SelectItem> getMapItems() {
        List<SelectItem> items = new ArrayList<SelectItem>();
        
        if (this.getCodePageItems() != null) {
            Lang lang = this.getLangManager().getDefaultLang();
            if (this.getCodePageItems() != null && !this.getCodePageItems().isEmpty()) {
                String[] pagine = this.getCodePageItems().split(";");                
                for (String code : pagine) {
                    if(this.isShowChildsPage()){
                        ITreeNode nodo = this.getPageManager().getNode(code);
                        if (nodo != null && nodo.getChildrenCodes() != null && nodo.getChildrenCodes().length > 0) {
                            String[] figli = nodo.getChildrenCodes();
                            for (String codeFiglio : figli) {
                                IPage page = this.getPageManager().getOnlinePage(codeFiglio);
                                if(page != null && page.isShowable()){
                                    String titolo = page.getTitle(lang.getCode());
                                    items.add(new SelectItem(codeFiglio, titolo));
                                }
                            }
                        }else{
                           IPage page = this.getPageManager().getOnlinePage(code);
                            if(page != null && page.isShowable()){
                                String titolo = page.getTitle(lang.getCode());
                                items.add(new SelectItem(code, titolo));
                            } 
                        }
                    }else{                    
                        IPage page = this.getPageManager().getOnlinePage(code);
                        if(page != null && page.isShowable()){
                            String titolo = page.getTitle(lang.getCode());
                            items.add(new SelectItem(code, titolo));
                        }
                    }
                }
            }
        }        
        return items;
    }

    @Override
    public void setCodePageItems(String codesPages) {
        this._codePageItems = codesPages;
    }

    @Override
    public String getCodePageItems() {
        return this._codePageItems;
    }

    public boolean isShowChildsPage() {
        return _showChildsPage;
    }

    public void setShowChildsPage(boolean _showChildsPage) {
        this._showChildsPage = _showChildsPage;
    }
    
    private String _codePageItems;
    
    private boolean _showChildsPage;

}
