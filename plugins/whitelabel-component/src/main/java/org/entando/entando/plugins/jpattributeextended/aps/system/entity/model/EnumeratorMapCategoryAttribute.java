package org.entando.entando.plugins.jpattributeextended.aps.system.entity.model;

import com.agiletec.aps.system.common.entity.model.attribute.AbstractJAXBAttribute;
import com.agiletec.aps.system.common.entity.model.attribute.DefaultJAXBAttributeType;
import com.agiletec.aps.system.exception.ApsSystemException;
import com.agiletec.aps.util.SelectItem;
import java.util.List;
import org.entando.entando.aps.system.common.entity.model.attribute.EnumeratorMapAttribute;
import org.entando.entando.aps.system.common.entity.model.attribute.JAXBEnumeratorMapValue;
import org.entando.entando.plugins.jpattributeextended.aps.system.entity.model.attribute.JAXBEnumeratorMapCategoryAttribute;
import org.entando.entando.plugins.jpattributeextended.aps.system.entity.model.attribute.JAXBEnumeratorMapCategoryAttributeType;
import org.entando.entando.plugins.jpattributeextended.aps.system.entity.model.util.EnumeratorMapCategoryAttributeItemsExtractor;
import org.jdom.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author emarcias
 */
public class EnumeratorMapCategoryAttribute extends EnumeratorMapAttribute /*implements IReferenceableAttribute*/{

    private static final Logger _logger = LoggerFactory.getLogger(EnumeratorMapCategoryAttribute.class);
    
    @Override
    public Object getAttributePrototype() {
        EnumeratorMapCategoryAttribute prototype = (EnumeratorMapCategoryAttribute) super.getAttributePrototype();
        prototype.setBeanFactory(this.getBeanFactory());
        prototype.setItems(this.getItems());        
        prototype.setExtractorBeanName(this.getExtractorBeanName());
        prototype.setCodeCategoryItems(this.getCodeCategoryItems());
        prototype.setParentEntity(this.getParentEntity());
        return prototype;
    }
    
    @Override
    public Element getJDOMConfigElement() {
        Element configElement = super.getJDOMConfigElement();
        this.setConfig(configElement);
        return configElement;
    }
    
    private void setConfig(Element configElement) {        
        if (null != this.getExtractorBeanName()) {
            configElement.setAttribute("extractorBean", this.getExtractorBeanName());
        }
        if (null != this.getCodeCategoryItems()) {
            configElement.setAttribute("codeCategoryItems", this.getCodeCategoryItems());
        }
    }
    
    @Override
    protected void initItems() {
        if (null != this.getExtractorBeanName()) {
            try {
                EnumeratorMapCategoryAttributeItemsExtractor extractor = 
                        (EnumeratorMapCategoryAttributeItemsExtractor) this.getBeanFactory().getBean(this.getExtractorBeanName(), EnumeratorMapCategoryAttributeItemsExtractor.class);
                if (null != extractor) {
                    extractor.setCodeCategoryItems(this.getCodeCategoryItems());
                    List<SelectItem> items = extractor.getMapItems();
                    if (items != null && items.size() > 0) {
                        this.addExtractedItems(items);
                    }
                }
            } catch (Throwable t) {
                _logger.error("Error while extract items from bean extractor '{}'", this.getExtractorBeanName(), t);
            }
        }
    }
    
    private void addExtractedItems(List<SelectItem> items) {
        SelectItem[] values = null;
        if (null == this.getMapItems() || this.getMapItems().length == 0) {
            values = new SelectItem[items.size()];
            for (int i = 0; i < items.size(); i++) {
                SelectItem item = items.get(i);
                values[i] = item;
            }
        } else {
            values = new SelectItem[this.getMapItems().length + items.size()];
            for (int i = 0; i < this.getMapItems().length; i++) {
                SelectItem item = this.getMapItems()[i];
                values[i] = item;
            }
            for (int i = 0; i < items.size(); i++) {
                SelectItem item = items.get(i);
                values[i + this.getMapItems().length] = item;
            }
        }
        this.setMapItems(values);
    }
    
    
    @Override
    public void setAttributeConfig(Element attributeElement) throws ApsSystemException {
        super.setAttributeConfig(attributeElement);
        String codeCategory = this.extractXmlAttribute(attributeElement, "codeCategoryItems", false);
        if (null != codeCategory) {
            this.setCodeCategoryItems(codeCategory.trim());
        }
        String extractorBeanName = this.extractXmlAttribute(attributeElement, "extractorBean", false);
        this.setExtractorBeanName(extractorBeanName);
        this.initItems();
    }
    
    @Override
    protected AbstractJAXBAttribute getJAXBAttributeInstance() {
        return new JAXBEnumeratorMapCategoryAttribute();
    }
    
    @Override
    public AbstractJAXBAttribute getJAXBAttribute(String langCode) {
        JAXBEnumeratorMapCategoryAttribute jaxbAttribute = (JAXBEnumeratorMapCategoryAttribute)
                super.createJAXBAttribute(langCode);
        
        JAXBEnumeratorMapValue value = new JAXBEnumeratorMapValue();
        value.setKey(this.getMapKey());
        value.setValue(this.getMapValue());
        jaxbAttribute.setMapValue(value);
        jaxbAttribute.setCodeCategoryItems(getCodeCategoryItems());
        return jaxbAttribute;
    }
    
     @Override
    public JAXBEnumeratorMapCategoryAttributeType getJAXBAttributeType() {
        JAXBEnumeratorMapCategoryAttributeType jaxbAttribute = 
                (JAXBEnumeratorMapCategoryAttributeType) super.getJAXBAttributeType();
        jaxbAttribute.setCustomSeparator(this.getCustomSeparator());
        jaxbAttribute.setExtractorBeanName(this.getExtractorBeanName());
        jaxbAttribute.setStaticItems(this.getStaticItems());
        jaxbAttribute.setCodeCategoryItems(this.getCodeCategoryItems());
        return jaxbAttribute;
    }

    @Override
    protected DefaultJAXBAttributeType getJAXBAttributeTypeInstance() {
        return new JAXBEnumeratorMapCategoryAttributeType();
    }

    
    
    public String getCodeCategoryItems() {
        return _codeCategoryItems;
    }

    public void setCodeCategoryItems(String _codeCategoryItems) {
        this._codeCategoryItems = _codeCategoryItems;
    }
    
    private String _codeCategoryItems;
}
