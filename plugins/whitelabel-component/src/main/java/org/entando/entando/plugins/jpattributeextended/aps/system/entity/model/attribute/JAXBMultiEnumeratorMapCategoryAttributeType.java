/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package org.entando.entando.plugins.jpattributeextended.aps.system.entity.model.attribute;

import com.agiletec.aps.system.common.entity.model.attribute.AttributeInterface;
import com.agiletec.aps.system.common.entity.model.attribute.JAXBEnumeratorAttributeType;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlRootElement;
import org.entando.entando.aps.system.services.api.model.ApiException;
import org.entando.entando.plugins.jpattributeextended.aps.system.entity.model.MultiEnumeratorMapCategoryAttribute;

/**
 *
 * @author Entando
 */
@XmlRootElement(name = "attributeType")
public class JAXBMultiEnumeratorMapCategoryAttributeType extends JAXBEnumeratorAttributeType {
    
    @Override
    public AttributeInterface createAttribute(Map<String, AttributeInterface> attributes) throws ApiException {
        MultiEnumeratorMapCategoryAttribute attribute =
                (MultiEnumeratorMapCategoryAttribute) super.createAttribute(attributes);
        
        attribute.setCodeCategoryItems(this.getCodeCategoryItems());
        attribute.setValues(this.getValues());
        return attribute;
    }
    
    public List<String> getValues() {
        return values;
    }
    
    public void setValues(List<String> values) {
        this.values = values;
    }
    
    public String getCodeCategoryItems() {
        return _codeCategoryItems;
    }
    
    public void setCodeCategoryItems(String _codeCategoryItems) {
        this._codeCategoryItems = _codeCategoryItems;
    }
    
    private String _codeCategoryItems;
    
    private List<String> values = new ArrayList<>();
    
}
