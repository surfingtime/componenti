package org.entando.entando.plugins.jacms.aps.system.services.api;

import com.agiletec.aps.system.SystemConstants;
import com.agiletec.aps.system.common.entity.model.EntitySearchFilter;
import com.agiletec.aps.system.exception.ApsSystemException;
import com.agiletec.aps.system.services.user.UserDetails;
import com.agiletec.plugins.jacms.aps.system.services.content.helper.IContentListHelper;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Properties;
import javax.ws.rs.core.Response;
import org.apache.commons.lang3.StringUtils;
import org.entando.entando.aps.system.services.api.IApiErrorCodes;
import org.entando.entando.aps.system.services.api.model.ApiException;
import org.entando.entando.plugins.jacms.aps.system.services.api.model.ApiContentListBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Classe che estende AbstractCmsApiInterface
 * Questa estensione è stata necessaria per la creazione di una nuova API  "getContentsExtended".
 * La nuova API ha lo scopo di estendere la ricerca dei contenuti anche sugli attributi Monolist il cui NestedAttribute è LinkSearchable
 * @author emarcias
 */
public class ApiContentInterfaceExtended extends AbstractCmsApiInterface {

    private static final Logger _logger = LoggerFactory.getLogger(ApiContentInterfaceExtended.class);

    public List<String> getContentsExtended(Properties properties) throws Throwable {
        return this.extractContents(properties);
    }

    protected List<String> extractContents(Properties properties) throws Throwable {
        List<String> contentsId = null;
        try {
            ApiContentListBean bean = this.buildSearchBean(properties);
            UserDetails user = (UserDetails) properties.get(SystemConstants.API_USER_PARAMETER);
            contentsId = this.getContentListHelper().getContentsId(bean, user);
        } catch (ApiException ae) {
            throw ae;
        } catch (Throwable t) {
            _logger.error("error in extractContents", t);
            throw new ApsSystemException("Error into API method", t);
        }
        return contentsId;
    }

    protected ApiContentListBean buildSearchBean(Properties properties) throws ApiException, Throwable {
        ApiContentListBean bean = null;
        try {
            String contentType = properties.getProperty("contentType");
            if (null == this.getContentManager().getSmallContentTypesMap().get(contentType)) {
                throw new ApiException(IApiErrorCodes.API_PARAMETER_VALIDATION_ERROR, "Content Type '" + contentType + "' does not exist", Response.Status.CONFLICT);
            }
            String langCode = properties.getProperty(SystemConstants.API_LANG_CODE_PARAMETER);
            String filtersParam = properties.getProperty("filters");
            if (!StringUtils.isEmpty(filtersParam)) {
                filtersParam = URLDecoder.decode(filtersParam, StandardCharsets.UTF_8.toString());
            }
            EntitySearchFilter[] filters = this.getContentListHelper().getFilters(contentType, filtersParam, langCode);
            String[] categoryCodes = null;
            String categoriesParam = properties.getProperty("categories");
            boolean orClause = false;
            if (!StringUtils.isEmpty(categoriesParam)) {
                categoryCodes = categoriesParam.split(IContentListHelper.CATEGORIES_SEPARATOR);
                String orClauseString = properties.getProperty("orClauseCategoryFilter");
                orClause = !StringUtils.isEmpty(orClauseString) && orClauseString.trim().equalsIgnoreCase("true");
            }
            bean = new ApiContentListBean(contentType, filters, categoryCodes, orClause);
        } catch (ApiException ae) {
            throw ae;
        } catch (Throwable t) {
            _logger.error("error in buildSearchBean", t);
            throw new ApsSystemException("Error into API method", t);
        }
        return bean;
    }

    protected IContentListHelper getContentListHelper() {
        return _contentListHelper;
    }

    public void setContentListHelper(IContentListHelper contentListHelper) {
        this._contentListHelper = contentListHelper;
    }

    private IContentListHelper _contentListHelper;

}
