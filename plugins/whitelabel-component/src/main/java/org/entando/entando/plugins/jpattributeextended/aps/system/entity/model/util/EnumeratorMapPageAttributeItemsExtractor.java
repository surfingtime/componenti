package org.entando.entando.plugins.jpattributeextended.aps.system.entity.model.util;

import com.agiletec.aps.util.SelectItem;
import java.util.List;

/**
 * Base interface for those bean classes that must extract 'EnumeratorMapPage'
 * Attribute.
 *
 * @author emarcias
 */
public interface EnumeratorMapPageAttributeItemsExtractor {

    /**
     * Return the list of the items of the 'EnumeratorMapPage' attribute.
     *
     * @return The items list.
     */
    public List<SelectItem> getMapItems();

    /**
     * Setta e restituisce la configurazione del codice della pagina/e
     * @param _codesPages
     */
    public void setCodePageItems(String _codesPages);

    public String getCodePageItems();
    
    
    /**
     * Setta e restituisce la configurazione del booleano per restituire i figli di una pagina
     * @param _showChildsPage
     */
    public void setShowChildsPage(boolean _showChildsPage);
    
    public boolean isShowChildsPage();

}
