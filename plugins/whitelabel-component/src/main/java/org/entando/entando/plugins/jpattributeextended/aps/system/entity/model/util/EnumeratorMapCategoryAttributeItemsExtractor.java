package org.entando.entando.plugins.jpattributeextended.aps.system.entity.model.util;

import com.agiletec.aps.util.SelectItem;
import java.util.List;

/**
 * Base interface for those bean classes that must extract 'EnumeratorMapCategory'
 * Attribute.
 * 
 * @author emarcias
 */
public interface EnumeratorMapCategoryAttributeItemsExtractor {
    
    
    /**
     * Return the list of the items of the 'EnumeratorMapCategory' attribute.
     *
     * @return The items list.
     */
    public List<SelectItem> getMapItems();

    /**
     * Setta e restituisce la configurazione del codice della categoria
     * @param _codeCategory
     */
    public void setCodeCategoryItems(String _codeCategory);

    public String getCodeCategoryItems();
    
}
