/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.consulmedia.ponmetroca.aps.tags;

import com.agiletec.aps.system.RequestContext;
import com.agiletec.aps.system.SystemConstants;
import com.agiletec.aps.system.exception.ApsSystemException;
import com.agiletec.aps.system.services.page.IPage;
import com.agiletec.aps.system.services.page.IPageManager;
import com.agiletec.aps.system.services.page.Widget;
import com.agiletec.aps.util.ApsProperties;
import com.agiletec.aps.util.ApsWebApplicationUtils;
import com.agiletec.plugins.jacms.aps.system.services.content.model.Content;
import javax.servlet.ServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Consulmedia
 */
public class CagliariMainFrameContentTag extends TagSupport {
    private static final Logger _logger = LoggerFactory.getLogger(CagliariMainFrameContentTag.class);

    public CagliariMainFrameContentTag() {
        super();
        this.release();
    }

    @Override
    public int doStartTag() throws JspException {
        ServletRequest request = this.pageContext.getRequest();
        RequestContext reqCtx = (RequestContext) request.getAttribute(RequestContext.REQCTX);
        try {
            this.pageContext.setAttribute(this.getVar(),
                                          this.extractContent(reqCtx));
        } catch (Throwable t) {
            _logger.error("Error detected while initializing the tag",
                          t);
            throw new JspException("Error detected while initializing the tag",
                                   t);
        }
        return super.doStartTag();
    }

    @Override
    public void release() {
        this.setVar(null);
    }

    protected Widget extractMainWidget(RequestContext reqCtx) {
        IPageManager pageManager = (IPageManager) ApsWebApplicationUtils.getBean(SystemConstants.PAGE_MANAGER,
                                                                                 this.pageContext);
        Widget mainWidget = null;
        IPage page = null;
        if (null != this.getPageCode()) {
            page = pageManager.getOnlinePage(this.getPageCode());
        } else {
            page = (IPage) reqCtx.getExtraParam(SystemConstants.EXTRAPAR_CURRENT_PAGE);
        }
        int mainFrame = page.getModel().getMainFrame();
        if (mainFrame >= 0 && page.getWidgets().length > mainFrame) {
            mainWidget = page.getWidgets()[mainFrame];
        }
        return mainWidget;
    }

    protected Content extractContent(RequestContext reqCtx)
            throws ApsSystemException {

        com.agiletec.plugins.jacms.aps.system.services.content.IContentManager contentManager = (com.agiletec.plugins.jacms.aps.system.services.content.IContentManager) ApsWebApplicationUtils.getBean(com.agiletec.plugins.jacms.aps.system.JacmsSystemConstants.CONTENT_MANAGER,
                                                                                          this.pageContext);

        Widget mainWidget = this.extractMainWidget(reqCtx);
        String contentId = "";
        if (null != mainWidget) {
            ApsProperties widgetConfig = mainWidget.getConfig();
            if (widgetConfig != null) {
                contentId = (String) widgetConfig.get(SystemConstants.K_CONTENT_ID_PARAM);
            }
        }
        if (null == contentId || contentId.trim().length() == 0) {
            contentId = reqCtx.getRequest().getParameter(SystemConstants.K_CONTENT_ID_PARAM);
        }

        return (Content) contentManager.getEntity(contentId);
    }

    public String getVar() {
        return _var;
    }

    public void setVar(String var) {
        this._var = var;
    }

    public String getPageCode() {
        return _pageCode;
    }

    public void setPageCode(String pageCode) {
        this._pageCode = pageCode;
    }

    private String _var;
    private String _pageCode;

}
