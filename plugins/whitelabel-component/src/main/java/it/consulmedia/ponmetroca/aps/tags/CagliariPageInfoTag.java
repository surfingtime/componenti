package it.consulmedia.ponmetroca.aps.tags;

import com.agiletec.aps.system.RequestContext;
import com.agiletec.aps.system.SystemConstants;
import com.agiletec.aps.system.services.lang.Lang;
import com.agiletec.aps.system.services.page.IPage;
import com.agiletec.aps.system.services.page.IPageManager;
import com.agiletec.aps.system.services.page.Widget;
import com.agiletec.aps.tags.PageInfoTag;
import com.agiletec.aps.tags.util.IParameterParentTag;
import com.agiletec.aps.util.ApsWebApplicationUtils;

import javax.servlet.ServletRequest;
import javax.servlet.jsp.JspException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author emarcias
 */
public class CagliariPageInfoTag extends PageInfoTag implements IParameterParentTag {

    private static final Logger _logger = LoggerFactory.getLogger(CagliariPageInfoTag.class);

    public CagliariPageInfoTag() {
        super();
        this.release();
    }

    @Override
    public int doEndTag() throws JspException {
        ServletRequest request = this.pageContext.getRequest();
        RequestContext reqCtx = (RequestContext) request.getAttribute(RequestContext.REQCTX);
        Lang currentLang = (Lang) reqCtx.getExtraParam(SystemConstants.EXTRAPAR_CURRENT_LANG);
        try {
            IPageManager pageManager = (IPageManager) ApsWebApplicationUtils.getBean(SystemConstants.PAGE_MANAGER, this.pageContext);
            if (null != this.getPageCode() && this.getPageCode().startsWith("Page: ")) {
                // in case of toString of page
                this.setPageCode(this.getPageCode().substring(6));
            }
            IPage page = pageManager.getOnlinePage(this.getPageCode());
            if (null == page) {
                _logger.info("Required info for null public page : inserted code '{}'", this.getPageCode());
                page = pageManager.getDraftPage(this.getPageCode());
                if (null == page) {
                    _logger.error("Required info for null draft page : inserted code '{}'", this.getPageCode());
                    this.release();
                    return EVAL_PAGE;
                }
            }
            if (this.getInfo() == null || this.getInfo().equals(CODE_INFO)) {
                this.setValue(page.getCode());
            } else if (this.getInfo().equals(TITLE_INFO)) {
                this.extractPageTitle(page, currentLang);
            } else if (this.getInfo().equals(URL_INFO)) {
                this.extractPageUrl(page, currentLang, reqCtx);
            } else if (this.getInfo().equals(OWNER_INFO)) {
                this.extractPageOwner(page, reqCtx);
            } else if (this.getInfo().equals(CHILD_OF_INFO)) {
                this.extractIsChildOfTarget(page, pageManager);
            } else if (this.getInfo().equals(HAS_CHILD)) {
                boolean hasChild = (page.getChildrenCodes() != null && page.getChildrenCodes().length > 0);
                this.setValue(new Boolean(hasChild).toString());
            } else if (this.getInfo().equals(IS_PUBLISH_ON_FLY)) {
                this.extractIsPublishOnFly(page);
            } else if (this.getInfo().equals(PATH_ARRAY) && null != this.getVar()) {
                this.pageContext.setAttribute(this.getVar(), page.getPathArray(pageManager));
                if (null != this.getPageVar()) {
                    this.pageContext.setAttribute(this.getPageVar(), page);
                }
                this.release();
                return EVAL_PAGE;
            }
            if (null != this.getPageVar()) {
                this.pageContext.setAttribute(this.getPageVar(), page);
            }
            this.evalValue();
        } catch (Throwable t) {
            _logger.error("Error during tag initialization", t);
            throw new JspException("Error during tag initialization ", t);
        }
        this.release();
        return EVAL_PAGE;
    }

    protected void extractIsPublishOnFly(IPage page) {
        this.setValue("false");
        int mainFrame = page.getModel().getMainFrame();
        Widget mainWidget = null;
        if (mainFrame >= 0 && page.getWidgets().length > mainFrame) {
            mainWidget = page.getWidgets()[mainFrame];
            if (null != mainWidget && CONTENT_VIEWER_CODE.equals(mainWidget.getType().getCode()) && null == mainWidget.getConfig()) {
                this.setValue("true");
            }
        }
    }

    public String getPageVar() {
        return pageVar;
    }

    public void setPageVar(String pageVar) {
        this.pageVar = pageVar;
    }

    private String pageVar;

    public static final String PATH_ARRAY = "pathArray";
    public static final String IS_PUBLISH_ON_FLY = "isPublishOnFly";
    public static final String CONTENT_VIEWER_CODE = "content_viewer";
    
}
