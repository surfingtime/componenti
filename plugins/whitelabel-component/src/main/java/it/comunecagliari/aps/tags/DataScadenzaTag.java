package it.comunecagliari.aps.tags;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author emarcias
 */
public class DataScadenzaTag extends TagSupport {

    private static final Logger _logger = LoggerFactory.getLogger(DataScadenzaTag.class);

    private static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    
    @Override
    public int doStartTag() throws JspException {
        boolean isScaduta = false;
        try {            
            Date dataScadenza = sdf.parse(this.getDate());             
            Date dataAttuale = sdf.parse(this.getDataAttuale());
            if (!dataScadenza.after(dataAttuale)) {
                if (!dataScadenza.equals(dataAttuale)) {
                    isScaduta = true;
                }
            }
        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(DataScadenzaTag.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (null != this.getVar()) {
            this.pageContext.setAttribute(this.getVar(), isScaduta);
        }
        return super.doStartTag();
    }
    
    
    public String getDataAttuale(){
        return sdf.format(new Date());        
    }

    @Override
    public void release() {
        this.setVar(null);
    }

    public String getVar() {
        return _var;
    }

    public void setVar(String var) {
        this._var = var;
    }

    public String getDate() {
        return _date;
    }

    public void setDate(String _date) {
        this._date = _date;
    }

    private String _var;
    private String _date;
}
