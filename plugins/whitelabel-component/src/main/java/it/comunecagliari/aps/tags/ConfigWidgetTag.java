/*
 * Copyright 2015-Present Entando Inc. (http://www.entando.com) All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package it.comunecagliari.aps.tags;

import javax.servlet.ServletRequest;
import javax.servlet.jsp.JspException;

import org.entando.entando.aps.system.services.widgettype.WidgetType;
import org.entando.entando.aps.tags.ExtendedTagSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.agiletec.aps.system.RequestContext;
import com.agiletec.aps.system.SystemConstants;
import com.agiletec.aps.system.services.lang.ILangManager;
import com.agiletec.aps.system.services.lang.Lang;
import com.agiletec.aps.system.services.page.IPage;
import com.agiletec.aps.system.services.page.IPageManager;
import com.agiletec.aps.system.services.page.Widget;
import com.agiletec.aps.util.ApsProperties;
import com.agiletec.aps.util.ApsWebApplicationUtils;

/**
 * Returns informations about the showlet where the tag resides. The "param"
 * attribute acceptes the following values: - "code" returns the code of the
 * associated showlet type (empty if none associated)<br/>
 * - "title" returns the name of the associated showlet type (empty if none
 * associated)<br/>
 * - "config" returns the value of the configuration parameter declared in the
 * "configParam" attribute<br/>
 * To obtain information about a showlet placed in a frame other than the
 * current, use the "frame" attribute.
 *
 * @author E.Santoboni - E.Mezzano - G.Lonis
 */
@SuppressWarnings("serial")
public class ConfigWidgetTag extends ExtendedTagSupport {

    private static final Logger _logger = LoggerFactory.getLogger(ConfigWidgetTag.class);

    @Override
    public int doStartTag() throws JspException {
        try {
            Widget widget = this.extractWidget();
            if (null == widget) {
                return super.doStartTag();
            }
            String value = null;
            ApsProperties config = widget.getConfig();
            if (null != config) {
                value = config.getProperty(this.getConfigParam());
            }

            if (null != value) {
                String var = this.getVar();
                if (null == var || "".equals(var)) {
                    if (this.getEscapeXml()) {
                        out(this.pageContext, this.getEscapeXml(), value);
                    } else {
                        this.pageContext.getOut().print(value);
                    }
                } else {
                    this.pageContext.setAttribute(this.getVar(), value);
                }
            }
        } catch (Throwable t) {
            _logger.error("Error detected during widget preprocessing", t);
            String msg = "Error detected during widget preprocessing";
            throw new JspException(msg, t);
        }
        return super.doStartTag();
    }

    private Widget extractWidget() throws JspException {
        ServletRequest req = this.pageContext.getRequest();
        RequestContext reqCtx = (RequestContext) req.getAttribute(RequestContext.REQCTX);
        Widget widget = null;
        try {
            IPageManager pageManager = (IPageManager) ApsWebApplicationUtils.getBean(SystemConstants.PAGE_MANAGER, this.pageContext);
            IPage page = pageManager.getOnlinePage(this.getPageCode());
            if (null == page) {
                _logger.error("Required info for null page : inserted code '{}'", this.getPageCode());
            }
            Widget[] widgets = page.getWidgets();
            if (widgets != null) {
                for (Widget wdg : widgets) {
                    if (wdg != null) {
                        if (wdg.getType().getCode().equals(getWidgetCode())) {
                            widget = wdg;
                        }
                    }
                }
            }
        } catch (Throwable t) {
            _logger.error("Error during tag initialization", t);
            throw new JspException("Error during tag initialization ", t);
        }
        return widget;
    }

    @Override
    public void release() {
        super.release();
        this._var = null;
        this._pageCode = null;
        this._widgetCode = null;
        this._configParam = null;
    }

    public String getVar() {
        return _var;
    }

    public void setVar(String var) {
        this._var = var;
    }

    public String getPageCode() {
        return _pageCode;
    }

    public void setPageCode(String pageCode) {
        this._pageCode = pageCode;
    }

    public String getWidgetCode() {
        return _widgetCode;
    }

    public void setWidgetCode(String _widgetCode) {
        this._widgetCode = _widgetCode;
    }

    public String getConfigParam() {
        return _configParam;
    }

    public void setConfigParam(String configParam) {
        this._configParam = configParam;
    }

    private String _var;
    private String _pageCode;
    private String _widgetCode;
    private String _configParam;
}
