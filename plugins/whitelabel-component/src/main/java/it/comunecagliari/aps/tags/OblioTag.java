package it.comunecagliari.aps.tags;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author emarcias
 */
public class OblioTag extends TagSupport {
    
    private static final Logger _logger = LoggerFactory.getLogger(OblioTag.class);

    private static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    
    @Override
    public int doStartTag() throws JspException {
        boolean isOblio = false;
        try {  
            // se son passati 3 anni dalla data di scadenza
            Date dataScadenza = sdf.parse(this.getDate());
            //System.out.println(sdf.format(dataScadenza)+"****** DATA iniziale");            
            GregorianCalendar cal = new GregorianCalendar();
            cal.setTime(dataScadenza);
            cal.add(Calendar.YEAR, 3);
            cal.set(cal.get(Calendar.YEAR), Calendar.DECEMBER, 31);
            Date dataOblio = cal.getTime();              
            Date dataAttuale = sdf.parse(this.getDataAttuale());            
            //System.out.println(sdf.format(dataOblio)+"****** DATA calcolata dopo 3 anni");
                       
            if (!dataOblio.after(dataAttuale)) {
                if (!dataOblio.equals(dataAttuale)) {
                    isOblio = true;
                }
            }
        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(OblioTag.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (null != this.getVar()) {
            this.pageContext.setAttribute(this.getVar(), isOblio);
        }
        return super.doStartTag();
    }
    
    
    public String getDataAttuale(){
        return sdf.format(new Date());        
    }

    @Override
    public void release() {
        this.setVar(null);
    }

    public String getVar() {
        return _var;
    }

    public void setVar(String var) {
        this._var = var;
    }

    public String getDate() {
        return _date;
    }

    public void setDate(String _date) {
        this._date = _date;
    }

    private String _var;
    private String _date;
    
}
