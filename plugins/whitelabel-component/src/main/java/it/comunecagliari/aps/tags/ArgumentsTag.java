package it.comunecagliari.aps.tags;

import com.agiletec.aps.system.RequestContext;
import com.agiletec.aps.system.SystemConstants;
import com.agiletec.aps.system.common.entity.model.attribute.AttributeInterface;
import com.agiletec.aps.system.exception.ApsSystemException;
import com.agiletec.aps.util.ApsWebApplicationUtils;
import com.agiletec.plugins.jacms.aps.system.JacmsSystemConstants;
import com.agiletec.plugins.jacms.aps.system.services.content.IContentManager;
import com.agiletec.plugins.jacms.aps.system.services.content.model.Content;
import javax.servlet.ServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author emarcias
 */
public class ArgumentsTag  extends TagSupport{
    private static final Logger _logger = LoggerFactory.getLogger(ArgumentsTag.class);
    
    public ArgumentsTag() {
        super();
        this.release();
    }
    
    @Override
    public int doStartTag() throws JspException {
        ServletRequest request = this.pageContext.getRequest();
        RequestContext reqCtx = (RequestContext) request.getAttribute(RequestContext.REQCTX);
        try {
            this.pageContext.setAttribute(this.getVar(), this.extractCategory(reqCtx));
        } catch (Throwable t) {
            _logger.error("Error detected while initializing the tag", t);
            throw new JspException("Error detected while initializing the tag", t);
        }
        return super.doStartTag();
    }
    
    protected String extractCategory(RequestContext reqCtx) throws ApsSystemException {
        
        String categoryCode = reqCtx.getRequest().getParameter("categoryCode");
        if(categoryCode == null) {
            String contentId = reqCtx.getRequest().getParameter(SystemConstants.K_CONTENT_ID_PARAM);
            if(contentId != null && !contentId.trim().equalsIgnoreCase("")) {
                String prova = contentId.substring(0,3);
                int prova2 = contentId.length();
                if(contentId.length()> 2 && contentId.substring(0,3).equalsIgnoreCase("ARG")){
                    IContentManager contentManager = (IContentManager) ApsWebApplicationUtils.getBean(JacmsSystemConstants.CONTENT_MANAGER, this.pageContext);
                    Content content = (Content) contentManager.getEntity(contentId);
                    AttributeInterface attribute = content.getAttributeByRole("jpattributeextended:argomento");
                    if(attribute != null)
                        categoryCode = attribute.getValue().toString();                              
                }
            }
        }              
        return categoryCode;
    }
    
    @Override
    public void release() {
        this.setVar(null);
    }
    
    public String getVar() {
        return _var;
    }

    public void setVar(String var) {
        this._var = var;
    }
    
    private String _var;
}
