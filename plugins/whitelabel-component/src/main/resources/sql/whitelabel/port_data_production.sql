INSERT INTO widgetcatalog (code, titles, parameters, plugincode, parenttypecode, defaultconfig, locked, maingroup) VALUES ('cagliari_widget_argomento_dettaglio', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Cagliari - Argomenti dettaglio</property>
<property key="it">Cagliari - Argomenti dettaglio</property>
</properties>

', NULL, 'whitelabel', NULL, NULL, 1, 'free');
INSERT INTO guifragment (code, widgettypecode, plugincode, gui, defaultgui, locked) VALUES ('cagliari_widget_argomento_dettaglio', 'cagliari_widget_argomento_dettaglio', NULL, '<#assign jacms=JspTaglibs["/jacms-aps-core"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign cw=JspTaglibs["/comuneCagliari-whitelabel"]>

<@cw.currentArgument var="currentArg" />
<#if (currentArg??)>
    <@cw.contentList listName="contentList" contentType="ARG" category="${currentArg}"/>
    <#if (contentList??) && (contentList?has_content) && (contentList?size > 0)>
        <#list contentList as contentId>
            <@jacms.content contentId="${contentId}" />
        </#list>
    <#else>
    	<p class="alert alert-info"><@wp.i18n key="LIST_VIEWER_EMPTY" /></p>
    </#if>
</#if>
<#assign contentList="">

', NULL, 1);
INSERT INTO widgetcatalog (code,titles,parameters,plugincode,parenttypecode,defaultconfig,locked,maingroup) VALUES 
('cagliari_widget_lista_multilayout','<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Cagliari - Lista multi-layout</property>
<property key="it">Cagliari - Lista multi-layout</property>
</properties>','<config>
	<parameter name="contentType">Content Type (mandatory)</parameter>
        <parameter name="layout">Layout of content list</parameter>
	<parameter name="modelId">Content Model</parameter>
	<parameter name="modelId2">Content Model 2</parameter>
	<parameter name="userFilters">Front-End user filter options</parameter>
	<parameter name="category">Content Category **deprecated**</parameter>
	<parameter name="categories">Content Category codes (comma separeted)</parameter>
	<parameter name="orClauseCategoryFilter" />	
	<parameter name="maxElements">Number of contents</parameter>
	<parameter name="maxElemForItem">Contents for each page</parameter>
	<parameter name="filters" />
	<parameter name="title_{lang}">Widget Title in lang {lang}</parameter>
	<parameter name="pageLink">The code of the Page to link</parameter>
	<parameter name="linkDescr_{lang}">Link description in lang {lang}</parameter>
	<action name="listViewerConfigExt"/>
</config>','whitelabel',NULL,NULL,1,NULL);
INSERT INTO guifragment (code, widgettypecode, plugincode, gui, defaultgui, locked) VALUES ('cagliari_widget_lista_multilayout_uf_ent_Enumer', NULL, 'whitelabel', NULL, NULL, 1);
INSERT INTO guifragment (code, widgettypecode, plugincode, gui, defaultgui, locked) VALUES ('cagliari_widget_lista_multilayout_uf_ent_EnumerMap', NULL, 'whitelabel', NULL, NULL, 1);
INSERT INTO guifragment (code, widgettypecode, plugincode, gui, defaultgui, locked) VALUES ('cagliari_widget_lista_multilayout_uf_ent_Number', NULL, 'whitelabel', NULL, NULL, 1);
INSERT INTO guifragment (code, widgettypecode, plugincode, gui, defaultgui, locked) VALUES ('cagliari_widget_lista_multilayout_uf_met_category', NULL, 'whitelabel', NULL, NULL, 1);
INSERT INTO guifragment (code, widgettypecode, plugincode, gui, defaultgui, locked) VALUES ('cagliari_widget_lista_multilayout_uf_met_fulltext', NULL, 'whitelabel', NULL, NULL, 1);
INSERT INTO guifragment (code, widgettypecode, plugincode, gui, defaultgui, locked) VALUES ('cagliari_widget_lista_multilayout_uf_ent_Bool_io', NULL, 'whitelabel', NULL, NULL, 1);
INSERT INTO guifragment (code, widgettypecode, plugincode, gui, defaultgui, locked) VALUES ('cagliari_widget_lista_multilayout_uf_ent_ThreeSt', NULL, 'whitelabel', NULL, NULL, 1);
INSERT INTO guifragment (code, widgettypecode, plugincode, gui, defaultgui, locked) VALUES ('cagliari_widget_lista_multilayout_uf_ent_Boolean', NULL, 'whitelabel', NULL, NULL, 1);
INSERT INTO guifragment (code, widgettypecode, plugincode, gui, defaultgui, locked) VALUES ('cagliari_widget_lista_multilayout_uf_ent_CheckBox', NULL, 'whitelabel', NULL, NULL, 1);
INSERT INTO guifragment (code, widgettypecode, plugincode, gui, defaultgui, locked) VALUES ('cagliari_widget_lista_multilayout_uf', NULL, 'whitelabel', '<#assign wp=JspTaglibs["/aps-core"]>
<#if (userFilterOptionsVar??) && userFilterOptionsVar?has_content && (userFilterOptionsVar?size > 0)>
	<#assign hasUserFilterError = false >
	<#list userFilterOptionsVar as userFilterOptionVar>
		<#if (userFilterOptionVar.formFieldErrors??) && userFilterOptionVar.formFieldErrors?has_content && (userFilterOptionVar.formFieldErrors?size > 0)>
			<#assign hasUserFilterError = true >
		</#if>
	</#list>
	<#if (hasUserFilterError)>
	<div class="alert alert-error">
		<a class="close" data-dismiss="alert" href="#"><i class="icon-remove"></i></a>
		<h2 class="alert-heading"><@wp.i18n key="ERRORS" /></h2>
		<ul>
			<#list userFilterOptionsVar as userFilterOptionVar>
				<#if (userFilterOptionVar.formFieldErrors??) && (userFilterOptionVar.formFieldErrors?size > 0)>
					<#assign formFieldErrorsVar = userFilterOptionVar.formFieldErrors >
					<#list formFieldErrorsVar?keys as formFieldErrorKey>
						<li>
							<@wp.i18n key="jacms_LIST_VIEWER_FIELD" />&#32;<em>${formFieldErrorsVar[formFieldErrorKey].attributeName}</em><#if (formFieldErrorsVar[formFieldErrorKey].rangeFieldType??)>:&#32;<em><@wp.i18n key="${formFieldErrorsVar[formFieldErrorKey].rangeFieldType}" /></em></#if>&#32;<@wp.i18n key="${formFieldErrorsVar[formFieldErrorKey].errorKey}" />
						</li>
					</#list>
				</#if>
			</#list>	
		</ul>
	</div>
	</#if>
	<#assign hasUserFilterError = false >


	<#list userFilterOptionsVar as userFilterOptionVar>
		<@wp.freemarkerTemplateParameter var="userFilterOptionVar" valueName="userFilterOptionVar" removeOnEndTag=true >
			<#if userFilterOptionVar.attributeFilter >
				<#if userFilterOptionVar.attribute.type == "Monotext" || userFilterOptionVar.attribute.type == "Text" || userFilterOptionVar.attribute.type == "Longtext" || userFilterOptionVar.attribute.type == "Hypertext">
					<@wp.fragment code="cagliari_widget_lista_multilayout_uf_ent_Text" escapeXml=false />
				</#if>
				<#if userFilterOptionVar.attribute.type == "Enumerator" >
					<@wp.fragment code="cagliari_widget_lista_multilayout_uf_ent_Enumer" escapeXml=false />
				</#if>
				<#if userFilterOptionVar.attribute.type == "EnumeratorMap" >
					<@wp.fragment code="cagliari_widget_lista_multilayout_uf_ent_EnumerMap" escapeXml=false />
				</#if>
				<#if userFilterOptionVar.attribute.type == "Number">
					<@wp.fragment code="cagliari_widget_lista_multilayout_uf_ent_Number" escapeXml=false />
				</#if>
				<#if userFilterOptionVar.attribute.type == "Date">
					<@wp.fragment code="cagliari_widget_lista_multilayout_uf_ent_Date" escapeXml=false />
				</#if>
				<#if userFilterOptionVar.attribute.type == "Boolean">
					<@wp.fragment code="cagliari_widget_lista_multilayout_uf_ent_Boolean" escapeXml=false />
				</#if>
				<#if userFilterOptionVar.attribute.type == "CheckBox">
					<@wp.fragment code="cagliari_widget_lista_multilayout_uf_ent_CheckBox" escapeXml=false />
				</#if>
				<#if userFilterOptionVar.attribute.type == "ThreeState">
					<@wp.fragment code="cagliari_widget_lista_multilayout_uf_ent_ThreeSt" escapeXml=false />
				</#if>
			</#if>
		</@wp.freemarkerTemplateParameter>
	</#list>
</#if>', '<#assign wp=JspTaglibs["/aps-core"]>
<#if (userFilterOptionsVar??) && userFilterOptionsVar?has_content && (userFilterOptionsVar?size > 0)>
	<#assign hasUserFilterError = false >
	<#list userFilterOptionsVar as userFilterOptionVar>
		<#if (userFilterOptionVar.formFieldErrors??) && userFilterOptionVar.formFieldErrors?has_content && (userFilterOptionVar.formFieldErrors?size > 0)>
			<#assign hasUserFilterError = true >
		</#if>
	</#list>
	<#if (hasUserFilterError)>
	<div class="alert alert-error">
		<a class="close" data-dismiss="alert" href="#"><i class="icon-remove"></i></a>
		<h2 class="alert-heading"><@wp.i18n key="ERRORS" /></h2>
		<ul>
			<#list userFilterOptionsVar as userFilterOptionVar>
				<#if (userFilterOptionVar.formFieldErrors??) && (userFilterOptionVar.formFieldErrors?size > 0)>
					<#assign formFieldErrorsVar = userFilterOptionVar.formFieldErrors >
					<#list formFieldErrorsVar?keys as formFieldErrorKey>
						<li>
							<@wp.i18n key="jacms_LIST_VIEWER_FIELD" />&#32;<em>${formFieldErrorsVar[formFieldErrorKey].attributeName}</em><#if (formFieldErrorsVar[formFieldErrorKey].rangeFieldType??)>:&#32;<em><@wp.i18n key="${formFieldErrorsVar[formFieldErrorKey].rangeFieldType}" /></em></#if>&#32;<@wp.i18n key="${formFieldErrorsVar[formFieldErrorKey].errorKey}" />
						</li>
					</#list>
				</#if>
			</#list>	
		</ul>
	</div>
	</#if>
	<#assign hasUserFilterError = false >


	<#list userFilterOptionsVar as userFilterOptionVar>
		<@wp.freemarkerTemplateParameter var="userFilterOptionVar" valueName="userFilterOptionVar" removeOnEndTag=true >
			<#if userFilterOptionVar.attributeFilter >
				<#if userFilterOptionVar.attribute.type == "Monotext" || userFilterOptionVar.attribute.type == "Text" || userFilterOptionVar.attribute.type == "Longtext" || userFilterOptionVar.attribute.type == "Hypertext">
					<@wp.fragment code="cagliari_widget_lista_multilayout_uf_ent_Text" escapeXml=false />
				</#if>
				<#if userFilterOptionVar.attribute.type == "Enumerator" >
					<@wp.fragment code="cagliari_widget_lista_multilayout_uf_ent_Enumer" escapeXml=false />
				</#if>
				<#if userFilterOptionVar.attribute.type == "EnumeratorMap" >
					<@wp.fragment code="cagliari_widget_lista_multilayout_uf_ent_EnumerMap" escapeXml=false />
				</#if>
				<#if userFilterOptionVar.attribute.type == "Number">
					<@wp.fragment code="cagliari_widget_lista_multilayout_uf_ent_Number" escapeXml=false />
				</#if>
				<#if userFilterOptionVar.attribute.type == "Date">
					<@wp.fragment code="cagliari_widget_lista_multilayout_uf_ent_Date" escapeXml=false />
				</#if>
				<#if userFilterOptionVar.attribute.type == "Boolean">
					<@wp.fragment code="cagliari_widget_lista_multilayout_uf_ent_Boolean" escapeXml=false />
				</#if>
				<#if userFilterOptionVar.attribute.type == "CheckBox">
					<@wp.fragment code="cagliari_widget_lista_multilayout_uf_ent_CheckBox" escapeXml=false />
				</#if>
				<#if userFilterOptionVar.attribute.type == "ThreeState">
					<@wp.fragment code="cagliari_widget_lista_multilayout_uf_ent_ThreeSt" escapeXml=false />
				</#if>
			</#if>
		</@wp.freemarkerTemplateParameter>
	</#list>
</#if>', 1);
INSERT INTO guifragment (code, widgettypecode, plugincode, gui, defaultgui, locked) VALUES ('cagliari_widget_lista_multilayout_uf_ent_Text', NULL, 'whitelabel', '<#assign wp=JspTaglibs["/aps-core"]>
<#assign formFieldNameVar = userFilterOptionVar.formFieldNames[0] >
<#assign formFieldValue = userFilterOptionVar.getFormFieldValue(formFieldNameVar) >
<#assign i18n_Attribute_Key = userFilterOptionVar.attribute.name >

<div class="col-md-6">
	<div class="form-group">
		<label class="active" for="${formFieldNameVar}" ><@wp.i18n key="${i18n_Attribute_Key}" /></label>
		<input name="${formFieldNameVar}" id="${formFieldNameVar}" data-ng-model="searchParameters.${i18n_Attribute_Key}" type="text" class="form-control" placeholder="Inserisci <@wp.i18n key=''${i18n_Attribute_Key}'' />"/>
	</div>
</div>', '<#assign wp=JspTaglibs["/aps-core"]>
<#assign formFieldNameVar = userFilterOptionVar.formFieldNames[0] >
<#assign formFieldValue = userFilterOptionVar.getFormFieldValue(formFieldNameVar) >
<#assign i18n_Attribute_Key = userFilterOptionVar.attribute.name >

<div class="col-md-6">
	<div class="form-group">
		<label class="active" for="${formFieldNameVar}" ><@wp.i18n key="${i18n_Attribute_Key}" /></label>
		<input name="${formFieldNameVar}" id="${formFieldNameVar}" data-ng-model="searchParameters.${i18n_Attribute_Key}" type="text" class="form-control" placeholder="Inserisci <@wp.i18n key=''${i18n_Attribute_Key}'' />"/>
	</div>
</div>', 1);
INSERT INTO guifragment (code, widgettypecode, plugincode, gui, defaultgui, locked) VALUES ('cagliari_widget_lista_multilayout_uf_ent_Date', NULL, 'whitelabel', '<#assign wp=JspTaglibs["/aps-core"]>

<#assign currentLangVar ><@wp.info key="currentLang" /></#assign>

<#assign i18n_Attribute_Key = userFilterOptionVar.attribute.name >
<#assign formFieldStartNameVar = userFilterOptionVar.formFieldNames[0] >
<#assign formFieldStartValueVar = userFilterOptionVar.getFormFieldValue(formFieldStartNameVar) >
<#assign formFieldEndNameVar = userFilterOptionVar.formFieldNames[1] >
<#assign formFieldEndValueVar = userFilterOptionVar.getFormFieldValue(formFieldEndNameVar) >

<#assign js_datapicker="$(function() {
	$(''#${formFieldStartNameVar}'').datepicker();
	$(''#${formFieldEndNameVar}'').datepicker();
});" />
<@wp.headInfo type="JS_CA_BTM_INC" info="${js_datapicker}" />


<div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 search-filter-dategroup">
	<div class="form-group">
		<label for="jiride_dallaDataAdozione"><@wp.i18n key="${i18n_Attribute_Key}" /> - <@wp.i18n key="DATE_FROM" /></label>
		<input type="text" class="form-control" name="${formFieldStartNameVar}" id="${formFieldStartNameVar}" placeholder="gg/mm/aaaa" data-ng-model="searchParameters.${i18n_Attribute_Key}.start"/>
		<button aria-label="Apri calendario" type="button" class="ico-sufix" onclick="$(''#${formFieldStartNameVar}'').focus();" onkeypress="$(''#${formFieldStartNameVar}'').focus();"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-event"></use></svg></button>
	</div>
</div>

<div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 search-filter-dategroup">
	<div class="form-group">
		<label for="jiride_dallaDataAdozione"><@wp.i18n key="${i18n_Attribute_Key}" /> - <@wp.i18n key="DATE_TO" /></label>
		<input type="text" class="form-control" name="${formFieldEndNameVar}" id="${formFieldEndNameVar}" placeholder="gg/mm/aaaa" data-ng-model="searchParameters.${i18n_Attribute_Key}.end"/>
		<button aria-label="Apri calendario" type="button" class="ico-sufix" onclick="$(''#${formFieldEndNameVar}'').focus();" onkeypress="$(''#${formFieldEndNameVar}'').focus();"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-event"></use></svg></button>
	</div>
</div>', '<#assign wp=JspTaglibs["/aps-core"]>

<#assign currentLangVar ><@wp.info key="currentLang" /></#assign>

<#assign i18n_Attribute_Key = userFilterOptionVar.attribute.name >
<#assign formFieldStartNameVar = userFilterOptionVar.formFieldNames[0] >
<#assign formFieldStartValueVar = userFilterOptionVar.getFormFieldValue(formFieldStartNameVar) >
<#assign formFieldEndNameVar = userFilterOptionVar.formFieldNames[1] >
<#assign formFieldEndValueVar = userFilterOptionVar.getFormFieldValue(formFieldEndNameVar) >

<#assign js_datapicker="$(function() {
	$(''#${formFieldStartNameVar}'').datepicker();
	$(''#${formFieldEndNameVar}'').datepicker();
});" />
<@wp.headInfo type="JS_CA_BTM_INC" info="${js_datapicker}" />
<div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 search-filter-dategroup">
	<div class="form-group">
		<label for="jiride_dallaDataAdozione"><@wp.i18n key="${i18n_Attribute_Key}" /> - <@wp.i18n key="DATE_FROM" /></label>
		<input type="text" class="form-control" name="${formFieldStartNameVar}" id="${formFieldStartNameVar}" placeholder="gg/mm/aaaa" data-ng-model="searchParameters.${i18n_Attribute_Key}.start"/>
		<button aria-label="Apri calendario" type="button" class="ico-sufix" onclick="$(''#${formFieldStartNameVar}'').focus();" onkeypress="$(''#${formFieldStartNameVar}'').focus();"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-event"></use></svg></button>
	</div>
</div>
<div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 search-filter-dategroup">
	<div class="form-group">
		<label for="jiride_dallaDataAdozione"><@wp.i18n key="${i18n_Attribute_Key}" /> - <@wp.i18n key="DATE_TO" /></label>
		<input type="text" class="form-control" name="${formFieldEndNameVar}" id="${formFieldEndNameVar}" placeholder="gg/mm/aaaa" data-ng-model="searchParameters.${i18n_Attribute_Key}.end"/>
		<button aria-label="Apri calendario" type="button" class="ico-sufix" onclick="$(''#${formFieldEndNameVar}'').focus();" onkeypress="$(''#${formFieldEndNameVar}'').focus();"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-event"></use></svg></button>
	</div>
</div>', 1);