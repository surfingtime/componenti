INSERT INTO pages (code,parentcode,pos) VALUES ('ser_root','homepage',8);

INSERT INTO pages_metadata_draft (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('ser_root','free','<?xml version="1.0" encoding="UTF-8"?><properties><property key="en">Service root</property><property key="it">Service root</property></properties>','service',1,'<?xml version="1.0" encoding="UTF-8"?><config>  <useextratitles>false</useextratitles>  <charset>utf-8</charset>  <mimeType>text/html</mimeType>  <useextradescriptions>false</useextradescriptions>  <descriptions>    <property key="en" useDefaultLang="false">Service root</property>    <property key="it" useDefaultLang="false">Service root</property>  </descriptions>  <keywords>    <property key="en" useDefaultLang="false">Service root</property>    <property key="it" useDefaultLang="false">Service root</property>  </keywords>  <complexParameters /></config>','2020-06-22 13:20:52');

INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('ser_root','free','<?xml version="1.0" encoding="UTF-8"?><properties><property key="en">Service root</property><property key="it">Service root</property></properties>','service',1,'<?xml version="1.0" encoding="UTF-8"?><config>  <useextratitles>false</useextratitles>  <charset>utf-8</charset>  <mimeType>text/html</mimeType>  <useextradescriptions>false</useextradescriptions>  <descriptions>    <property key="en" useDefaultLang="false">Service root</property>    <property key="it" useDefaultLang="false">Service root</property>  </descriptions>  <keywords>    <property key="en" useDefaultLang="false">Service root</property>    <property key="it" useDefaultLang="false">Service root</property>  </keywords>  <complexParameters /></config>','2020-06-22 13:20:52');


INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_SRV_cosa_serve','en','Cosa serve');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_SRV_cosa_serve','it','Cosa serve');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_SRV_casi_part','en','Casi particolari');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_SRV_casi_part','it','Casi particolari');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_SRV_desc_dest','en','A chi si rivolge');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_SRV_desc_dest','it','A chi si rivolge');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_SRV_costi_vinc','en','Costi e vincoli');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_SRV_costi_vinc','it','Costi e vincoli');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_SRV_scadenze','en','Tempi e scadenze');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_SRV_scadenze','it','Tempi e scadenze');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_SRV_contatto','en','Contatti');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_SRV_contatto','it','Contatti');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_SRV_descr_est','en','Cos''è');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_SRV_descr_est','it','Cos''è');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_SRV_allegati','en','Documenti');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_SRV_allegati','it','Documenti');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_SRV_can_digit','en','Accesso online');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_SRV_can_digit','it','Accesso online');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_SRV_parte_di','en','Fa parte di');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_SRV_parte_di','it','Fa parte di');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_SRV_link','en','Siti esterni');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_SRV_link','it','Siti esterni');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_SRV_micro_serv','en','Altri servizi');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_SRV_micro_serv','it','Altri servizi');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_SRV_proc_esito','en','Procedure collegate all''esito');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_SRV_proc_esito','it','Procedure collegate all''esito');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_SRV_come_si_fa','en','Accedere al servizio');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_SRV_come_si_fa','it','Accedere al servizio');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_SRV_esito','en','Cosa si ottiene');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_SRV_esito','it','Cosa si ottiene');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_SRV_can_fisico','en','Accesso agli uffici');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_SRV_can_fisico','it','Accesso agli uffici');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_SRV_area','en','Area di riferimento');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_SRV_area','it','Area di riferimento');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_SRV_uffici','en','Uffici');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_SRV_uffici','it','Uffici');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_SRV_documento','en','Scarica il documento');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_SRV_documento','it','Scarica il documento');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_SRV','en','Servizi');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_SRV','it','Servizi');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_SRV_documenti','en','Documenti correlati');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_SRV_documenti','it','Documenti correlati');
