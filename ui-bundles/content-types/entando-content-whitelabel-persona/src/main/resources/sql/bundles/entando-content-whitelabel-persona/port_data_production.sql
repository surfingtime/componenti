INSERT INTO pages (code,parentcode,pos) VALUES ('pers_root','homepage',7);
INSERT INTO pages (code,parentcode,pos) VALUES ('per_1','pers_root',1);
INSERT INTO pages (code,parentcode,pos) VALUES ('per_2','pers_root',2);

INSERT INTO pages_metadata_draft (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('pers_root','free','<?xml version="1.0" encoding="UTF-8"?><properties><property key="en">Person root</property><property key="it">Person root</property></properties>','service',1,'<?xml version="1.0" encoding="UTF-8"?><config>  <useextratitles>false</useextratitles>  <charset>utf-8</charset>  <mimeType>text/html</mimeType>  <useextradescriptions>false</useextradescriptions>  <descriptions>    <property key="en" useDefaultLang="false">Person root</property>    <property key="it" useDefaultLang="false">Person root</property>  </descriptions>  <keywords>    <property key="en" useDefaultLang="false">Person root</property>    <property key="it" useDefaultLang="false">Person root</property>  </keywords>  <complexParameters /></config>','2020-06-22 13:17:37');
INSERT INTO pages_metadata_draft (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('per_1','free','<?xml version="1.0" encoding="UTF-8"?><properties><property key="en">Person 1</property><property key="it">Person 1</property></properties>','service',1,'<?xml version="1.0" encoding="UTF-8"?><config>  <useextratitles>false</useextratitles>  <charset>utf-8</charset>  <mimeType>text/html</mimeType>  <useextradescriptions>false</useextradescriptions>  <descriptions>    <property key="en" useDefaultLang="false">Person 1</property>    <property key="it" useDefaultLang="false">Person 1</property>  </descriptions>  <keywords>    <property key="en" useDefaultLang="false">Person 1</property>    <property key="it" useDefaultLang="false">Person 1</property>  </keywords>  <complexParameters /></config>','2020-06-22 13:19:25');
INSERT INTO pages_metadata_draft (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('per_2','free','<?xml version="1.0" encoding="UTF-8"?><properties><property key="en">Person 2</property><property key="it">Person 2</property></properties>','service',1,'<?xml version="1.0" encoding="UTF-8"?><config>  <useextratitles>false</useextratitles>  <charset>utf-8</charset>  <mimeType>text/html</mimeType>  <useextradescriptions>false</useextradescriptions>  <descriptions>    <property key="en" useDefaultLang="false">Person 2</property>    <property key="it" useDefaultLang="false">Person 2</property>  </descriptions>  <keywords>    <property key="en" useDefaultLang="false">Person 2</property>    <property key="it" useDefaultLang="false">Person 2</property>  </keywords>  <complexParameters /></config>','2020-06-22 15:23:03');

INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('pers_root','free','<?xml version="1.0" encoding="UTF-8"?><properties><property key="en">Person root</property><property key="it">Person root</property></properties>','service',1,'<?xml version="1.0" encoding="UTF-8"?><config>  <useextratitles>false</useextratitles>  <charset>utf-8</charset>  <mimeType>text/html</mimeType>  <useextradescriptions>false</useextradescriptions>  <descriptions>    <property key="en" useDefaultLang="false">Person root</property>    <property key="it" useDefaultLang="false">Person root</property>  </descriptions>  <keywords>    <property key="en" useDefaultLang="false">Person root</property>    <property key="it" useDefaultLang="false">Person root</property>  </keywords>  <complexParameters /></config>','2020-06-22 13:17:37');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('per_1','free','<?xml version="1.0" encoding="UTF-8"?><properties><property key="en">Person 1</property><property key="it">Person 1</property></properties>','service',1,'<?xml version="1.0" encoding="UTF-8"?><config>  <useextratitles>false</useextratitles>  <charset>utf-8</charset>  <mimeType>text/html</mimeType>  <useextradescriptions>false</useextradescriptions>  <descriptions>    <property key="en" useDefaultLang="false">Person 1</property>    <property key="it" useDefaultLang="false">Person 1</property>  </descriptions>  <keywords>    <property key="en" useDefaultLang="false">Person 1</property>    <property key="it" useDefaultLang="false">Person 1</property>  </keywords>  <complexParameters /></config>','2020-06-22 13:19:25');
INSERT INTO pages_metadata_online (code,groupcode,titles,modelcode,showinmenu,extraconfig,updatedat) VALUES ('per_2','free','<?xml version="1.0" encoding="UTF-8"?><properties><property key="en">Person 2</property><property key="it">Person 2</property></properties>','service',1,'<?xml version="1.0" encoding="UTF-8"?><config>  <useextratitles>false</useextratitles>  <charset>utf-8</charset>  <mimeType>text/html</mimeType>  <useextradescriptions>false</useextradescriptions>  <descriptions>    <property key="en" useDefaultLang="false">Person 2</property>    <property key="it" useDefaultLang="false">Person 2</property>  </descriptions>  <keywords>    <property key="en" useDefaultLang="false">Person 2</property>    <property key="it" useDefaultLang="false">Person 2</property>  </keywords>  <complexParameters /></config>','2020-06-22 15:23:03');


INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_uffici','it','Uffici');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_uffici','en','Uffici');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_ruolo','en','Ruolo');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_ruolo','it','Ruolo');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_contatti','en','Contatti');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_contatti','it','Contatti');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_documenti','en','Documenti');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_documenti','it','Documenti');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_fapartedi','en','Fa parte di:');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_fapartedi','it','Fa parte di:');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_biografia','en','Biografia');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_biografia','it','Biografia');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_deleghe','en','Deleghe');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_deleghe','it','Deleghe');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_nome','it','Nome');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_nome','en','Nome');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_cognome','en','Cognome');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_cognome','it','Cognome');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_telefono','en','Telefono');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_telefono','it','Telefono');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_struttura','en','Struttura');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_struttura','it','Struttura');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_area','en','Area');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_area','it','Area');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_non_disponibile','en','non disponibile');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_non_disponibile','it','non disponibile');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_CARICHE_non_disponibile','en','Altri incarichi');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_CARICHE_non_disponibile','it','Altri incarichi');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_PATRIMONIO_non_disponibile','en','Variazioni situazione patrimoniale');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_PATRIMONIO_non_disponibile','it','Variazioni situazione patrimoniale');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_COMPENSI_non_disponibile','en','Compensi');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_COMPENSI_non_disponibile','it','Compensi');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_CV_non_disponibile','en','Curriculum Vitae');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_CV_non_disponibile','it','Curriculum Vitae');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_IMPORTI_non_disponibile','en','Importi di viaggi di servizio e missioni');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_IMPORTI_non_disponibile','it','Importi di viaggi di servizio e missioni');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_REDDITI_non_disponibile','en','Dichiarazione dei redditi');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_REDDITI_non_disponibile','it','Dichiarazione dei redditi');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_SPESE_non_disponibile','en','Spese elettorali');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_SPESE_non_disponibile','it','Spese elettorali');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_membro','en','Membro di');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_membro','it','Membro di');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_partedi','en','Fa parte di');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_partedi','it','Fa parte di');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_atto_nom','en','Atto di nomina / proclamazione / incarico');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_atto_nom','it','Atto di nomina / proclamazione / incarico');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_spese_elet','en','Spese elettorali');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_spese_elet','it','Spese elettorali');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_compensi','en','Compensi connessi all''assunzione della carica');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_compensi','it','Compensi connessi all''assunzione della carica');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_importi','en','Spese di missione');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_importi','it','Spese di missione');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_altre_car','en','Altre cariche e incarichi');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_altre_car','it','Altre cariche e incarichi');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_patrim_sit','en','Situazione patrimoniale');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_patrim_sit','it','Situazione patrimoniale');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_patrim_var','en','Situazione patrimoniale dopo cessazione');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_patrim_var','it','Situazione patrimoniale dopo cessazione');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_redditi','en','Dichiarazione dei redditi titolare');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_redditi','it','Dichiarazione dei redditi titolare');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_redditi_pa','en','Dichiarazione dei redditi coniuge e parenti');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_redditi_pa','it','Dichiarazione dei redditi coniuge e parenti');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_insussist','en','Dichiarazione insussistenza cause di inconferibilità e incompatibilità');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_insussist','it','Dichiarazione insussistenza cause di inconferibilità e incompatibilità');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_emolumenti','en','Emolumenti complessivi percepiti a carico della finanza pubblica');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_emolumenti','it','Emolumenti complessivi percepiti a carico della finanza pubblica');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_email','en','Email');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_email','it','Email');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS','en','Persone');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS','it','Persone');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_data_insed','en','Data di inizio insediamento');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_data_insed','it','Data di inizio insediamento');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_data_fine','en','Data di fine insediamento');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_data_fine','it','Data di fine insediamento');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_data_incarico','en','Data di inizio incarico');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_data_incarico','it','Data di inizio incarico');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_data_fine_incarico','en','Data di fine incarico');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRS_data_fine_incarico','it','Data di fine incarico');






