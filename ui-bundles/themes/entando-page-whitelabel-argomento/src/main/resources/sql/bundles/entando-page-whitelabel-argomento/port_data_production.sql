INSERT INTO pagemodels (code,descr,frames,plugincode,templategui) VALUES ('cagliari_argumentpage','Cagliari - Argomento','<?xml version="1.0" encoding="UTF-8"?>
<frames>
	<frame pos="0">
		<descr>Menu orizzontale</descr>
		<sketch x1="0" y1="0" x2="5" y2="0" />
		<defaultWidget code="cagliari_widget_header_horizontalmenu">
			<properties>
				<property key="navSpec">code(amministrazione) + code(servizi) + code(documenti)+code(aggiornamenti)</property>
			</properties>
		</defaultWidget>
	</frame>
	<frame pos="1">
		<descr>Menu utility</descr>
		<sketch x1="6" y1="0" x2="11" y2="0" />
		<defaultWidget code="cagliari_widget_menu_link_utili" />
	</frame>
	<frame pos="2">
		<descr>Argomento</descr>
		<sketch x1="0" y1="1" x2="11" y2="1" />
	</frame>
	<frame pos="3">
		<descr>In evidenza</descr>
		<sketch x1="0" y1="2" x2="11" y2="2" />
	</frame>
	<frame pos="4">
		<descr>Amministrazione</descr>
		<sketch x1="0" y1="3" x2="11" y2="3" />
	</frame>
	<frame pos="5">
		<descr>Servizi</descr>
		<sketch x1="0" y1="4" x2="11" y2="4" />
	</frame>
	<frame pos="6">
		<descr>Novita</descr>
		<sketch x1="0" y1="5" x2="11" y2="5" />
	</frame>
	<frame pos="7">
		<descr>Documenti</descr>
		<sketch x1="0" y1="6" x2="11" y2="6" />
	</frame>
	<frame pos="8">
		<descr>Altri contenuti</descr>
		<sketch x1="0" y1="7" x2="11" y2="7" />
	</frame>
	<frame pos="9" main="true">
		<descr>Dettaglio finto pagina volante</descr>
		<sketch x1="0" y1="8" x2="11" y2="8" />
	</frame>
	<frame pos="10">
		<descr>Footer</descr>
		<sketch x1="0" y1="9" x2="11" y2="9" />
		<defaultWidget code="cagliari_widget_footer_menu">
			<properties>
				<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1)+code(novita).subtree(1)+ code(documenti).subtree(1)</property>
			</properties>
		</defaultWidget>
	</frame>
</frames>

',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<@wp.headInfo type="CSS_CA_INT" info="sezioni.min.css" />
<@wp.headInfo type="CSS_CA_INT" info="interne.min.css" />

<!doctype html>
	<@wp.fragment code="cagliari_template_head" escapeXml=false />
	<body class="push-body" data-ng-app="ponmetroca" data-ng-csp="no-inline-style">
	    <@wp.fragment code="cagliari_template_googletagmanager" escapeXml=false />
		<div class="body_wrapper push_container clearfix" id="page_top">
			<@wp.fragment code="cagliari_template_skiplink" escapeXml=false />
			<header id="mainheader" class="navbar-fixed-top bg-blu container-fullwidth">
				<@wp.fragment code="cagliari_template_header_preheader" escapeXml=false />
				<!-- Button Menu -->
 				<button class="navbar-toggle menu-btn pull-left menu-left push-body jPushMenuBtn">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar icon-bar1"></span>
					<span class="icon-bar icon-bar2"></span>
					<span class="icon-bar icon-bar3"></span>
				</button>
				<!--End Button Menu -->
 
 				<!-- Menu -->
 				<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="menup">
 					<div class="cbp-menu-wrapper clearfix">
 						<@c.import url="/WEB-INF/aps/jsp/models/inc/cagliari_template_header_navigation_menu.jsp" />
 						<ul class="utilitymobile">
 						    <@wp.show frame=1 />
 						</ul>
 						<ul class="list-inline socialmobile">
 						    <@wp.fragment code="cagliari_template_header_social_mobile" escapeXml=false />
 						</ul>
 					</div>
 				</nav>
 				<!-- End Menu -->
 				
 				<@wp.fragment code="cagliari_template_menu_ap" escapeXml=false />
 				
				<@wp.fragment code="cagliari_template_header_intestazione" escapeXml=false />
				<section class="hidden-xs" id="sub_nav">
                    <h2 class="sr-only">Submenu</h2>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-md-8 pull-left text-left">
                                <ul class="sub_nav clearfix">
				                    <@wp.show frame=0 />
				                </ul>
				            </div>
				            <div class="col-lg-6 col-md-4 pull-right text-right">
				                <ul class="list_link-utili">
				                <@wp.show frame=1 />
				                </ul>
				            </div>
				        </div>
                    </div>
                </section>
			</header>
			<main id="main_container">
				<div class="bg-grigio">
					<section id="intro-argomenti">
						<@wp.show frame=2 />
					</section>
					<section id="argomenti-inevidenza" class="sezioni">
						<div class="container">
							<@wp.show frame=3 />
						</div>
					</section>
				</div>
				<section id="argomenti-amministrazione">
					<div class="container">
						<@wp.show frame=4 />
					</div>
				</section>
				<section id="argomenti-servizi">
					<div class="container">
						<@wp.show frame=5 />
					</div>
				</section>
				<section id="argomenti-novita" class="sezioni bg-grigio">
					<div class="container">
						<@wp.show frame=6 />
					</div>
				</section>
				<section id="argomenti-documenti">
					<div class="container">
						<@wp.show frame=7 />
					</div>
				</section>
				<@wp.show frame=8 />
			</main>
			<footer id="footer">
				<div class="container">
					<@wp.fragment code="cagliari_template_footer_intestazione" escapeXml=false />
					<section class="lista-sezioni">
				        <div class="row">
					        <@wp.show frame=10 />
					    </div>
					</section>
				    <section class="lista-linkutili">
				        <div class="row">
					        <@wp.fragment code="cagliari_template_footer_info" escapeXml=false />
					    </div>
					</section>
					<@wp.fragment code="cagliari_template_footer_link" escapeXml=false />
				</div>
			</footer>
		</div>
		<@wp.fragment code="cagliari_template_backtotop" escapeXml=false />
		<@wp.fragment code="cagliari_template_footer_js" escapeXml=false />
	</body>
</html>');