INSERT INTO pagemodels (code,descr,frames,plugincode,templategui) VALUES ('cagliari_internalpage','Cagliari - Dettaglio','<?xml version="1.0" encoding="UTF-8"?>
<frames>
	<frame pos="0">
		<descr>Menu orizzontale</descr>
		<sketch x1="0" y1="0" x2="5" y2="0" />
		<defaultWidget code="cagliari_widget_header_horizontalmenu">
			<properties>
				<property key="navSpec">code(amministrazione) + code(servizi) + code(documenti)+code(aggiornamenti)</property>
			</properties>
		</defaultWidget>
	</frame>
	<frame pos="1">
		<descr>Menu utility</descr>
		<sketch x1="6" y1="0" x2="11" y2="0" />
		<defaultWidget code="cagliari_widget_menu_link_utili" />
	</frame>
	<frame pos="2" main="true">
		<descr>Contenuto principale</descr>
		<sketch x1="0" y1="1" x2="11" y2="1" />
	</frame>
	<frame pos="3">
		<descr>Servizio 1</descr>
		<sketch x1="0" y1="2" x2="11" y2="2" />
	</frame>
	<frame pos="4">
		<descr>Servizio 2</descr>
		<sketch x1="0" y1="3" x2="11" y2="3" />
	</frame>
	<frame pos="5">
		<descr>Footer</descr>
		<sketch x1="0" y1="5" x2="11" y2="5" />
		<defaultWidget code="cagliari_widget_footer_menu">
			<properties>
				<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1)+code(novita).subtree(1)+ code(documenti).subtree(1)</property>
			</properties>
		</defaultWidget>
	</frame>
</frames>

',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<@wp.headInfo type="CSS_CA_INT" info="sezioni.min.css" />
<@wp.headInfo type="CSS_CA_INT" info="interne.min.css" />
<@wp.headInfo type="JS_CA_BTM" info="file-saver.min.js" />
<@wp.headInfo type="JS_CA_BTM" info="filtri-controller.min.js" />
<@wp.headInfo type="JS_CA_BTM" info="filtri-service.min.js" />

<!doctype html>
	<@wp.fragment code="cagliari_template_head" escapeXml=false />
	<body class="push-body" data-ng-app="ponmetroca" data-ng-csp="no-inline-style">
	    <@wp.fragment code="cagliari_template_googletagmanager" escapeXml=false />
		<div class="body_wrapper push_container clearfix" id="page_top">
			<@wp.fragment code="cagliari_template_skiplink" escapeXml=false />
		
			<header id="mainheader" class="navbar-fixed-top bg-blu container-fullwidth">
				<@wp.fragment code="cagliari_template_header_preheader" escapeXml=false />
				<!-- Button Menu -->
 				<button class="navbar-toggle menu-btn pull-left menu-left push-body jPushMenuBtn">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar icon-bar1"></span>
					<span class="icon-bar icon-bar2"></span>
					<span class="icon-bar icon-bar3"></span>
				</button>
				<!--End Button Menu -->
 
 				<!-- Menu -->
 				<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="menup">
 					<div class="cbp-menu-wrapper clearfix">
 						<@c.import url="/WEB-INF/aps/jsp/models/inc/cagliari_template_header_navigation_menu.jsp" />
 						<ul class="utilitymobile">
 						    <@wp.show frame=1 />
 						</ul>
 						<ul class="list-inline socialmobile">
 							<@wp.fragment code="cagliari_template_header_social_mobile" escapeXml=false />
 						</ul>
 					</div>
 				</nav>
 				<!-- End Menu -->
 				
 				<@wp.fragment code="cagliari_template_menu_ap" escapeXml=false />
 				
				<@wp.fragment code="cagliari_template_header_intestazione" escapeXml=false />
				<section class="hidden-xs" id="sub_nav">
                    <h2 class="sr-only">Submenu</h2>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-md-8 pull-left text-left">
                                <ul class="sub_nav clearfix">
				                    <@wp.show frame=0 />
				                </ul>
				            </div>
				            <div class="col-lg-6 col-md-4 pull-right text-right">
				                <ul class="list_link-utili">
				                <@wp.show frame=1 />
				                </ul>
				            </div>
				        </div>
                    </div>
                </section>
			</header>
			<main id="main_container">
			    <@wp.fragment code="cagliari_template_briciole" escapeXml=false />
                
				<@wp.show frame=2 />
				<section id="sezioni-servizi-interno">
	                <div class="container">
	                    <@wp.show frame=3 />
				    </div>
                </section>
                <section id="sezioni-eventi-interno">
                    <div class="container">
				        <@wp.show frame=4 />
				    </div>
                </section>
			</main>
			<footer id="footer">
				<div class="container">
					<@wp.fragment code="cagliari_template_footer_intestazione" escapeXml=false />
					<section class="lista-sezioni">
				        <div class="row">
					        <@wp.show frame=5 />
					    </div>
					</section>
				    <section class="lista-linkutili">
				        <div class="row">
					        <@wp.fragment code="cagliari_template_footer_info" escapeXml=false />
					    </div>
					</section>
					<@wp.fragment code="cagliari_template_footer_link" escapeXml=false />
				</div>
			</footer>
		</div>
		<@wp.fragment code="cagliari_template_backtotop" escapeXml=false />
		<@wp.fragment code="cagliari_template_footer_js" escapeXml=false />
	</body>
</html>');

INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cagliari_template_briciole',NULL,NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign uc=JspTaglibs["/comuneCagliari-whitelabel"]>

<section id="briciole">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-10 col-md-12">
				<nav class="breadcrumb-container" aria-label="<@wp.i18n key="CAGLIARI_PORTAL_YOUAREIN" />">
					<ol class="breadcrumb">
						<@wp.currentPage param="code" var="currentViewCode" />
						<@wp.freemarkerTemplateParameter var="currentViewCode" valueName="currentViewCode" />
						<#assign first=true />
						<#assign lastPageTitle="" />
						
						<@uc.cagliariMainFrameContentTag var="content"/>
						<@uc.cagliariPageInfoTag pageCode="${currentViewCode}" info="isPublishOnFly" var="isPublishOnFly"/>
						<@wp.nav spec="current.path" var="currentTarget">
							<#assign currentCode = currentTarget.code />
							<#if !currentTarget.voidPage>
								<#if currentCode == currentViewCode>
									<#if (isPublishOnFly == "true")>
										<#if (content.getAttributeByRole(''jacms:title''))??>
											<#assign title=content.getAttributeByRole(''jacms:title'').text>
											<li class="breadcrumb-item active" aria-current="page"><a>${title}</a></li>
										</#if>
									<#else>									
										<li class="breadcrumb-item active" aria-current="page"><a>${currentTarget.title}</a></li>
									</#if>
								<#else>
									<li class="breadcrumb-item"><a href="${currentTarget.url}" title="<@wp.i18n key="CAGLIARI_PORTAL_GOTOPAGE" /> ${currentTarget.title}"><strong>${currentTarget.title}</strong></a><span class="separator">/</span></li>
									<#if (!first)>
										
									</#if>
								</#if>
							<#else>
								<li class="breadcrumb-item active" aria-current="page"><a>${currentTarget.title}</a></li>
							</#if>
							<#assign lastPageTitle = currentTarget.title />
						</@wp.nav>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</section>',NULL,0);