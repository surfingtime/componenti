<%@ taglib prefix="wp" uri="/aps-core" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="uc" uri="/comuneCagliari-whitelabel" %>

<div class="logo-burger<c:if test="${sessionScope.currentUser != 'guest'}">-user</c:if>">
        <div class="logoimg-burger">
            <a href="<wp:url page="homepage" />" title="<wp:i18n key="CAGLIARI_PORTAL_DESCRIPTION" />"> 
            <img src="<wp:imgURL/>logo_cagliari_print.svg" alt="<wp:i18n key="CAGLIARI_HEADER_INTESTAZIONE_IMGALT" />"/>
        </a>
    </div>
    <div class="logotxt-burger">
        <a href="<wp:url page="homepage" />" title="<wp:i18n key="CAGLIARI_PORTAL_DESCRIPTION" />"><wp:i18n key="CAGLIARI_HEADER_INTESTAZIONE_TEXT" /></a>
    </div>
</div>

<h2 class="sr-only"><wp:i18n key="CAGLIARI_HEADER_MENU_TITLE" /></h2>

<wp:currentPage param="code" var="paginaCorrente" />
<c:set var="paginaCorrente" value="${paginaCorrente}"/>
<uc:cagliariPageInfoTag pageCode="${paginaCorrente}" info="pathArray" var="pathCorrente"/>

<c:set var="paginaPrecedente" value="${null}"/>
<c:set var="livelloPrecedente" value="-1"/>
<c:set var="livello" value="0"/>

<ul class="nav navmenu">
    <wp:nav var="pagina" spec="code(homepage).subtree(1)">
        <c:if test="${paginaPrecedente.code != null}">
            <c:set var="livelloPrecedente" value="${paginaPrecedente.level}"/>
            <c:set var="livello" value="${pagina.level}"/>
        </c:if>
        <c:set var="aURL" value="${pagina.url}"/>

        <c:if test="${0 == livelloPrecedente}">
        </li>
    </c:if>
    <c:choose>
        <c:when test="${livello == livelloPrecedente}">
        </li>
        <li>
        </c:when>
        <c:when test="${livello > livelloPrecedente}">
            <c:if test="${livello > 1}">
                <ul>
                </c:if>
                <li>
                </c:when>
                <c:when test="${livello < livelloPrecedente}">
                </li>
                <c:forEach begin="${livello}" end="${livelloPrecedente-1}"></ul></li></c:forEach>
            <li>
        </c:when>
    </c:choose>

    <c:set var="aperto" value="false"/>
    <c:forEach var="pgitem" items="${pathCorrente}">
        <c:if test="${(pgitem == pagina.code) && (pagina.code != 'homepage')}">
            <c:set var="aperto" value="true"/>
        </c:if>
    </c:forEach>
    <a href="<c:out value="${aURL}" escapeXml="false" />" title="<wp:i18n key="CAGLIARI_PORTAL_GOTO" /> <c:out value="${pagina.title}" />"
       <c:if test="${aperto || (pagina.code == paginaCorrente)}">class="current"</c:if>>
        <c:out value="${pagina.title}"/>
    </a>
    <c:set var="paginaPrecedente" value="${pagina}"/>
</wp:nav>
<c:if test="${1 == livelloPrecedente}">
</li>
</c:if>
<c:forEach begin="2" end="${livelloPrecedente}"></li></ul></c:forEach>
</ul>
