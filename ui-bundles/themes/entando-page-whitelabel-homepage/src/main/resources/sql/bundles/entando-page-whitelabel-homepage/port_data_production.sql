INSERT INTO pagemodels (code,descr,frames,plugincode,templategui) VALUES ('cagliari_homepage','Cagliari - Home page','<?xml version="1.0" encoding="UTF-8"?>
<frames>
	<frame pos="0">
		<descr>Menu orizzontale</descr>
		<sketch x1="0" y1="0" x2="5" y2="0" />
		<defaultWidget code="cagliari_widget_header_horizontalmenu">
			<properties>
				<property key="navSpec">code(amministrazione) + code(servizi) + code(documenti)+code(aggiornamenti)</property>
			</properties>
		</defaultWidget>
	</frame>
	<frame pos="1">
		<descr>Menu utility</descr>
		<sketch x1="6" y1="0" x2="11" y2="0" />
		<defaultWidget code="cagliari_widget_menu_link_utili" />
	</frame>
	<frame pos="2">
		<descr>Allerta meteo</descr>
		<sketch x1="0" y1="1" x2="11" y2="1" />
	</frame>
	<frame pos="3">
		<descr>Notizia principale</descr>
		<sketch x1="0" y1="2" x2="11" y2="2" />
	</frame>
	<frame pos="4">
		<descr>Ultime notizie</descr>
		<sketch x1="0" y1="3" x2="11" y2="3" />
	</frame>
	<frame pos="5">
		<descr>Calendario</descr>
		<sketch x1="0" y1="4" x2="11" y2="4" />
	</frame>
	<frame pos="6">
		<descr>Argomenti in evidenza</descr>
		<sketch x1="0" y1="5" x2="11" y2="5" />
	</frame>
	<frame pos="7">
		<descr>Siti tematici</descr>
		<sketch x1="0" y1="6" x2="11" y2="6" />
	</frame>
	<frame pos="8">
		<descr>Cerca servizi</descr>
		<sketch x1="0" y1="7" x2="11" y2="7" />
	</frame>
	<frame pos="9">
		<descr>Altro 1</descr>
		<sketch x1="0" y1="8" x2="11" y2="8" />
	</frame>
	<frame pos="10">
		<descr>Altro 2</descr>
		<sketch x1="0" y1="9" x2="11" y2="9" />
	</frame>
	<frame pos="11">
		<descr>Altro 3</descr>
		<sketch x1="0" y1="10" x2="11" y2="10" />
	</frame>
	<frame pos="12">
		<descr>Altro 4</descr>
		<sketch x1="0" y1="11" x2="11" y2="11" />
	</frame>
	<frame pos="13">
		<descr>Footer</descr>
		<sketch x1="0" y1="12" x2="11" y2="12" />
		<defaultWidget code="cagliari_widget_footer_menu">
			<properties>
				<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1)+code(novita).subtree(1)+ code(documenti).subtree(1)</property>
			</properties>
		</defaultWidget>
	</frame>
</frames>

',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<@wp.headInfo type="CSS_CA_INT" info="home.min.css" />

<!doctype html>
	<@wp.fragment code="cagliari_template_head" escapeXml=false />
	<body class="push-body" data-ng-app="ponmetroca" data-ng-csp="no-inline-style">
	    <@wp.fragment code="cagliari_template_googletagmanager" escapeXml=false /> 
		<div class="body_wrapper push_container clearfix" id="page_top">
			<@wp.fragment code="cagliari_template_skiplink" escapeXml=false /> 
		
			<header id="mainheader" class="navbar-fixed-top bg-blu container-fullwidth">
				<@wp.fragment code="cagliari_template_header_preheader" escapeXml=false /> 
				<!-- Button Menu -->
 				<button class="navbar-toggle menu-btn pull-left menu-left push-body jPushMenuBtn">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar icon-bar1"></span>
					<span class="icon-bar icon-bar2"></span>
					<span class="icon-bar icon-bar3"></span>
				</button>
				<!--End Button Menu -->
 
 				<!-- Menu -->
 				<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="menup">
 					<div class="cbp-menu-wrapper clearfix">
 						<@c.import url="/WEB-INF/aps/jsp/models/inc/cagliari_template_header_navigation_menu.jsp" />
                                                                
 						<ul class="utilitymobile">
 						    <@wp.show frame=1 />
 						</ul>
 						<ul class="list-inline socialmobile">
 							<@wp.fragment code="cagliari_template_header_social_mobile" escapeXml=false /> 
 						</ul>
 					</div>
 				</nav>
 				<!-- End Menu -->
 				
 				<@wp.fragment code="cagliari_template_menu_ap" escapeXml=false /> 
 				
				<@wp.fragment code="cagliari_template_header_intestazione" escapeXml=false /> 
				<section class="hidden-xs" id="sub_nav">
                    <h2 class="sr-only">Submenu</h2>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-md-8 pull-left text-left">
                                <ul class="sub_nav clearfix">
				                    <@wp.show frame=0 />
				                </ul>
				            </div>
				            <div class="col-lg-6 col-md-4 pull-right text-right">
				                <ul class="list_link-utili">
				                <@wp.show frame=1 />
				                </ul>
				            </div>
				        </div>
                    </div>
                </section>
			</header>
			<main id="main_container">
			    <section id="allerta_meteo">
				    <@wp.show frame=2 />
				</section>
				<section id="novita_evidenza">
				    <@wp.show frame=3 />
				</section>
				<section id="home-novita">
                    <div class="container">
                        <div class="mt-88n">
                            <@wp.show frame=4 />
                    	</div>		
	                </div>
				</section>
				<section id="calendario">
				    <@wp.show frame=5 />
				</section>
				<section id="argomenti_evidenza">
				    <@wp.show frame=6 />
				</section>
				<section id="siti_tematici">
				    <div class="container">
				        <@wp.show frame=7 />
				    </div>
				</section>
				<section class="bg-servizi" id="home-servizi">
				    <@wp.show frame=8 />
				</section>
				<@wp.show frame=9 />
				<@wp.show frame=10 />
				<@wp.show frame=11 />
				<@wp.show frame=12 />
			</main>
			<footer id="footer">
				<div class="container">
					<@wp.fragment code="cagliari_template_footer_intestazione" escapeXml=false /> 
					<section class="lista-sezioni">
				        <div class="row">
					        <@wp.show frame=13 />
					    </div>
					</section>
				    <section class="lista-linkutili">
				        <div class="row">
					        <@wp.fragment code="cagliari_template_footer_info" escapeXml=false /> 
					    </div>
					</section>
					<@wp.fragment code="cagliari_template_footer_link" escapeXml=false />
				</div>
			</footer>
		</div>
		<@wp.fragment code="cagliari_template_backtotop" escapeXml=false /> 
		<@wp.fragment code="cagliari_template_footer_js" escapeXml=false />
	</body>
</html>');

INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cagliari_template_header_preheader',NULL,NULL,'<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign jpavatar=JspTaglibs["/jpavatar-apsadmin-core"]>

<!-- Fascia Appartenenza -->
<section class="preheader bg-bluscuro">
	<div class="container">
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 entesup">
				<a aria-label="<@wp.i18n key="CAGLIARI_HEADER_PREHEADER_LINKTITLE" />" title="<@wp.i18n key="CAGLIARI_HEADER_PREHEADER_TEXT_TITLE" />" href="http://www.regione.sardegna.it" target="_blank">
					<@wp.i18n key="CAGLIARI_HEADER_PREHEADER_TEXT" escapeXml=false />
				</a>
				<div class="float-right">
					<!-- siti verticali -->
					<div class="sitiverticali float-left text-right">
						<a aria-label="<@wp.i18n key="CAGLIARI_HEADER_PREHEADER_OLDSITE_ARIA" />" title="<@wp.i18n key="CAGLIARI_HEADER_PREHEADER_OLDSITE" />" href="<@wp.url page="old_site" />"><@wp.i18n key="CAGLIARI_HEADER_PREHEADER_OLDSITE" /></a>
					</div>
					<!-- siti verticali -->
					<!-- accedi -->
					<div class="accedi float-left text-right">
						<@wp.ifauthorized groupName="cittadini" var="isCittadino"/>
						<@wp.ifauthorized groupName="administrators" var="isAdmin"/>
						<#if Session.currentUser?? && Session.currentUser != "guest" && isCittadino && !isAdmin>
							<#assign color= (.now?long % 9) + 1/>
							<@wp.currentUserProfileAttribute attributeRoleName="jpspid:codicefiscale" var="utenteCodiceFiscale" />
							<@wp.currentUserProfileAttribute attributeRoleName="userprofile:firstname" var="utenteName" />
							<@wp.currentUserProfileAttribute attributeRoleName="userprofile:surname" var="utenteSurname" />
							<#assign utenteFullname= ''''/>
							<#assign utenteShortname= ''''/>
							<#if utenteName?? && utenteSurname??>
								<#assign utenteFullname>${utenteName} ${utenteSurname}</#assign>
								<#assign utenteShortname>${utenteName?substring(0,1)}${utenteSurname?substring(0,1)}</#assign>
							<#elseif utenteCodiceFiscale??>
								<#assign utenteFullname>${utenteCodiceFiscale}</#assign>
								<#assign utenteShortname>${utenteCodiceFiscale?substring(3,4)}${utenteCodiceFiscale?substring(0,1)}</#assign>
							</#if>
							<button class="btn btn-default btn-accedi btn-accedi-ap-md" role="button" id="dropdownAP" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="${utenteFullname}">
								<@jpavatar.avatar var="currentAvatar" returnDefaultAvatar="true" avatarStyleVar="styleVar" />
								<span class="avatar size-sm bg${color}">
									<#if (currentAvatar?has_content)>
										<img src="${currentAvatar}" alt="${utenteShortname}" />
									<#else>
										<strong>${utenteShortname}</strong>
									</#if>
								</span>
								<span>${utenteFullname}</span>
								<svg class="icon-expand icon icon-sm icon-primary"><use xlink:href="<@wp.imgURL />ponmetroca.svg#ca-keyboard_arrow_down"></use></svg>
							</button>
							<div class="dropdown-menu dropdown-menu-accedi" aria-labelledby="dropdownAP">
								<div class="link-list-wrapper">
									<ul class="link-list">
										<@wp.nav var="page" spec="code(ap).subtree(1)">
											<li>
												<a href="<@wp.url page="${page.code}" />" class="list-item" title="<@wp.i18n key="CAGLIARI_PORTAL_GOTOPAGE" escapeXml=false /> ${page.title}">
													${page.title}
												</a>
											</li>
										</@wp.nav>
										<li>
											<a href="<@wp.info key="systemParam" paramName="applicationBaseURL" />do/logout.action" class="list-item" title="<@wp.i18n key="CAGLIARI_PORTAL_GOTOPAGE" escapeXml=false /> <@wp.i18n key="CAGLIARI_RESERVEDAREA_LOGOUT" escapeXml=false />">
												<@wp.i18n key="CAGLIARI_RESERVEDAREA_LOGOUT" escapeXml=false />
											</a>
										</li>
									</ul>
								</div>
							</div>
							<button class="navbar-toggle-ap btn btn-default menu-right btn-accedi btn-accedi-ap-sm push-body jPushMenuBtn" id="toggleAP">
								<div class="avatar size-sm bg${color}">
									<#if (currentAvatar?has_content)>
										<img src="${currentAvatar}" alt="${utenteShortname}" />
									<#else>
										<strong>${utenteShortname}</strong>
									</#if>
								</div>
							</button>
						<#else>
							<!-- <a class="btn btn-default btn-accedi" href="<@wp.url page="login_spid" />"> -->
<a class="btn btn-default btn-accedi" href="https://servizi.comune.cagliari.it/portale/page/it/login_spid">
								<svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-account_circle"></use></svg>
								<span class="btn-accedi-txt"><@wp.i18n key="CAGLIARI_HEADER_PREHEADER_LOGIN" /></span>
							</a>
						</#if>
					</div>
					<!-- accedi -->
				</div>
			</div>
		</div>
	</div>               
</section>
<!-- Fascia Appartenenza -->','<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
 <#assign wp=JspTaglibs["/aps-core"]>
 				<!-- Fascia Appartenenza -->
 				<section class="preheader bg-bluscuro">
 					<div class="container">
 						<div class="row clearfix">
 							<div class="col-lg-12 col-md-12 col-sm-12 entesup">
 								<a title="<@wp.i18n key="CAGLIARI_HEADER_PREHEADER_LINKTITLE" />" href="http://www.regione.sardegna.it" target="_blank">
 									<@wp.i18n key="CAGLIARI_HEADER_PREHEADER_TEXT" escapeXml=false />
 								</a>
 								 <!-- accedi -->
 								<div class="accedi float-right text-right">
 									<button class="btn btn-default btn-accedi" data-toggle="modal" data-target="xxx">
										<#if (Session.currentUser.username != "guest") >
											<span>${Session.currentUser}</span> 
										<#else>
											<span><@wp.i18n key="CAGLIARI_HEADER_PREHEADER_LOGIN" /></span>
										</#if>
										<i class="material-icons">perm_identity</i>
									</button>
 								</div>
 								<!-- accedi -->
 							</div>
 						</div>
 					</div>               
 				</section>
 				<!-- Fascia Appartenenza -->',1);

INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cagliari_template_googletagmanager',NULL,NULL,'<!-- Google Tag Manager -->',NULL,0);
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cagliari_template_skiplink',NULL,NULL,'<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
 <#assign wp=JspTaglibs["/aps-core"]>    
 
 			<!--[if lt IE 8]>
 				<p class="browserupgrade"><@wp.i18n key="CAGLIARI_PORTAL_BROWSERUPDATE" escapeXml=false /></p>
 			<![endif]-->
 			<div class="skiplink sr-only">
 				<ul>
 					<li><a accesskey="2" href="<@wp.url />#main_container"><@wp.i18n key="CAGLIARI_PORTAL_SKIPLINK_GOCONTENT" /></a></li>
 					<li><a accesskey="3" href="<@wp.url />#menup"><@wp.i18n key="CAGLIARI_PORTAL_SKIPLINK_GONAVIGATION" /></a></li>
 					<li><a accesskey="4" href="<@wp.url />#footer"><@wp.i18n key="CAGLIARI_PORTAL_SKIPLINK_GOFOOTER" /></a></li>
 				</ul>
 			</div>','<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
 <#assign wp=JspTaglibs["/aps-core"]>    
 
 			<!--[if lt IE 8]>
 				<p class="browserupgrade"><@wp.i18n key="CAGLIARI_PORTAL_BROWSERUPDATE" escapeXml=false /></p>
 			<![endif]-->
 			<div class="skiplink sr-only">
 				<ul>
 					<li><a accesskey="2" href="<@wp.url />#main_container"><@wp.i18n key="CAGLIARI_PORTAL_SKIPLINK_GOCONTENT" /></a></li>
 					<li><a accesskey="3" href="<@wp.url />#menup"><@wp.i18n key="CAGLIARI_PORTAL_SKIPLINK_GONAVIGATION" /></a></li>
 					<li><a accesskey="4" href="<@wp.url />#footer"><@wp.i18n key="CAGLIARI_PORTAL_SKIPLINK_GOFOOTER" /></a></li>
 				</ul>
 			</div>',1);

INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cagliari_template_head',NULL,NULL,'<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign jpseo=JspTaglibs["/jpseo-aps-core"]>
<#assign cw=JspTaglibs["/comuneCagliari-whitelabel"]>
<#assign jacms=JspTaglibs["/jacms-aps-core"]>

<#--Inserire il nome della webapp -->
<#assign webappNameVar= ''miocomune'' > 
<html lang="<@wp.info key="currentLang" />"> 	<head>		<base href="<@wp.info key="systemParam" paramName="applicationBaseURL" />"> 		<!--[if IE]><script type="text/javascript"> 			(function() { 				var baseTag = document.getElementsByTagName(''base'')[0]; 				baseTag.href = baseTag.href; 			})(); 		</script><![endif]-->		<@wp.i18n key="CAGLIARI_PORTAL_TITLE" var="siteName"/>  		<@wp.currentPage param="code" var="pageCode" />      		<@wp.currentPage param="title" var="pageTitle" />		<@cw.cagliariPageInfoTag pageCode="${pageCode}" info="isPublishOnFly" var="isPublishOnFly"/>		<#if (isPublishOnFly == "true")>			<@cw.cagliariMainFrameContentTag var="content"/>			<#if content??>				<#if (content.getAttributeByRole(''jpsocial:title''))??>					<#assign title=content.getAttributeByRole(''jpsocial:title'').text>					<#if title??>						<#assign pageTitle = title>					</#if>				</#if>				<#if (content.getAttributeByRole(''jpsocial:description''))??>					<#assign description=content.getAttributeByRole(''jpsocial:description'').text>    				</#if>				<#if (content.getAttributeByRole(''jpsocial:image''))??>					<#assign image=content.getAttributeByRole(''jpsocial:image'').getImagePath("0")>				</#if>			</#if>		</#if>		<#if (pageCode == "homepage")>			<title>${siteName}</title>		<#else>			<#if (pageCode == "arg_01_dett")>				<@cw.currentArgument var="currentArg" />				<#if (currentArg??)>					<@cw.contentList listName="contentList" contentType="ARG" category="${currentArg}"/>					<#if (contentList??) && (contentList?has_content) && (contentList?size > 0)>						<#list contentList as contentId>							<title>${siteName} | <@jacms.content contentId="${contentId}" modelId="400012" /></title>							<#assign description><@jacms.content contentId="${contentId}" modelId="400013" /></#assign>						</#list>					</#if>					<#assign contentList="">				</#if>			<#else>				<title>${siteName} | ${pageTitle}</title>			</#if>		</#if> 		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> 		<@jpseo.currentPage param="description" var="metaDescr" /> 		<#if metaDescr?? && metaDescr?has_content>			<meta name="description" content="${metaDescr}" />		<#else>			<#if description??>				<meta name="description" content="${description}" />			<#else>				<@cw.cagliariMainFrameContentTag var="content"/>				<#if content??>					<#assign description=content.getAttributeByRole(''jacms:description'').text>					<meta name="description" content="${description}" />				</#if>			</#if>		</#if>		<@jpseo.currentPage param="keywords" var="metaKeyword" />		<#if metaKeyword??>			<meta name="keywords" content="${metaKeyword}" />		</#if>  		<link rel="apple-touch-icon" sizes="57x57" href="<@wp.imgURL />apple-icon-57x57.png" /> 		<link rel="apple-touch-icon" sizes="60x60" href="<@wp.imgURL />apple-icon-60x60.png" /> 		<link rel="apple-touch-icon" sizes="72x72" href="<@wp.imgURL />apple-icon-72x72.png" /> 		<link rel="apple-touch-icon" sizes="76x76" href="<@wp.imgURL />apple-icon-76x76.png" /> 		<link rel="apple-touch-icon" sizes="114x114" href="<@wp.imgURL />apple-icon-114x114.png" /> 		<link rel="apple-touch-icon" sizes="120x120" href="<@wp.imgURL />apple-icon-120x120.png" /> 		<link rel="apple-touch-icon" sizes="144x144" href="<@wp.imgURL />apple-icon-144x144.png" /> 		<link rel="apple-touch-icon" sizes="152x152" href="<@wp.imgURL />apple-icon-152x152.png" /> 		<link rel="apple-touch-icon" sizes="180x180" href="<@wp.imgURL />apple-icon-180x180.png" /> 		<link rel="icon" type="image/png" href="<@wp.imgURL />favicon-32x32.png" sizes="32x32" /> 		<link rel="icon" type="image/png" href="<@wp.imgURL />android-chrome-192x192.png" sizes="192x192" /> 		<link rel="icon" type="image/png" href="<@wp.imgURL />favicon-96x96.png" sizes="96x96" /> 		<link rel="icon" type="image/png" href="<@wp.imgURL />favicon-16x16.png" sizes="16x16" /> 		<link rel="stylesheet" href="<@wp.resourceURL/>static/font/fonts.min.css" /> 		<link rel="stylesheet" href="<@wp.cssURL/>bootstrap-italia_1.2.0.min.css" /> 		<link rel="stylesheet" href="<@wp.cssURL/>angular-material.cagliari.min.css"  /> 		<link href="<@wp.cssURL/>cagliari.min.css" rel="stylesheet" type="text/css" /> 		<link href="<@wp.cssURL/>areapersonale.min.css" rel="stylesheet" type="text/css" /> 		<link href="<@wp.cssURL/>cagliari-print.min.css" rel="stylesheet" type="text/css" media="print" /> 		<@wp.outputHeadInfo type="CSS_CA_INT"> 		<link href="<@wp.cssURL/><@wp.printHeadInfo />" rel="stylesheet" type="text/css" /> 		</@wp.outputHeadInfo> 		<@wp.outputHeadInfo type="CSS_CA_EXT"> 		<link href="<@wp.printHeadInfo />" rel="stylesheet" type="text/css" /> 		</@wp.outputHeadInfo> 		<@wp.outputHeadInfo type="CSS_CA_EXT_GLB"><@wp.printHeadInfo /></@wp.outputHeadInfo> 		<@wp.outputHeadInfo type="CSS_CA_PLGN"> 		<link href="<@wp.resourceURL />static/<@wp.printHeadInfo />" rel="stylesheet" type="text/css" /> 		</@wp.outputHeadInfo>  		<script src="<@wp.resourceURL />static/js/jquery-3.3.1.min.js"></script> 		<@wp.outputHeadInfo type="JS_CA_TOP_EXT"><script src="<@wp.printHeadInfo />"></script></@wp.outputHeadInfo> 		<@wp.outputHeadInfo type="JS_CA_EXT_GLB"><@wp.printHeadInfo /></@wp.outputHeadInfo> 		<@wp.outputHeadInfo type="JS_CA_TOP"><script src="<@wp.resourceURL />static/js/<@wp.printHeadInfo />"></script></@wp.outputHeadInfo>   		<@wp.i18n key="CAGLIARI_PORTAL_TITLE" var="siteName"/>  		<@wp.currentPage param="code" var="pageCode" />      		<@wp.currentPage param="title" var="pageTitle" />		<#if pageCode?? && pageCode != "">		<#else>			<#assign pageCode=''homepage''>		</#if>  		<#if pageCode == ''homepage''>  		 	<#assign pageTitle=siteName>  		</#if>       		<@wp.i18n key="CAGLIARI_PORTAL_DESCRIPTION" var="description"/>     <#if (webappNameVar??)>  	<@wp.info key="systemParam" paramName="applicationBaseURL" var="baseURL"/>		<#assign baseURL = baseURL?substring(0,baseURL?last_index_of(webappNameVar))>				<!-- FACEBOOK -->		<meta property="og:site_name" content="${siteName}" />		<meta property="og:type" content="article" />		<meta property="og:title" content="${pageTitle}" />		<#if description??>			<meta property="og:description" content="${description}"/>		<#else>			<meta property="og:description" content="-"/>		</#if>		<#if image?? && image != "">			<meta property="og:image" content="${baseURL}${image}" />		<#else>			<meta property="og:image" content="${baseURL}<@wp.imgURL/>logo_cagliari_print.png" />		</#if>		<meta property="fb:app_id" content="409032892503811" />		<!-- TWITTER -->		<meta name="twitter:card" content="summary_large_image" />		<meta name="twitter:site" content="@Comune_Cagliari" />		<meta name="twitter:title" content="${pageTitle}" />		<#if description??>			<meta name="twitter:description" content="${description}"/>		<#else>			<meta name="twitter:description" content="-"/>		</#if>    		<#if image?? && image != "">			<meta name="twitter:image" content="${baseURL}${image}" />		<#else>			<meta name="twitter:image" content="${baseURL}<@wp.imgURL/>logo_cagliari_print.png" />		</#if>	  <#else>		  <!-- ATTENZIONE: Definire ''webappNameVar'' nel fragment ''cagliari_template_head''-->	  </#if> 	</head>','<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]> <#assign wp=JspTaglibs["/aps-core"]> <#assign uc=JspTaglibs["/comuneCagliari-whitelabel"]>  	<html lang="<@wp.info key="currentLang" />"> 	<head> 		<base href="<@wp.info key="systemParam" paramName="applicationBaseURL" />"> 		<!--[if IE]><script type="text/javascript"> 			(function() { 				var baseTag = document.getElementsByTagName(''base'')[0]; 				baseTag.href = baseTag.href; 			})(); 		</script><![endif]-->		 		<title><@wp.i18n key="CAGLIARI_PORTAL_TITLE" /> - <@wp.currentPage param="title" /></title> 		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> 		<meta name="description" content="<@wp.i18n key="CAGLIARI_PORTAL_DESCRIPTION" />">  		<link rel="apple-touch-icon" sizes="57x57" href="<@wp.resourceURL />apple-icon-57x57.png" /> 		<link rel="apple-touch-icon" sizes="60x60" href="<@wp.resourceURL />apple-icon-60x60.png" /> 		<link rel="apple-touch-icon" sizes="72x72" href="<@wp.resourceURL />apple-icon-72x72.png" /> 		<link rel="apple-touch-icon" sizes="76x76" href="<@wp.resourceURL />apple-icon-76x76.png" /> 		<link rel="apple-touch-icon" sizes="114x114" href="<@wp.resourceURL />apple-icon-114x114.png" /> 		<link rel="apple-touch-icon" sizes="120x120" href="<@wp.resourceURL />apple-icon-120x120.png" /> 		<link rel="apple-touch-icon" sizes="144x144" href="<@wp.resourceURL />apple-icon-144x144.png" /> 		<link rel="apple-touch-icon" sizes="152x152" href="<@wp.resourceURL />apple-icon-152x152.png" /> 		<link rel="apple-touch-icon" sizes="180x180" href="<@wp.resourceURL />apple-icon-180x180.png" /> 		<link rel="icon" type="image/png" href="<@wp.resourceURL />favicon-32x32.png" sizes="32x32" /> 		<link rel="icon" type="image/png" href="<@wp.resourceURL />android-chrome-192x192.png" sizes="192x192" /> 		<link rel="icon" type="image/png" href="<@wp.resourceURL />favicon-96x96.png" sizes="96x96" /> 		<link rel="icon" type="image/png" href="<@wp.resourceURL />favicon-16x16.png" sizes="16x16" />  		<link rel="stylesheet" href="<@wp.cssURL/>bootstrap-italia.min.css" /> 		<link href="https://fonts.googleapis.com/css?family=Lora" rel="stylesheet" /> 		<link href="https://fonts.googleapis.com/css?family=Roboto+Mono:100,400,400i,500,500i" rel="stylesheet" /> 		<link rel="stylesheet" href="<@wp.cssURL/>italia-icon-font.css" /> 		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous" /> 		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" /> 		<link href="<@wp.cssURL/>cagliari.min.css" rel="stylesheet" type="text/css" /> 		<link href="<@wp.cssURL/>cagliari-print.min.css" rel="stylesheet" type="text/css" /> 		<@wp.outputHeadInfo type="CSS_CA_INT"> 		<link href="<@wp.cssURL/><@wp.printHeadInfo />" rel="stylesheet" type="text/css" /> 		</@wp.outputHeadInfo> 		<@wp.outputHeadInfo type="CSS_CA_EXT"> 		<link href="<@wp.printHeadInfo />" rel="stylesheet" type="text/css" /> 		</@wp.outputHeadInfo> 		<@wp.outputHeadInfo type="CSS_CA_PLGN"> 		<link href="<@wp.resourceURL />static/<@wp.printHeadInfo />" rel="stylesheet" type="text/css" /> 		</@wp.outputHeadInfo>  		<@wp.outputHeadInfo type="JS_CA_TOP_EXT"><script src="<@wp.printHeadInfo />"></script></@wp.outputHeadInfo> 		<@wp.outputHeadInfo type="JS_CA_TOP"><script src="<@wp.resourceURL />static/js/<@wp.printHeadInfo />"></script></@wp.outputHeadInfo>   		<@wp.i18n key="CAGLIARI_PORTAL_TITLE" var="siteName"/>  		<@wp.currentPage param="code" var="pageCode" />      		<@wp.currentPage param="title" var="pageTitle" />       		<#if pageCode == ''homepage''>  		 		<#assign pageTitle=siteName>  		</#if>       		<@wp.url page="${pageCode}" paramRepeat=true var="pageURL"/>  		<@wp.i18n key="CAGLIARI_PORTAL_DESCRIPTION" var="description"/>       		<@wp.info key="systemParam" paramName="applicationBaseURL" var="baseURL"/> 	</head>',1);

INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cagliari_template_header_social_mobile',NULL,NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>

<li class="small"><@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_SOCIAL" /></li>
 							<li class=""><a target="_blank" aria-label="<@wp.i18n key="CAGLIARI_PORTAL_EXT_SITE" /> - Twitter - <@wp.i18n key="CAGLIARI_PORTAL_NEW_WIN" />" href="https://twitter.com/Comune_Cagliari" title="<@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_SOCIAL_TW" />"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-twitter"></use></svg><span class="hidden"><@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_SOCIAL_TW" /></span></a></li>
 							<li><a target="_blank" aria-label="<@wp.i18n key="CAGLIARI_PORTAL_EXT_SITE" /> - Facebook - <@wp.i18n key="CAGLIARI_PORTAL_NEW_WIN" />" href="https://www.facebook.com/comunecagliarinews.it" title="<@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_SOCIAL_FB" />"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-facebook"></use></svg><span class="hidden"><@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_SOCIAL_FB" /></span></a></li>
 							<li><a target="_blank" aria-label="<@wp.i18n key="CAGLIARI_PORTAL_EXT_SITE" /> - YouTube - <@wp.i18n key="CAGLIARI_PORTAL_NEW_WIN" />" href="https://www.youtube.com/user/ComuneCagliariNews" title="<@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_SOCIAL_YT" />"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-youtube"></use></svg><span class="hidden"><@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_SOCIAL_YT" /></span></a></li>
							<li><a target="_blank" aria-label="<@wp.i18n key="CAGLIARI_PORTAL_EXT_SITE" /> - Telegram - <@wp.i18n key="CAGLIARI_PORTAL_NEW_WIN" />" href="https://telegram.me/comunecagliari" title="<@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_SOCIAL_TG" />"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-telegram"></use></svg><span class="hidden"><@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_SOCIAL_TG" /></span></a></li>
							<li><a target="_blank" aria-label="<@wp.i18n key="CAGLIARI_PORTAL_EXT_SITE" /> - Whatsapp - <@wp.i18n key="CAGLIARI_PORTAL_NEW_WIN" />" href="tel:+393290582872" title="<@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_SOCIAL_WA" />"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-whatsapp"></use></svg><span class="hidden"><@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_SOCIAL_WA" /></span></a></li>
							<li><a aria-label="<@wp.i18n key="CAGLIARI_PORTAL_GOTO" /> - RSS" href="<@wp.url page="rss" />" title="RSS"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-rss"></use></svg><span class="hidden">RSS</span></a></li>',NULL,0);

INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cagliari_template_menu_ap',NULL,NULL,'<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign jpavatar=JspTaglibs["/jpavatar-apsadmin-core"]>

<@wp.ifauthorized groupName="cittadini" var="isCittadino"/>
<@wp.ifauthorized groupName="administrators" var="isAdmin"/>
<#if Session.currentUser?? && Session.currentUser != "guest" && isCittadino && !isAdmin>
	<@wp.currentPage param="code" var="paginaCorrente" />
	<@wp.currentUserProfileAttribute attributeRoleName="jpspid:codicefiscale" var="utenteCodiceFiscale" />
	<@wp.currentUserProfileAttribute attributeRoleName="userprofile:firstname" var="utenteName" />
	<@wp.currentUserProfileAttribute attributeRoleName="userprofile:surname" var="utenteSurname" />
	<@wp.currentUserProfileAttribute attributeRoleName="userprofile:sex" var="utenteSex" />
	<@wp.currentUserProfileAttribute attributeRoleName="userprofile:email" var="utenteEmail" />
	<#assign utenteFullname= ''''/>
	<#assign utenteShortname= ''''/>
	<#if utenteName?? && utenteSurname??>
		<#assign utenteFullname>${utenteName} ${utenteSurname}</#assign>
		<#assign utenteShortname>${utenteName?substring(0,1)}${utenteSurname?substring(0,1)}</#assign>
	<#elseif utenteCodiceFiscale??>
		<#assign utenteFullname>${utenteCodiceFiscale}</#assign>
		<#assign utenteShortname>${utenteCodiceFiscale?substring(3,4)}${utenteCodiceFiscale?substring(0,1)}</#assign>
	</#if>
	<#assign color= (.now?long % 9) + 1/>

	<!-- Menu AP -->
	<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right" id="menuap">
		<div class="overlay"></div>
		<div class="cbp-menu-wrapper clearfix">
			<div class="user-burger bg-blu">
				<div class="usertitle-burger">
					<div class="avatar-wrapper">
						<div class="avatar size-lg bg${color}">
							<#if (currentAvatar?has_content)>
								<img src="${currentAvatar}" alt="${utenteShortname}" />
							<#else>
								${utenteShortname}
							</#if>
							<!--<span class="avatar-notifiche">3 <em class="sr-only">nuove notifiche</em></span>-->
						</div>
					</div>
					<span>Bentornat<#if utenteSex?? && utenteSex == ''F''>a<#else>o</#if><#if utenteName??><br /><strong>${utenteName}</strong></#if>!<span>
					<a href="<@wp.url page="ap_profilo" />" title="<@wp.i18n key="CAGLIARI_PORTAL_GOTOPAGE" />: <@wp.pageInfo pageCode="ap_profilo" info="title" />">
						<svg class="icon"><use xlink:href="<@wp.imgURL />ponmetroca.svg#ca-settings"></use></svg>
					</a>
				</div>
				<#if utenteEmail??><div class="usermail-burger"><a>${utenteEmail}</a></div></#if>
			</div>
			<div class="logo-burger-user">
				<div class="logoimg-burger">
					<a href="<@wp.url page="homepage" />" title="<@wp.i18n key="CAGLIARI_PORTAL_DESCRIPTION" />"> 
						<img src="<@wp.imgURL/>logo_cagliari_print.svg" alt="<@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_IMGALT" />"/>
					</a>
				</div>
				<div class="logotxt-burger">
					<a href="<@wp.url page="homepage" />" title="<@wp.i18n key="CAGLIARI_PORTAL_DESCRIPTION" />"><@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_TEXT" /></a>
				</div>
			</div>

			<h2 class="sr-only"><@wp.i18n key="CAGLIARI_HEADER_MENU_TITLE" /> - <@wp.i18n key="CAGLIARI_RESERVEDAREA" /></h2>
			
			<ul class="nav navmenu">
				<li>
					<a href="<@wp.url page="ap" />" class="list-item" title="<@wp.i18n key="CAGLIARI_PORTAL_GOTOPAGE" escapeXml=false /> <@wp.pageInfo pageCode="ap" info="title" />">
							<@wp.pageInfo pageCode="ap" info="title" />
					</a>
				</li>
				<@wp.nav var="page" spec="code(ap).subtree(1)">
					<li>
						<a href="<@wp.url page="${page.code}" />" class="list-item <#if (page.code == paginaCorrente)>current</#if>" title="<@wp.i18n key="CAGLIARI_PORTAL_GOTOPAGE" escapeXml=false /> ${page.title}">
							${page.title}
						</a>
					</li>
				</@wp.nav>
				<li>
					<a href="<@wp.info key="systemParam" paramName="applicationBaseURL" />do/logout.action" class="list-item" title="<@wp.i18n key="CAGLIARI_PORTAL_GOTOPAGE" escapeXml=false /> <@wp.i18n key="CAGLIARI_RESERVEDAREA_LOGOUT" escapeXml=false />">
						<@wp.i18n key="CAGLIARI_RESERVEDAREA_LOGOUT" escapeXml=false />
					</a>
				</li>
			</ul>
		</div>
	</nav>
	<!-- End Menu AP -->
</#if>',NULL,0);

INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cagliari_template_header_intestazione',NULL,NULL,'<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign wp=JspTaglibs["/aps-core"]>
 
<@wp.pageWithWidget var="searchResultPageVar" widgetTypeCode="jpfacetnav_results" listResult=false />
 
<!-- Intestazione -->
<div class="container header" data-ng-controller="ctrlRicerca as ctrl">
	<div class="row clearfix">
		<div class="col-xl-6 col-lg-6 col-md-8 col-sm-12 comune">
			<div class="logoprint">
				<h1>
					<img src="<@wp.imgURL/>logo_cagliari_print.png" alt="<@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_IMGALT" />"/>
					<@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_TEXT" />
				</h1>
			</div>
			<div class="logoimg">
				<a href="<@wp.url page="homepage" />" title="<@wp.i18n key="CAGLIARI_PORTAL_DESCRIPTION" />"> 
					<img src="<@wp.imgURL/>logo_cagliari.svg" alt="<@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_IMGALT" />"/>
				</a>
			</div>
			<div class="logotxt">
				<h1>
					<a href="<@wp.url page="homepage" />" title="<@wp.i18n key="CAGLIARI_PORTAL_DESCRIPTION" />"><@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_TEXT" /></a>
				</h1>
			</div>
			<!-- pulsante ricerca mobile -->
			<div class="p_cercaMobile clearfix">
				<button aria-label="<@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_SEARCH" />" class="btn btn-default btn-cerca pull-right" data-target="#searchModal" data-toggle="modal" type="button">
					<svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-search"></use></svg>
				</button>
			</div>
			<!-- pulsante ricerca mobile -->
		</div>
		
		<div class="col-xl-4 col-lg-4 d-none d-lg-block d-md-none pull-right text-right">
			<!-- social-->
			<ul class="list-inline text-right social">
				<li class="small list-inline-item"><@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_SOCIAL" /></li>
				<li class="list-inline-item"><a target="_blank" href="https://twitter.com/Comune_Cagliari" aria-label="<@wp.i18n key="CAGLIARI_PORTAL_EXT_SITE" /> - Twitter - <@wp.i18n key="CAGLIARI_PORTAL_NEW_WIN" />" title="<@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_SOCIAL_TW" />"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-twitter"></use></svg><span class="hidden"><@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_SOCIAL_TW" /></span></a></li>
				<li class="list-inline-item"><a target="_blank" href="https://www.facebook.com/comunecagliarinews.it" aria-label="<@wp.i18n key="CAGLIARI_PORTAL_EXT_SITE" /> - Facebook - <@wp.i18n key="CAGLIARI_PORTAL_NEW_WIN" />" title="<@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_SOCIAL_FB" />"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-facebook"></use></svg><span class="hidden"><@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_SOCIAL_FB" /></span></a></li>
				<li class="list-inline-item"><a target="_blank" href="https://www.youtube.com/user/ComuneCagliariNews" aria-label="<@wp.i18n key="CAGLIARI_PORTAL_EXT_SITE" /> - YouTube - <@wp.i18n key="CAGLIARI_PORTAL_NEW_WIN" />" title="<@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_SOCIAL_YT" />"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-youtube"></use></svg><span class="hidden"><@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_SOCIAL_YT" /></span></a></li>
				<li class="list-inline-item"><a target="_blank" href="https://telegram.me/comunecagliari" aria-label="<@wp.i18n key="CAGLIARI_PORTAL_EXT_SITE" /> - Telegram - <@wp.i18n key="CAGLIARI_PORTAL_NEW_WIN" />" title="<@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_SOCIAL_TG" />"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-telegram"></use></svg><span class="hidden"><@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_SOCIAL_TG" /></span></a></li>
				<li class="list-inline-item"><a target="_blank" href="tel:+393290582872" aria-label="<@wp.i18n key="CAGLIARI_PORTAL_EXT_SITE" /> - Whatsapp - <@wp.i18n key="CAGLIARI_PORTAL_NEW_WIN" />" title="<@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_SOCIAL_WA" />"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-whatsapp"></use></svg><span class="hidden"><@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_SOCIAL_WA" /></span></a></li>
				<li class="list-inline-item"><a aria-label="<@wp.i18n key="CAGLIARI_PORTAL_GOTO" /> - RSS" href="<@wp.url page="rss" />" title="RSS"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-rss"></use></svg><span class="hidden">RSS</span></a></li>
			</ul>
			<!-- social-->
		</div>

		<div class="col-xl-2 col-lg-2 col-md-4 d-none d-md-block d-md-none text-right">
			<!-- ricerca -->
			<div class="cerca float-right">
				<span><@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_SEARCH" /></span>
				<button aria-label="<@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_SEARCH" />" class="btn btn-default btn-cerca pull-right" type="button" data-toggle="modal" data-target="#searchModal">
					<svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-search"></use></svg>
				</button>
			</div>
			<!-- ricerca -->
		</div>
	</div>
</div>
<!-- Intestazione -->','<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
 <#assign wp=JspTaglibs["/aps-core"]>
 
 <@wp.pageWithWidget var="searchResultPageVar" widgetTypeCode="jpfacetnav_results" listResult=false />
 
 				<!-- Intestazione -->
 				<div class="container header">
 					<div class="row clearfix">
 						<div class="col-xl-7 col-lg-7 col-md-8 col-sm-12 comune">
 							<div class="logoprint">
 								<h1>
 									<img src="<@wp.imgURL/>logo_cagliari_print.png" alt="<@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_IMGALT" />"/>
									<@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_TEXT" />
 								</h1>
 							</div>
 							<div class="logoimg">
 								<a href="<@wp.url page="homepage" />" title="<@wp.i18n key="CAGLIARI_PORTAL_DESCRIPTION" />"> 
 									<img src="<@wp.imgURL/>logo_cagliari.svg" alt="<@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_IMGALT" />"/>
 								</a>
 							</div>
 							<div class="logotxt">
 								<h1>
 									<a href="<@wp.url page="homepage" />" title="<@wp.i18n key="CAGLIARI_PORTAL_DESCRIPTION" />"><@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_TEXT" /></a>
 								</h1>
 							</div>
 							<!-- pulsante ricerca mobile -->
 							<div class="p_cercaMobile clearfix">
 								<span class="input-group-btn">
 									<button class="btn btn-default btn-cerca pull-right" data-target="#cercaMobile" data-toggle="collapse" type="button">
 										<i class="material-icons">search</i>
 									</button>
 								</span>
 							</div>
 							<!-- pulsante ricerca mobile -->
 						</div>
 						
 						<div class="col-xl-3 col-lg-3 d-none d-lg-block d-md-none pull-right text-right">
 							<!-- social-->
 							<ul class="list-inline text-right social">
 								<li class="small list-inline-item"><@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_SOCIAL" /></li>
 								<li class="list-inline-item"><a href="#" title="<@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_SOCIAL_TW" />"><i class="fab fa-twitter" aria-hidden="true"></i><span class="hidden"><@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_SOCIAL_TW" /></span></a></li>
 								<li class="list-inline-item"><a href="#" title="<@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_SOCIAL_FB" />"><i class="fab fa-facebook-f" aria-hidden="true"></i><span class="hidden"><@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_SOCIAL_FB" /></span></a></li>
 								<li class="list-inline-item"><a href="#" title="<@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_SOCIAL_MD" />"><i class="fab fa-medium-m" aria-hidden="true"></i><span class="hidden"><@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_SOCIAL_MD" /></span></a></li>
 							</ul>
 							<!-- social-->
 						</div>

 						<div class="col-xl-2 col-lg-2 col-md-4 d-none d-md-block d-md-none text-right">
 							<!-- ricerca -->
 							<div class="cerca float-right">
 								<form action="#" method="post">
 									<@wp.pageWithWidget var="searchResultPageVar" widgetTypeCode="search_result" listResult=false />
 									<div class="input-group">
 										<label for="cm_search" class="hidden"><@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_SEARCH" /></label>
 										<input type="text" class="form-control" placeholder="<@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_SEARCH" />" name="search" id="cm_search" />
 										<span>
 											<button class="btn btn-default btn-cerca pull-right" type="submit">
 												<i class="material-icons">search</i>
 											</button>
 										</span>                     
 									</div>
 								</form>
 							</div>
 							<!-- ricerca -->
 						</div>
 					</div>
 				</div>
 				<!-- Intestazione -->
 
 				<!-- Ricerca Mobile -->
 				<div class="collapse" id="cercaMobile" aria-expanded="false" role="form">
 					<form action="#" method="post">
 						<div class="container">
 							<div class="row">
 								<label for="cm_searchmobile" class="hidden"><@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_SEARCH" /></label>
 								<input type="text" class="form-control squared" placeholder="<@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_SEARCH" />" name="search" id="cm_searchmobile" />
 							</div>
 						</div>
 					</form>
 				</div>      
 				<!-- Ricerca Mobile -->',1);

INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cagliari_template_footer_intestazione',NULL,NULL,'<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
 <#assign wp=JspTaglibs["/aps-core"]>  
 
 					<section class="footer-logo">
 						<div class="row clearfix">
 							<div class="col-sm-12 intestazione">
 								<div class="logoimg">
 									<a href="<@wp.url page="homepage" />" title="<@wp.i18n key="CAGLIARI_PORTAL_DESCRIPTION" />">
 										<img src="<@wp.imgURL/>logo_cagliari.svg" alt="<@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_IMGALT" />"/>
 									</a>
 								</div>
 								<div class="logotxt">
 									<h3>
 										<a href="<@wp.url page="homepage" />" title="<@wp.i18n key="CAGLIARI_PORTAL_GOTO" /> <@wp.i18n key="CAGLIARI_PORTAL_DESCRIPTION" />"><@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_TEXT" /></a>
 									</h3>
 								</div>
 							</div>
 						</div>
 					</section>','<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
 <#assign wp=JspTaglibs["/aps-core"]>  
 
 					<section>
 						<div class="row clearfix">
 							<div class="col-sm-12 intestazione">
 								<div class="logoimg">
 									<a href="<@wp.url page="homepage" />" title="<@wp.i18n key="CAGLIARI_PORTAL_DESCRIPTION" />">
 										<img src="<@wp.imgURL/>logo_cagliari.svg" alt="<@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_IMGALT" />"/>
 									</a>
 								</div>
 								<div class="logotxt">
 									<h3>
 										<a href="<@wp.url page="homepage" />" title="<@wp.i18n key="CAGLIARI_PORTAL_GOTO" /> <@wp.i18n key="CAGLIARI_PORTAL_DESCRIPTION" />"><@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_TEXT" /></a>
 									</h3>
 								</div>
 							</div>
 						</div>
 					</section>',1);

INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cagliari_template_footer_info',NULL,NULL,'<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign wp=JspTaglibs["/aps-core"]>

<#-- Elenco dei codici di pagina referenziati nel footer -->
<#assign ammTraspPageIdVar=''amministrazione_trasparente'' >
<#assign certEmailPageIdVar=''posta_elettronica_certificata'' >
<#assign organizzazionePageIdVar=''organizzazione'' >
<#assign scriviComunePageIdVar=''scrivi_comune'' >
<#assign rubricaDipendentiPageIdVar=''rubrica'' >
<#assign rssPageIdVar=''rss'' >
<#-- Contenuto da mostrare nel caso la pagina relativa all''organizzazione sia una di pubblicazione volante -->
<#-- assign organizzazioneContentIdVar='''' -->



<#if (!ammTraspPageIdVar??)><!--ATTENZIONE! Definire ''ammTraspPageIdVar'' nel fragment ''cagliari_template_footer_info''--><#assign ammTraspPageIdVar=''home''></#if><#if (!certEmailPageIdVar??)><!--ATTENZIONE! Definire ''certEmailPageIdVar'' nel fragment ''cagliari_template_footer_info''--><#assign certEmailPageIdVar=''home''></#if><#if (!organizzazionePageIdVar??)><!--ATTENZIONE! Definire ''organizzazionePageIdVar'' nel fragment ''cagliari_template_footer_info''--><#assign organizzazionePageIdVar=''home''></#if><#if (!scriviComunePageIdVar??)><!--ATTENZIONE! Definire ''scriviComunePageIdVar'' nel fragment ''cagliari_template_footer_info''--><#assign scriviComunePageIdVar=''home''></#if><#if (!rubricaDipendentiPageIdVar??)><!--ATTENZIONE! Definire ''rubricaDipendentiPageIdVar'' nel fragment ''cagliari_template_footer_info''--><#assign rubricaDipendentiPageIdVar=''home''></#if><#if (!rssPageIdVar??)><!--ATTENZIONE! Definire ''rssPageIdVar'' nel fragment ''cagliari_template_footer_info''--><#assign rssPageIdVar=''home''></#if><div class="col-lg-3 col-md-3 col-sm-6 footer-ammtrasp"> 	<h4><a href="<@wp.url page="${ammTraspPageIdVar}" />" title="<@wp.i18n key="CAGLIARI_PORTAL_GOTOPAGE" />: <@wp.pageInfo pageCode="amministrazione_trasparente" info="title" />"><@wp.pageInfo pageCode="amministrazione_trasparente" info="title" /></a></h4> 	<p><@wp.i18n key="CAGLIARI_FOOTER_AMMTRASP_TEXT" /></p></div><div class="col-lg-3 col-md-3 col-sm-6 footer-contatti"> 	<h4><@wp.i18n key="CAGLIARI_FOOTER_CONTACT_TITLE" /></h4> 	<p> 	 	<strong><@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_TEXT" /></strong><br /> 	 	<@wp.i18n key="CAGLIARI_FOOTER_CONTACT_ADDRESS" /><br /> 	 	<@wp.i18n key="CAGLIARI_FOOTER_CONTACT_CODFISC" /> / <abbr title="<@wp.i18n key="CAGLIARI_FOOTER_CONTACT_PIVA" />"><@wp.i18n key="CAGLIARI_FOOTER_CONTACT_PIVA_ABBR" />:</abbr> <@wp.i18n key="CAGLIARI_FOOTER_CONTACT_PIVA_VAL" /><br /> 	 	 <abbr title="<@wp.i18n key="CAGLIARI_FOOTER_CONTACT_IBAN_ACRO" />">IBAN</abbr>: <@wp.i18n key="CAGLIARI_FOOTER_CONTACT_IBAN_VAL" /> 	</p> 	<ul class="footer-list clearfix">		<li><a href="<@wp.url page="${certEmailPageIdVar}" />" title="<@wp.i18n key="CAGLIARI_PORTAL_GOTOPAGE" />: <@wp.pageInfo pageCode="posta_elettronica_certificata" info="title" />"><@wp.pageInfo pageCode="posta_elettronica_certificata" info="title" /></a></li>		<li><a href="<@wp.url page="${organizzazionePageIdVar}"><#if (organizzazioneContentIdVar??)><@wp.parameter name="contentId" value="${organizzazioneContentIdVar}" /></#if></@wp.url>" title="<@wp.i18n key="CAGLIARI_PORTAL_GOTOPAGE" />: <@wp.i18n key="CAGLIARI_FOOTER_CONTACT_URP" />"><@wp.i18n key="CAGLIARI_FOOTER_CONTACT_URP" /></a></li>	</ul></div><div class="col-lg-3 col-md-3 col-sm-6 footer-contatti"> 	<h4><span class="sr-only"><@wp.i18n key="CAGLIARI_FOOTER_CONTACT_NUMBERS" /></span>&nbsp;</h4> 	<p> 	 	<@wp.i18n key="CAGLIARI_FOOTER_CONTACT_CENTRAL" /><br /> 	 	<@wp.i18n key="CAGLIARI_FOOTER_CONTACT_GREENNUM" /><br /> 	 	<@wp.i18n key="CAGLIARI_FOOTER_CONTACT_WHTPP" /> 	</p><ul class="footer-list clearfix">		<li><a href="<@wp.url page="${scriviComunePageIdVar}" />" title="<@wp.i18n key="CAGLIARI_PORTAL_GOTOPAGE" />: <@wp.pageInfo pageCode="scrivi_comune" info="title" />"><@wp.pageInfo pageCode="scrivi_comune" info="title" /></a></li>		<li><a href="<@wp.url page="${rubricaDipendentiPageIdVar}" />" title="<@wp.i18n key="CAGLIARI_PORTAL_GOTOPAGE" />: <@wp.pageInfo pageCode="amm_05_01" info="title" />"><@wp.pageInfo pageCode="amm_05_01" info="title" /></a></li>	</ul></div><div class="col-lg-3 col-md-3 col-sm-6 footer-seguici"> 	<h4><@wp.i18n key="CAGLIARI_FOOTER_CONTACT_FOLLOW" /></h4> 	<ul class="list-inline text-left social"> 		<li class="list-inline-item"><a aria-label="<@wp.i18n key="CAGLIARI_PORTAL_EXT_SITE" /> - Twitter - <@wp.i18n key="CAGLIARI_PORTAL_NEW_WIN" />" href="https://twitter.com/Comune_Cagliari" title="<@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_SOCIAL_TW" />"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-twitter"></use></svg><span class="hidden"><@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_SOCIAL_TW" /></span></a></li> 								<li class="list-inline-item"><a target="_blank" aria-label="<@wp.i18n key="CAGLIARI_PORTAL_EXT_SITE" /> - Facebook - <@wp.i18n key="CAGLIARI_PORTAL_NEW_WIN" />" href="https://www.facebook.com/comunecagliarinews.it" title="<@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_SOCIAL_FB" />"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-facebook"></use></svg><span class="hidden"><@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_SOCIAL_FB" /></span></a></li> 								<li class="list-inline-item"><a target="_blank" aria-label="<@wp.i18n key="CAGLIARI_PORTAL_EXT_SITE" /> - YouTube - <@wp.i18n key="CAGLIARI_PORTAL_NEW_WIN" />" href="https://www.youtube.com/user/ComuneCagliariNews" title="<@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_SOCIAL_YT" />"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-youtube"></use></svg><span class="hidden"><@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_SOCIAL_YT" /></span></a></li>								<li class="list-inline-item"><a target="_blank" aria-label="<@wp.i18n key="CAGLIARI_PORTAL_EXT_SITE" /> - Telegram - <@wp.i18n key="CAGLIARI_PORTAL_NEW_WIN" />" href="https://telegram.me/comunecagliari" title="<@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_SOCIAL_TG" />"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-telegram"></use></svg><span class="hidden"><@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_SOCIAL_TG" /></span></a></li>								<li class="list-inline-item"><a target="_blank" aria-label="<@wp.i18n key="CAGLIARI_PORTAL_EXT_SITE" /> - Whatsapp - <@wp.i18n key="CAGLIARI_PORTAL_NEW_WIN" />" href="tel:+393290582872" title="<@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_SOCIAL_WA" />"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-whatsapp"></use></svg><span class="hidden"><@wp.i18n key="CAGLIARI_HEADER_INTESTAZIONE_SOCIAL_WA" /></span></a></li>								<li class="list-inline-item"><a aria-label="<@wp.i18n key="CAGLIARI_PORTAL_GOTO" /> - RSS" href="<@wp.url page="${rssPageIdVar}" />" title="RSS"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-rss"></use></svg><span class="hidden">RSS</span></a></li> 	</ul></div>','<div class="col-lg-3 col-md-3 col-sm-6"> 	<h4>Contatti</h4> 	<p> 	 	<strong>Comune di Cagliari</strong><br /> 	 	Via Roma 145 - 09124 Cagliari 	 	Codice fiscale / P. IVA: 00147990923 	</p> 	<ul class="footer-list clearfix">		<li><a href="#" title="Vai alla pagina: Posta Elettronica Certificata">Posta Elettronica Certificata</a></li>		<li><a href="#" title="Vai alla pagina: URP - Ufficio Relazioni con il Pubblico">URP - Ufficio Relazioni con il Pubblico</a></li>	</ul></div><div class="col-lg-3 col-md-3 col-sm-6"> 	<h4 class="d-none d-sm-block">&nbsp;</h4> 	<p> 	 	<strong><a href="#" title="Vai alla pagina: Amministrazione Trasparente">Amministrazione Trasparente</a></strong><br /> 	 	I dati personali pubblicati sono riutilizzabili solo alle condizioni previste dalla direttiva comunitaria 2003/98/CE e dal d.lgs. 36/2006 	</p></div><div class="col-lg-3 col-md-3 col-sm-6"> 	<h4>Newsletter</h4> 	 <form action="#" method="post"> 		<label for="cm_newsletter">Iscriviti per riceverla</label> 		<input type="text" class="form-control" placeholder="Scrivi il tuo indirizzo email" name="newsletter" id="cm_newsletter"> 		<button class="btn btn-default btn-iscriviti pull-right" type="submit">Iscriviti</button>                  	</form></div><div class="col-lg-3 col-md-3 col-sm-6"> 	<h4>Seguici su</h4> 	<ul class="list-inline text-left social"> 		<li class="list-inline-item"><a href="#" title="Seguici su Twitter"><i class="fab fa-twitter" aria-hidden="true"></i><span class="hidden">Seguici su Twitter</span></a></li> 		<li class="list-inline-item"><a href="#" title="Seguici su Facebook"><i class="fab fa-facebook-f" aria-hidden="true"></i><span class="hidden">Seguici su Facebook</span></a></li> 		<li class="list-inline-item"><a href="#" title="Seguici su Medium"><i class="fab fa-medium-m" aria-hidden="true"></i><span class="hidden">Seguici su Medium</span></a></li> 	</ul></div>',0);

INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cagliari_template_footer_link',NULL,NULL,'<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign wp=JspTaglibs["/aps-core"]>  

<section class="postFooter clearfix">
	<div class="link_piede_footer">
		<h3 class="sr-only"><@wp.i18n key="CAGLIARI_FOOTER_LINKUTILI_TITLE" /></h3>
		<a href="<@wp.url page="privacy" />" title="<@wp.pageInfo pageCode="privacy" info="title" />" lang="en"><@wp.pageInfo pageCode="privacy" info="title" /></a> | 
		<a href="<@wp.url page="note_legali" />" title="<@wp.pageInfo pageCode="note_legali" info="title" />"><@wp.pageInfo pageCode="note_legali" info="title" /></a> | 
		<a href="<@wp.url page="accessibilita" />" title="<@wp.pageInfo pageCode="accessibilita" info="title" />"><@wp.pageInfo pageCode="accessibilita" info="title" /></a> | 
		<a href="<@wp.url page="redazione" />" title="<@wp.pageInfo pageCode="redazione" info="title" />"><@wp.pageInfo pageCode="redazione" info="title" /></a>
	</div>
	<div class="loghi_progetto">
		<div class="lista_loghi_progetto">
			<span class="loghi_progetto_img logo_progetto_1"><a target="blank" aria-label="<@wp.i18n key="CAGLIARI_FOOTER_SITO_PONMETRO_ARIA" />" href="http://www.ponmetro.it/home/ecosistema/viaggio-nei-cantieri-pon-metro/pon-metro-cagliari/" title="<@wp.i18n key="CAGLIARI_FOOTER_SITO_PONMETRO" />"><img src="<@wp.imgURL />footer_ponmetro.svg" alt="<@wp.i18n key="CAGLIARI_FOOTER_LOGO_PONMETRO" />" /></a></span>
			<span class="loghi_progetto_img logo_progetto_2"><a target="blank" aria-label="<@wp.i18n key="CAGLIARI_FOOTER_SITO_REPUBBLICA_ARIA" />" href="http://www.agenziacoesione.gov.it" title="<@wp.i18n key="CAGLIARI_FOOTER_SITO_REPUBBLICA" />"><img src="<@wp.imgURL />footer_repubblica.svg" alt="<@wp.i18n key="CAGLIARI_FOOTER_LOGO_REPUBBLICA" />" /></a></span>
			<span class="loghi_progetto_img logo_progetto_3"><a target="blank" aria-label="<@wp.i18n key="CAGLIARI_FOOTER_SITO_UE_ARIA" />" href="https://europa.eu/european-union/index_it" title="<@wp.i18n key="CAGLIARI_FOOTER_SITO_UE" />"><img src="<@wp.imgURL />footer_ue.svg" alt="<@wp.i18n key="CAGLIARI_FOOTER_LOGO_UE" />" /></a></span>
		</div> 
		<div class="testo_progetto">
			<p><@wp.i18n key="CAGLIARI_FOOTER_TXT_PROGETTO" /></p>
		</div>
	</div> 
</section>','<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
 <#assign wp=JspTaglibs["/aps-core"]>  
 
 					<section class="postFooter clearfix">
 						<h3 class="sr-only"><@wp.i18n key="CAGLIARI_FOOTER_LINKUTILI_TITLE" /></h3>
 							<a href="#" title="Privacy-Cookies">Privacy-Cookies</a> | 
 							<a href="#" title="Note Legali">Note Legali</a> | 
 							<a href="#" title="Contatti">Contatti</a> | 
 							<a href="#" title="AccessibilitÃ ">AccessibilitÃ </a> | 
 							<a href="#" title="Mappa del sito">Mappa del sito</a> | 
 							<a href="#" title="La Redazione">La Redazione</a>
 					</section>',1);

INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cagliari_template_backtotop',NULL,NULL,'<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
 <#assign wp=JspTaglibs["/aps-core"]>  
 
 			<div id="topcontrol" class="topcontrol bg-bluscuro" title="<@wp.i18n key="CAGLIARI_PORTAL_BACKTOTOP" />">
 				<svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-keyboard_arrow_up"></use></svg>
 			 </div>','<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
 <#assign wp=JspTaglibs["/aps-core"]>  
 
 			<div id="topcontrol" class="topcontrol bg-bluscuro" title="<@wp.i18n key="CAGLIARI_PORTAL_BACKTOTOP" />">
 				<svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-keyboard_arrow_up"></use></svg>
 			 </div>',1);

INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cagliari_template_footer_js',NULL,NULL,'<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign wp=JspTaglibs["/aps-core"]>
<@wp.categories var="systemCategories" titleStyle="full" />

<link href="<@wp.cssURL/>jquery-ui.min.css" rel="stylesheet" type="text/css" />
<@wp.outputHeadInfo type="CSS_CA_BTM"><link href="<@wp.cssURL/><@wp.printHeadInfo />" rel="stylesheet" type="text/css" /></@wp.outputHeadInfo>
<@wp.outputHeadInfo type="CSS_CA_BTM_EXT"><link href="<@wp.printHeadInfo />" rel="stylesheet" type="text/css" /></@wp.outputHeadInfo>
<@wp.outputHeadInfo type="CSS_CA_BTM_EXT_GLB"><@wp.printHeadInfo /></@wp.outputHeadInfo>


<link href="<@wp.cssURL/>angular-material.min.css" rel="stylesheet" type="text/css" />

<script src="<@wp.resourceURL />static/js/popper.min.js"></script>

<script src="<@wp.resourceURL />static/js/bootstrap-italia_1.2.0sf.min.js"></script>
<script src="<@wp.resourceURL />static/js/cagliari.min.js"></script>
<script src="<@wp.resourceURL />static/js/jquery-ui.min.js"></script>
<script src="<@wp.resourceURL />static/js/i18n/datepicker-it.min.js"></script>
<script src="<@wp.resourceURL />static/js/angular.min.js"></script>
<script src="<@wp.resourceURL />static/js/angular-animate.min.js"></script>
<script src="<@wp.resourceURL />static/js/angular-aria.min.js"></script>
<script src="<@wp.resourceURL />static/js/angular-messages.min.js"></script>
<script src="<@wp.resourceURL />static/js/angular-sanitize.min.js"></script>
<script src="<@wp.resourceURL />static/js/app.min.js"></script>
<script src="<@wp.resourceURL />static/js/ricerca.min.js"></script>
<script src="<@wp.resourceURL />static/js/ricerca-service.min.js"></script>
<script src="<@wp.resourceURL />static/js/general-service.js"></script>
<script src="<@wp.resourceURL />static/js/angular-material.cagliari.min.js"></script>
<script src="<@wp.resourceURL />static/js/jquery.cookie.min.js"></script>
<script src="<@wp.resourceURL />static/js/jquery.cookiecuttr.min.js"></script>

<@wp.outputHeadInfo type="JS_CA_BTM_EXT"><script src="<@wp.printHeadInfo />"></script></@wp.outputHeadInfo>
<@wp.outputHeadInfo type="JS_CA_BTM_EXT_GLB"><@wp.printHeadInfo /></@wp.outputHeadInfo>
<@wp.outputHeadInfo type="JS_CA_BTM"><script src="<@wp.resourceURL />static/js/<@wp.printHeadInfo />"></script></@wp.outputHeadInfo>
<@wp.outputHeadInfo type="JS_CA_BTM_PLGN"><script src="<@wp.resourceURL />static/<@wp.printHeadInfo />"></script></@wp.outputHeadInfo>

<script>
  <@wp.outputHeadInfo type="JS_CA_BTM_INC">
	<@wp.printHeadInfo />
  </@wp.outputHeadInfo>
</script>

<!-- COOKIE BAR, ANALITICS e HOTJAR  -->
<script>
	(function ($) {
		$(document).ready(function () {
			// activate cookie cutter
			$.cookieCuttr({
				cookieAnalytics: false,
				cookieMessage: ''<p><@wp.i18n key="CAGLIARI_FOOTER_COOKIES_TEXT" />&nbsp;<a href="<@wp.url page="privacy" />" class="text-white" title="<@wp.i18n key="CAGLIARI_PORTAL_GOTO" /> <@wp.i18n key="CAGLIARI_PORTAL_PRIVACYPOLICY" />"><@wp.i18n key="CAGLIARI_PORTAL_PRIVACYPOLICY" /></a>.<br/><@wp.i18n key="CAGLIARI_FOOTER_COOKIES_QUESTION" /></p>'',
				cookieAcceptButtonText: ''<@wp.i18n key="CAGLIARI_FOOTER_ACCEPTS_COOKIES" />'',
				cookieDiscreetPosition: "bottomleft",	
				cookieDeclineButton: true,
				//cookieResetButton: true,
				cookieDeclineButtonText: ''<@wp.i18n key="CAGLIARI_FOOTER_DECLINES_COOKIES" />'',
				//cookieResetButtonText: ''Cambia scelta cookies'',
				cookieDomain: ''comune.cagliari.it''
			});
		});
	})(jQuery);	
	
	if (jQuery.cookie(''cc_cookie_accept'') == "cc_cookie_accept") {	
		  (function(i,s,o,g,r,a,m){i[''GoogleAnalyticsObject'']=r;i[r]=i[r]||function(){
				   (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				   m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,''script'',''https://www.google-analytics.com/analytics.js'',''ga'');
		  ga(''create'', ''UA-27957186-1'', ''auto'');
		  ga(''set'', ''anonymizeIp'', true);
		  ga(''send'', ''pageview'');

		 (function(h,o,t,j,a,r){
		 		 h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
		 		 h._hjSettings={hjid:553641,hjsv:6};
		 		 a=o.getElementsByTagName(''head'')[0];
		 		 r=o.createElement(''script'');r.async=1;
		 		 r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
		 		 a.appendChild(r);
		 })(window,document,''https://static.hotjar.com/c/hotjar-'',''.js?sv='');
	} 
</script>
<@wp.fragment code="cagliari_template_search_modal" escapeXml=false />','<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
 <#assign wp=JspTaglibs["/aps-core"]>    
 		 <@wp.outputHeadInfo type="CSS_CA_BTM">
 		<link href="<@wp.cssURL/><@wp.printHeadInfo />" rel="stylesheet" type="text/css" />
 		</@wp.outputHeadInfo>
 		  <@wp.outputHeadInfo type="CSS_CA_BTM_EXT"><link href="<@wp.printHeadInfo />" rel="stylesheet" type="text/css" /></@wp.outputHeadInfo>

             <script src="<@wp.resourceURL />static/js/bootstrap-italia.bundle.min.js"></script>
             <script type="text/javascript" src="<@wp.resourceURL />static/js/cagliari.min.js"></script>
             <@wp.outputHeadInfo type="JS_CA_BTM_EXT"><script src="<@wp.printHeadInfo />"></script></@wp.outputHeadInfo>
             <@wp.outputHeadInfo type="JS_CA_BTM"><script src="<@wp.resourceURL />static/js/<@wp.printHeadInfo />"></script></@wp.outputHeadInfo>
             <@wp.outputHeadInfo type="JS_CA_BTM_PLGN"><script src="<@wp.resourceURL />static/<@wp.printHeadInfo />"></script></@wp.outputHeadInfo>
             <script>
             <@wp.outputHeadInfo type="JS_CA_BTM_INC">
                         <@wp.printHeadInfo />
             </@wp.outputHeadInfo>
             </script>
 		<#if (RequestParameters.login??) >
 			<script>
 				$( document ).ready(function() {
 					$(''#accessModal'').modal(''show'');
 				});
 			</script>
 		</#if>
 		<!-- ACCESSO AREA RISERVATA -->
 		<div class="modal fade" id="accessModal" tabindex="-1" role="dialog" aria-labelledby="modalaccess">
 			<div class="modal-dialog" role="document">
 				<div class="modal-content">
 					<div class="modal-header">
 						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
 						<h4 class="modal-title" id="modalaccess"><@wp.i18n key="CAGLIARI_HEADER_ACCESS_TITLE" /></h4>
 					</div>
 					<div class="modal-body">
 						<div class="modal-footer">
 							<h4><@wp.i18n key="CAGLIARI_HEADER_ACCESS_PORTAL" /></h4>
 							<#if (Session.currentUser.username != "guest") >
 							<h5>Username: ${Session.currentUser}</h5>
 							<div class="mt30 mb30">
 							<#if (Session.currentUser.entandoUser) >
 								<#if (!Session.currentUser.credentialsNotExpired) >
 									<div class="alert alert-block">
 										<p><@wp.i18n key="USER_STATUS_EXPIRED_PASSWORD" />: <a href="<@wp.info key="systemParam" paramName="applicationBaseURL" />do/editPassword.action"><@wp.i18n key="USER_STATUS_EXPIRED_PASSWORD_CHANGE" /></a></p>
 									</div>
 								</#if>
 							</#if>
 							<@wp.ifauthorized permission="enterBackend">
 								<div class="btn-group">
 									<a href="<@wp.info key="systemParam" paramName="applicationBaseURL" />do/main.action?request_locale=<@wp.info key="currentLang" />" class="btn bg-blu"><@wp.i18n key="ADMINISTRATION" /></a>
 								</div>
 							</@wp.ifauthorized>
 							<p class="pull-right"><a href="<@wp.info key="systemParam" paramName="applicationBaseURL" />do/logout.action" class="btn bg-blu"><@wp.i18n key="LOGOUT" /></a></p>
 							<@wp.pageWithWidget widgetTypeCode="userprofile_editCurrentUser" var="userprofileEditingPageVar" listResult=false />
 							<#if (userprofileEditingPageVar??) >
 								<p><a href="<@wp.url page="${userprofileEditingPageVar.code}" />" ><@wp.i18n key="userprofile_CONFIGURATION" /></a></p>
 							</#if>
 							</div>
 						<#else>
 							<#if (accountExpired?? && accountExpired == true) >
 								<div class="alert alert-block alert-error">
 									<p><@wp.i18n key="USER_STATUS_EXPIRED" /></p>
 								</div>
 								</#if>
 								<#if (wrongAccountCredential?? && wrongAccountCredential == true) >
 								<div class="alert alert-block alert-error">
 									<p><@wp.i18n key="USER_STATUS_CREDENTIALS_INVALID" /></p>
 								</div>
 							</#if>
 							<form action="<@wp.url/>?login=true" method="post">
 								<#if (RequestParameters.returnUrl??) >
 									<input type="hidden" name="returnUrl" value="${RequestParameters.returnUrl}" />
 								</#if>
 								<div class="form-group">
 									<label for="username" class="control-label">Username</label>
 									<input type="text" class="form-control" name="username" id="username" placeholder="<@wp.i18n key="CAGLIARI_HEADER_ACCESS_USERNAME_PLACEHOLDER" />" />
 								</div>
 								<div class="form-group">
 									<label for="password_modal" class="control-label">Password</label>
 									<input type="password" class="form-control" name="password" id="password_modal" placeholder="<@wp.i18n key="CAGLIARI_HEADER_ACCESS_PASSWORD_PLACEHOLDER" />" />
 								</div>
 								<div class="form-group small text-center mt15 mb15">
 									<input type="submit" class="btn btn-primary" value="Login" />
 								</div>
 							</form>
 						</#if>
 						</div>
 					</div>
 				</div>
 			</div>
 		</div>',1);

INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cagliari_template_search_modal',NULL,NULL,'<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign cw=JspTaglibs["/comuneCagliari-whitelabel"]>

<#-- Inserire widget di defalt con i risultati della ricerca -->
<#assign searchResultWidgetCode=''search_result''>

<@wp.categories var="systemCategories" root="arg_argomenti" />
<@wp.currentPage param="code" var="code" /><@wp.pageWithWidget widgetTypeCode="${searchResultWidgetCode}" var="searchWidgetPage" /><#if (searchWidgetPage??)><@cw.configWidget pageCode="${searchWidgetPage.code}" widgetCode="${searchResultWidgetCode}" configParam="contentTypesFilter" var="tipiContenuto" /><#else><!--ATTENZIONE! Definire ''searchResultWidgetCode'' nel fragment ''cagliari_template_search_modal'' --></#if><#if (!tipiContenuto??)><#assign tipiContenuto = ""/></#if><div id="dvRicerca">	<div data-ng-cloak data-ng-controller="ctrlRicerca as ctrl">		<!-- CERCA -->		<div class="modal fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="searchModalTitle" aria-hidden="false">			<script>				$(function() {				  $("#datepicker_start").datepicker();				  $("#datepicker_end").datepicker();				});			</script>			<div class="modal-dialog" role="document">				<div class="modal-content">					<form id="ricerca" action="<#if (searchWidgetPage??)><@wp.url page="${searchWidgetPage.code}"/></#if>" method="post">						<div class="modal-header-fullsrc">							<div class="container">								<div class="row">									<div class="col-sm-1" data-ng-class="{pb12: !ctrl.General.getFiltriMode().filtri}">										<button data-ng-hide="ctrl.General.getFiltriMode().filtri && !ctrl.General.getFiltriMode().internal" type="button" class="close" data-dismiss="modal" aria-label="<@wp.i18n key="CAGLIARI_PORTAL_CLOSE_SEARCH_FILTERS" />" data-ng-click="ctrl.General.setFiltriMode(false, false)">											<svg class="icon"<use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-arrow_back"></use></svg>										</button>										<button data-ng-show="ctrl.General.getFiltriMode().filtri && !ctrl.General.getFiltriMode().internal" type="button" class="close" aria-label="<@wp.i18n key="CAGLIARI_PORTAL_CLOSE_SEARCH_FILTERS" />" data-ng-click="ctrl.General.setFiltriMode(false, false)">											<svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-arrow_back"></use></svg>										</button>									</div>									<div class="col-sm-11">										<h1 class="modal-title" id="searchModalTitle">											<span data-ng-hide="ctrl.General.getFiltriMode().filtri"><@wp.i18n key="CAGLIARI_HEADER_SEARCH_TITLE" /></span>											<span data-ng-show="ctrl.General.getFiltriMode().filtri"><@wp.i18n key="CAGLIARI_HEADER_FILTER_TITLE" /></span>										</h1>										<button data-ng-show="ctrl.General.getFiltriMode().filtri" class="confirm btn btn-default btn-trasparente float-right"><@wp.i18n key="CAGLIARI_PORTAL_CONFIRM" /></button>									</div>								</div>							</div>							<div class="search-filter-type ng-hide" data-ng-show="ctrl.General.getFiltriMode().filtri">								<ul class="nav nav-tabs" role="tablist">									<li role="presentation">										<a data-ng-if="ctrl.General.getFiltriMode().categoria == ''''" href="" aria-controls="categoriaTab" role="tab" data-toggle="tab" data-ng-click="ctrl.Ricerca.setActive(''categorie'')" data-ng-class="ctrl.Ricerca.get_categoriaTab()"><@wp.i18n key="CAGLIARI_PORTAL_CATEGORIES" /></a>										<a data-ng-if="ctrl.General.getFiltriMode().categoria != ''''" href="" aria-controls="categoriaTab" role="tab" data-toggle="tab" data-ng-click="ctrl.Ricerca.setActive(''categorie'')" data-ng-class="ctrl.Ricerca.get_categoriaTab()">{{ctrl.General.getFiltriMode().categoria}}</a>									</li>									<li role="presentation"><a href="" aria-controls="argomentoTab" role="tab" data-toggle="tab"										data-ng-click="ctrl.Ricerca.setActive(''argomenti'')" data-ng-class="ctrl.Ricerca.get_argomentoTab()"><@wp.i18n key="CAGLIARI_CONTENT_ARGUMENTS" /></a></li>									<li role="presentation"><a href="" aria-controls="opzioniTab" role="tab" data-toggle="tab"										data-ng-click="ctrl.Ricerca.setActive(''opzioni'')" data-ng-class="ctrl.Ricerca.get_opzioniTab()"><@wp.i18n key="CAGLIARI_CONTENT_OPTIONS" /></a></li>								</ul>							</div>						</div>						<div class="modal-body-search">							<div class="container">								<div class="row" data-ng-hide="ctrl.General.getFiltriMode().filtri">									<div class="col-lg-12 col-md-12 col-sm-12">										<div class="form-group" data-ng-init="ctrl.Ricerca.setContentTypes(''${tipiContenuto}'')">											<label for="cerca-txt" class="sr-only active"><@wp.i18n key="CAGLIARI_HEADER_SEARCH_TITLE" /></label>											<md-autocomplete type="text" md-input-name ="cercatxt" md-input-id ="cerca-txt"	placeholder="<@wp.i18n key="CAGLIARI_PORTAL_SEARCH_INFORMATION_PEOPLE_SERVICES" />" md-selected-item="ctrl.Ricerca.cercatxt"												md-search-text="ctrl.Ricerca.searchStringRicercaTxt" md-selected-item-change="ctrl.Ricerca.selectedRicercaTxtItemChanged(item)" md-items="item in ctrl.Ricerca.getAutocompleteResults(ctrl.Ricerca.searchStringRicercaTxt)" md-item-text="item.contentName"	md-min-length="1" md-clear-button="true">												<md-item-template>													<div>														<a data-ng-href="{{item.href}}" data-ng-if="!item.searchButton && item.searchButton != ''hidden''">															<div>																<svg class="icon icon-sm" data-ng-bind-html="item.icon"></svg>																<span class="autocomplete-list-text">																	<span data-md-highlight-text="ctrl.Ricerca.searchStringRicercaTxt" data-md-highlight-flags="i">{{item.contentName}}</span>																	<em>{{item.category}}</em>																</span>															</div>														</a>														<div class="search-start" data-ng-if="item.searchButton && item.searchButton != ''hidden''" data-ng-click="item.contentName = ctrl.Ricerca.searchStringRicercaTxt" onclick="document.getElementById(''search-button'').click();">															<svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-search"></use></svg>															<@wp.i18n key="CAGLIARI_PORTAL_SEARCH_THROUGHOUT_SITE" />														</div>														<div class="search-start-hidden" data-ng-if="item.searchButton == ''hidden''"> </div>													</div>												</md-item-template>											</md-autocomplete>											<svg class="icon ico-prefix"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-search"></use></svg>										</div>										<div data-ng-hide="ctrl.Ricerca.getAutocompleteIsOpened()">											<div class="form-filtro">												<a href="" class="btn btn-default btn-trasparente" data-ng-class="ctrl.Ricerca.class_tutti(''categorie'')" data-ng-click="ctrl.Ricerca.toggleAll(''categorie'')"><@wp.i18n key="CAGLIARI_PORTAL_ALL" /></a>												<a href="" class="btn btn-default btn-trasparente" data-ng-class="ctrl.Ricerca.class_categorie(ctrl.Ricerca.get_ammins_selected(), ctrl.Ricerca.get_ammins_chk())" data-ng-click="ctrl.Ricerca.ammins_toggleAll()"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-account_balance"></use></svg> <@wp.i18n key="CAGLIARI_PORTAL_AMMINISTRAZIONE" /></a>												<a href="" class="btn btn-default btn-trasparente" data-ng-class="ctrl.Ricerca.class_categorie(ctrl.Ricerca.get_servizi_selected(), ctrl.Ricerca.get_servizi_chk())" data-ng-click="ctrl.Ricerca.servizi_toggleAll()"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-settings"></use></svg> <@wp.i18n key="CAGLIARI_PORTAL_SERVIZI" /></a>												<a href="" class="btn btn-default btn-trasparente" data-ng-class="ctrl.Ricerca.class_categorie(ctrl.Ricerca.get_novita_selected(), ctrl.Ricerca.get_novita_chk())" data-ng-click="ctrl.Ricerca.novita_toggleAll()"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-event"></use></svg> <@wp.i18n key="CAGLIARI_PORTAL_NOVITA" /></a>												<a href="" class="btn btn-default btn-trasparente" data-ng-class="ctrl.Ricerca.class_categorie(ctrl.Ricerca.get_docs_selected(), ctrl.Ricerca.get_docs_chk())" data-ng-click="ctrl.Ricerca.docs_toggleAll()"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-description"></use></svg> <@wp.i18n key="CAGLIARI_PORTAL_DOCUMENTI" /></a>												<a href="" class="btn btn-default btn-trasparente" data-ng-click="ctrl.Ricerca.setActive(); ctrl.General.setFiltriMode(true, false)" aria-label="<@wp.i18n key="CAGLIARI_PORTAL_FILTER_BY" />&nbsp;<@wp.i18n key="CAGLIARI_PORTAL_CATEGORIES" />">...</a>											</div>											<div class="form-blockopz">												<h2><@wp.i18n key="CAGLIARI_CONTENT_ARGUMENTS" /></h2>												<a href="" data-ng-class="(ctrl.Ricerca.get_args_selected().length > 0 && ctrl.Ricerca.get_args_selected().length !== ctrl.Ricerca.get_argomenti().length)? ''badge badge-pill badge-argomenti'' : ''badge badge-pill badge-argomenti active''" data-ng-click="ctrl.Ricerca.toggleAll(''argomenti'')">													<@wp.i18n key="CAGLIARI_CONTENT_ALLARGUMENTS" />												</a>												<div data-ng-if="ctrl.Ricerca.get_args_selected().length !== ctrl.Ricerca.get_argomenti().length" data-ng-show="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_args_selected())" data-ng-repeat="item in ctrl.Ricerca.get_argomenti()" class="badge badge-pill badge-argomenti active">													<span data-ng-bind-html="item.name">-</span>													<a href="" data-ng-click="ctrl.Ricerca.unchk(item.code, ctrl.Ricerca.get_args_selected())" class="btn-badge-close">														<svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-clear"></use></svg>													</a>												</div>												<a href="" class="badge badge-pill badge-argomenti puntini" data-ng-click="ctrl.Ricerca.setActive(''argomenti''); ctrl.General.setFiltriMode(true, false)" aria-label="<@wp.i18n key="CAGLIARI_PORTAL_FILTER_BY" />&nbsp;<@wp.i18n key="CAGLIARI_CONTENT_ARGUMENTS" />">...</a>											</div>											<div class="form-blockopz">												<h2><@wp.i18n key="CAGLIARI_CONTENT_OPTIONS" /></h2>												<a href="" data-ng-class="(ctrl.Ricerca.activeChk || ctrl.Ricerca.dataInizio || ctrl.Ricerca.dataFine)? ''badge badge-pill badge-argomenti'' : ''badge badge-pill badge-argomenti active''" data-ng-click="ctrl.Ricerca.activeChk = false; ctrl.Ricerca.clean_date()">													<@wp.i18n key="CAGLIARI_CONTENT_ALLOPTIONS" />												</a>												<div data-ng-if="ctrl.Ricerca.activeChk" class="badge badge-pill badge-argomenti active">													<@wp.i18n key="CAGLIARI_CONTENT_ACTIVE_CONTENT" />&nbsp;<a href="" data-ng-click="ctrl.Ricerca.activeChk = false" class="btn-badge-close"><svg class="icon">													<use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-clear"></use></svg></a>												</div>												<div data-ng-if="ctrl.Ricerca.dataInizio && ctrl.Ricerca.dataFine" class="badge badge-pill badge-argomenti active">													<@wp.i18n key="CAGLIARI_PORTAL_FROM" />&nbsp;{{ ctrl.Ricerca.dataInizio }}&nbsp;<@wp.i18n key="CAGLIARI_PORTAL_TO" />&nbsp;{{ ctrl.Ricerca.dataFine }} <a href="" data-ng-click="ctrl.Ricerca.clean_date()" class="btn-badge-close"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-clear"></use></svg></a>												</div>												<div data-ng-if="ctrl.Ricerca.dataInizio && !ctrl.Ricerca.dataFine" class="badge badge-pill badge-argomenti active">													<@wp.i18n key="CAGLIARI_PORTAL_FROM" />&nbsp;{{ ctrl.Ricerca.dataInizio }}&nbsp;<a href="" data-ng-click="ctrl.Ricerca.clean_date()" class="btn-badge-close"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-clear"></use></svg></a>												</div>												<div data-ng-if="!ctrl.Ricerca.dataInizio && ctrl.Ricerca.dataFine" class="badge badge-pill badge-argomenti active">													<@wp.i18n key="CAGLIARI_PORTAL_UNTIL" />&nbsp;{{ ctrl.Ricerca.dataFine }} <a href="" data-ng-click="ctrl.Ricerca.clean_date()" class="btn-badge-close"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-clear"></use></svg></a>												</div>												<a href="" class="badge badge-pill badge-argomenti puntini" data-ng-click="ctrl.Ricerca.setActive(''opzioni''); ctrl.General.setFiltriMode(true, false)" aria-label="<@wp.i18n key="SEARCH_FILTERS_BUTTON" />">...</a>											</div>											<button id="search-button" class="search-start" >												<svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-search"></use></svg>												<@wp.i18n key="CAGLIARI_PORTAL_SEARCH_THROUGHOUT_SITE" />											</button>										</div>										<input type="hidden" id="amministrazione" name="amministrazione" value="{{ctrl.Ricerca.get_ammins_selected()}}">										<input type="hidden" id="servizi" name="servizi" value="{{ctrl.Ricerca.get_servizi_selected()}}">										<input type="hidden" id="novita" name="novita" value="{{ctrl.Ricerca.get_novita_selected()}}">										<input type="hidden" id="documenti" name="documenti" value="{{ctrl.Ricerca.get_docs_selected()}}">										<input type="hidden" id="argomenti" name="argomenti" value="{{ctrl.Ricerca.get_args_selected()}}">										<input type="hidden" id="attivi" name="attivi" value="{{ctrl.Ricerca.activeChk}}">										<input type="hidden" id="inizio" name="inizio" value="{{ctrl.Ricerca.dataInizio}}">										<input type="hidden" id="fine" name="fine" value="{{ctrl.Ricerca.dataFine}}">									</div>								</div>																<#list systemCategories?sort_by("value") as systemCategory>									<data-ng-container data-ng-init="ctrl.Ricerca.setArgomenti(''${systemCategory.value?replace("''", "\\''")}'', ''${systemCategory.key}'')"></data-ng-container>								</#list>								<@wp.nav var="pagina" spec="code(homepage).subtree(4)">									<#if (pagina.level == 1)>										<#assign padre = pagina.code />									</#if>									<#if (pagina.level == 2)>											<#assign padre2 = pagina.code />										<#assign categoria = pagina.title />										<#assign categoriaCode = pagina.code />										<data-ng-container data-ng-init="ctrl.Ricerca.setCategorie(''${categoria?replace("''", "\\''")}'', ''${categoriaCode}'', ''${padre}'')"></data-ng-container>									</#if>									<#if (pagina.level > 2)>										<#assign categoria = pagina.title />										<#assign categoriaCode = pagina.code />										<data-ng-container data-ng-init="ctrl.Ricerca.setCategorie(''${categoria?replace("''", "\\''")}'', ''${categoriaCode}'', ''${padre}'', ''${padre2}'')"></data-ng-container>									</#if>								</@wp.nav>																<#if (code == ''amministrazione'')><data-ng-container data-ng-init="ctrl.Ricerca.ammins_toggleAll()"></data-ng-container></#if>								<#if (code == ''servizi'')><data-ng-container data-ng-init="ctrl.Ricerca.servizi_toggleAll()"></data-ng-container></#if>								<#if (code == ''novita'')><data-ng-container data-ng-init="ctrl.Ricerca.novita_toggleAll()"></data-ng-container></#if>								<#if (code == ''documenti'')><data-ng-container data-ng-init="ctrl.Ricerca.docs_toggleAll()"></data-ng-container></#if>																<div role="tabpanel" data-ng-show="ctrl.General.getFiltriMode().filtri" class="ng-hide" data-ng-init="showallcat = false; showallarg = false">									<div class="tab-content">										<div role="tabpanel" data-ng-class="ctrl.Ricerca.get_categoriaTab() == ''active''? ''tab-pane active'' : ''tab-pane''" id="categoriaTab" data-ng-init="ctrl.Ricerca.chkCategoryCode(''${code}'')">											<div class="row">												<div class="offset-md-1 col-md-10 col-sm-12">													<div class="search-filter-ckgroup">														<md-checkbox aria-label="<@wp.i18n key="CAGLIARI_PORTAL_AMMINISTRAZIONE" />"															data-ng-if="ctrl.General.getFiltriMode().categoria == ''''"															data-ng-checked="ctrl.Ricerca.ammins_isChecked()"															md-indeterminate="ctrl.Ricerca.ammins_isIndeterminate()"															data-ng-click="ctrl.Ricerca.ammins_toggleAll()"><label class="search-filter-ckgroup-title"><@wp.i18n key="CAGLIARI_PORTAL_AMMINISTRAZIONE" /></label>														</md-checkbox>														<div class="flex-100" data-ng-repeat="item in ctrl.Ricerca.get_ammins_chk()"															data-ng-if="ctrl.General.getFiltriMode().categoria == ''Amministrazione'' || ctrl.General.getFiltriMode().categoria == ''''">															<md-checkbox data-ng-checked="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_ammins_selected())" data-ng-click="ctrl.Ricerca.toggle(item.code, ctrl.Ricerca.get_ammins_selected())" 	aria-label="{{item.name}}">																<label data-ng-bind-html="item.name"></label>															</md-checkbox>														</div>													</div>													<div class="search-filter-ckgroup">														<md-checkbox aria-label="<@wp.i18n key="CAGLIARI_PORTAL_SERVIZI" />"															data-ng-if="ctrl.General.getFiltriMode().categoria == ''''"															data-ng-checked="ctrl.Ricerca.servizi_isChecked()"															md-indeterminate="ctrl.Ricerca.servizi_isIndeterminate()"															data-ng-click="ctrl.Ricerca.servizi_toggleAll()"><label class="search-filter-ckgroup-title"><@wp.i18n key="CAGLIARI_PORTAL_SERVIZI" /></label>														</md-checkbox>														<div class="flex-100" data-ng-repeat="item in ctrl.Ricerca.get_servizi_chk()"															data-ng-if="ctrl.General.getFiltriMode().categoria == ''Servizi'' || ctrl.General.getFiltriMode().categoria == ''''">															<div data-ng-if = "$index <= 2">																<md-checkbox data-ng-checked="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_servizi_selected())" data-ng-click="ctrl.Ricerca.toggle(item.code, ctrl.Ricerca.get_servizi_selected())" aria-label="{{item.name}}">																<label data-ng-bind-html="item.name"></label>																</md-checkbox>															</div>															<div data-ng-if = "$index >= 3">																									<div data-ng-show="showallcat || ctrl.General.getFiltriMode().categoria != ''''">																	<md-checkbox data-ng-checked="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_servizi_selected())" data-ng-click="ctrl.Ricerca.toggle(item.code, ctrl.Ricerca.get_servizi_selected())" aria-label="{{item.name}}">																	<label data-ng-bind-html="item.name"></label>																	</md-checkbox>																</div>															</div>														</div>														<a href="" data-ng-hide="showallcat || ctrl.General.getFiltriMode().categoria != ''''" data-ng-click="showallcat=true"><strong><@wp.i18n key="CAGLIARI_PORTAL_SHOW_ALL" /></strong></a>													</div>													<div data-ng-show="showallcat || ctrl.General.getFiltriMode().categoria != ''''">														<div class="search-filter-ckgroup">															<md-checkbox aria-label="<@wp.i18n key="CAGLIARI_PORTAL_NOVITA" />"																data-ng-if="ctrl.General.getFiltriMode().categoria == ''''"																data-ng-checked="ctrl.Ricerca.novita_isChecked()"																md-indeterminate="ctrl.Ricerca.novita_isIndeterminate()"																data-ng-click="ctrl.Ricerca.novita_toggleAll()"><label class="search-filter-ckgroup-title"><@wp.i18n key="CAGLIARI_PORTAL_NOVITA" /></label>															</md-checkbox>															<div class="flex-100" data-ng-repeat="item in ctrl.Ricerca.get_novita_chk()"																data-ng-if="ctrl.General.getFiltriMode().categoria == ''NovitÃƒ '' || ctrl.General.getFiltriMode().categoria == ''''">																<md-checkbox data-ng-checked="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_novita_selected())" data-ng-click="ctrl.Ricerca.toggle(item.code, ctrl.Ricerca.get_novita_selected())" aria-label="{{item.name}}">																<label data-ng-bind-html="item.name"></label>																</md-checkbox>															</div>														</div>														<div class="search-filter-ckgroup">															<md-checkbox aria-label="<@wp.i18n key="CAGLIARI_PORTAL_DOCUMENTI" />"																data-ng-if="ctrl.General.getFiltriMode().categoria == ''''"																data-ng-checked="ctrl.Ricerca.docs_isChecked()"																md-indeterminate="ctrl.Ricerca.docs_isIndeterminate()"																data-ng-click="ctrl.Ricerca.docs_toggleAll()"><label class="search-filter-ckgroup-title"><@wp.i18n key="CAGLIARI_PORTAL_DOCUMENTI" /></label>															</md-checkbox>															<div class="flex-100" data-ng-repeat="item in ctrl.Ricerca.get_docs_chk()"																data-ng-if="ctrl.General.getFiltriMode().categoria == ''Documenti'' || ctrl.General.getFiltriMode().categoria == ''''">																<md-checkbox data-ng-checked="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_docs_selected())" data-ng-click="ctrl.Ricerca.toggle(item.code, ctrl.Ricerca.get_docs_selected())" aria-label="{{item.name}}">																<label data-ng-bind-html="item.name"></label>																</md-checkbox>															</div>															<a href="" data-ng-hide="ctrl.General.getFiltriMode().categoria != ''''" data-ng-click="showallcat=false"><strong><@wp.i18n key="CAGLIARI_PORTAL_SHOW_LESS" /></strong></a>														</div>													</div>												</div>											</div>										</div>										<div role="tabpanel" data-ng-class="ctrl.Ricerca.get_argomentoTab() == ''active''? ''tab-pane active'' : ''tab-pane''" id="argomentoTab">											<div class="row">												<div class="offset-md-1 col-md-10 col-sm-12">													<div class="search-filter-ckgroup">														<div class="flex-100" data-ng-repeat="item in ctrl.Ricerca.get_argomenti()">															<div data-ng-if = "$index <= 11">																<md-checkbox data-ng-checked="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_args_selected())" data-ng-click="ctrl.Ricerca.toggle(item.code, ctrl.Ricerca.get_args_selected()); ctrl.Ricerca.getContents()" aria-label="{{item.name}}">																<label data-ng-bind-html="item.name"></label></md-checkbox>															</div>															<div data-ng-if = "$index >= 12">																									<div data-ng-show="showallarg">																	<md-checkbox data-ng-checked="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_args_selected())" data-ng-click="ctrl.Ricerca.toggle(item.code, ctrl.Ricerca.get_args_selected()); ctrl.Ricerca.getContents()" aria-label="{{item.name}}">																	<label data-ng-bind-html="item.name"></label></md-checkbox>																</div>															</div>														</div>														<a href="" data-ng-hide="showallarg" data-ng-click="showallarg=true"><strong><@wp.i18n key="CAGLIARI_PORTAL_SHOW_ALL" /></strong></a>														<a href="" data-ng-show="showallarg" data-ng-click="showallarg=false"><strong><@wp.i18n key="CAGLIARI_PORTAL_SHOW_LESS" /></strong></a>													</div>												</div>											</div>										</div>										<div role="tabpanel" data-ng-class="ctrl.Ricerca.get_opzioniTab() == ''active''? ''tab-pane active'' : ''tab-pane''" id="opzioniTab">											<div class="row">												<div class="offset-lg-1 col-lg-4 col-md-6 col-sm-12">													<div class="form-check form-check-group">														<div class="toggles">															<label for="active_chk">																<@wp.i18n key="CAGLIARI_PORTAL_SEARCH_ONLY_ACTIVE_CONTENT" />																<input type="checkbox" id="active_chk" data-ng-model="ctrl.Ricerca.activeChk">																<span class="lever"></span>															</label>														</div>													</div>													<p class="small"><@wp.i18n key="CAGLIARI_PORTAL_SEARCH_ONLY_ACTIVE_CONTENT_INFO" /></p>												</div>												<div class="col-md-3 col-sm-12 search-filter-dategroup">													<div class="form-group">														<label for="datepicker_start"><@wp.i18n key="CAGLIARI_PORTAL_START_DATE" /></label>														<input type="text" class="form-control" id="datepicker_start" data-ng-model="ctrl.Ricerca.dataInizio" placeholder="gg/mm/aaaa" />														<button aria-label="<@wp.i18n key="CAGLIARI_PORTAL_OPEN_CALENDAR" />" type="button" class="ico-sufix" onclick="$(''#datepicker_start'').focus();" onkeypress="$(''#datepicker_start'').focus();"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-event"></use></svg></button>													</div>												</div>												<div class="col-md-3 col-sm-12 search-filter-dategroup">													<div class="form-group">														<label for="datepicker_end"><@wp.i18n key="CAGLIARI_PORTAL_END_DATE" /></label>														<input type="text" class="form-control" id="datepicker_end" data-ng-model="ctrl.Ricerca.dataFine" placeholder="gg/mm/aaaa" />														<button aria-label="<@wp.i18n key="CAGLIARI_PORTAL_OPEN_CALENDAR" />" type="button" class="ico-sufix" onclick="$(''#datepicker_end'').focus();" onkeypress="$(''#datepicker_end'').focus();"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-event"></use></svg></button>													</div>												</div>											</div>										</div>									</div>								</div>							</div>						</div>					</form>				</div>			</div>		</div>	</div></div>','<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]><#assign wp=JspTaglibs["/aps-core"]><#assign cw=JspTaglibs["/comuneCagliari-whitelabel"]><@wp.categories var="systemCategories" root="arg_argomenti" /><@wp.currentPage param="code" var="code" /><@wp.pageWithWidget widgetTypeCode="cagliari_widget_advsearch_results" var="searchWidgetPage" /><@cw.configWidget pageCode="${searchWidgetPage.code}" widgetCode="cagliari_widget_advsearch_results" configParam="contentTypesFilter" var="tipiContenuto" /><#if (!tipiContenuto??)><#assign tipiContenuto = ""/></#if><div id="dvRicerca">	<div data-ng-cloak data-ng-controller="ctrlRicerca as ctrl">		<!-- CERCA -->		<div class="modal fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="searchModalTitle" aria-hidden="false">			<script>				$(function() {				  $("#datepicker_start").datepicker();				  $("#datepicker_end").datepicker();				});			</script>			<div class="modal-dialog" role="document">				<div class="modal-content">					<form id="ricerca" action="<@wp.url page="${searchWidgetPage.code}"/>" method="post">						<div class="modal-header-fullsrc">							<div class="container">								<div class="row">									<div class="col-sm-1" data-ng-class="{pb12: !ctrl.General.getFiltriMode().filtri}">										<button data-ng-hide="ctrl.General.getFiltriMode().filtri && !ctrl.General.getFiltriMode().internal" type="button" class="close" data-dismiss="modal" aria-label="<@wp.i18n key="CAGLIARI_PORTAL_CLOSE_SEARCH_FILTERS" />" data-ng-click="ctrl.General.setFiltriMode(false, false)">											<svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-arrow_back"></use></svg>										</button>										<button data-ng-show="ctrl.General.getFiltriMode().filtri && !ctrl.General.getFiltriMode().internal" type="button" class="close" aria-label="<@wp.i18n key="CAGLIARI_PORTAL_CLOSE_SEARCH_FILTERS" />" data-ng-click="ctrl.General.setFiltriMode(false, false)">											<svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-arrow_back"></use></svg>										</button>									</div>									<div class="col-sm-11">										<h1 class="modal-title" id="searchModalTitle">											<span data-ng-hide="ctrl.General.getFiltriMode().filtri"><@wp.i18n key="CAGLIARI_HEADER_SEARCH_TITLE" /></span>											<span data-ng-show="ctrl.General.getFiltriMode().filtri"><@wp.i18n key="CAGLIARI_HEADER_FILTER_TITLE" /></span>										</h1>										<button data-ng-show="ctrl.General.getFiltriMode().filtri" class="confirm btn btn-default btn-trasparente float-right"><@wp.i18n key="CAGLIARI_PORTAL_CONFIRM" /></button>									</div>								</div>							</div>							<div class="search-filter-type ng-hide" data-ng-show="ctrl.General.getFiltriMode().filtri">								<ul class="nav nav-tabs" role="tablist">									<li role="presentation">										<a data-ng-if="ctrl.General.getFiltriMode().categoria == ''''" href="" aria-controls="categoriaTab" role="tab" data-toggle="tab" data-ng-click="ctrl.Ricerca.setActive(''categorie'')" data-ng-class="ctrl.Ricerca.get_categoriaTab()"><@wp.i18n key="CAGLIARI_PORTAL_CATEGORIES" /></a>										<a data-ng-if="ctrl.General.getFiltriMode().categoria != ''''" href="" aria-controls="categoriaTab" role="tab" data-toggle="tab" data-ng-click="ctrl.Ricerca.setActive(''categorie'')" data-ng-class="ctrl.Ricerca.get_categoriaTab()">{{ctrl.General.getFiltriMode().categoria}}</a>									</li>									<li role="presentation"><a href="" aria-controls="argomentoTab" role="tab" data-toggle="tab"										data-ng-click="ctrl.Ricerca.setActive(''argomenti'')" data-ng-class="ctrl.Ricerca.get_argomentoTab()"><@wp.i18n key="CAGLIARI_CONTENT_ARGUMENTS" /></a></li>									<li role="presentation"><a href="" aria-controls="opzioniTab" role="tab" data-toggle="tab"										data-ng-click="ctrl.Ricerca.setActive(''opzioni'')" data-ng-class="ctrl.Ricerca.get_opzioniTab()"><@wp.i18n key="CAGLIARI_CONTENT_OPTIONS" /></a></li>								</ul>							</div>						</div>						<div class="modal-body-search">							<div class="container">								<div class="row" data-ng-hide="ctrl.General.getFiltriMode().filtri">									<div class="col-lg-12 col-md-12 col-sm-12">										<div class="form-group" data-ng-init="ctrl.Ricerca.setContentTypes(''${tipiContenuto}'')">											<label class="sr-only active"><@wp.i18n key="CAGLIARI_HEADER_SEARCH_TITLE" /></label>											<md-autocomplete type="text" md-input-name ="cercatxt" md-input-id ="cerca-txt"	placeholder="<@wp.i18n key="CAGLIARI_PORTAL_SEARCH_INFORMATION_PEOPLE_SERVICES" />" md-selected-item="ctrl.Ricerca.cercatxt"												md-search-text="ctrl.Ricerca.searchStringRicercaTxt" md-selected-item-change="ctrl.Ricerca.selectedRicercaTxtItemChanged(item)" md-items="item in ctrl.Ricerca.getAutocompleteResults(ctrl.Ricerca.searchStringRicercaTxt)"												md-item-text="item.contentName"	md-min-length="1" md-clear-button="true">												<md-item-template>													<div>														<a href="{{item.href}}" data-ng-if="!item.searchButton && item.searchButton != ''hidden''">															<div>																<svg class="icon icon-sm" data-ng-bind-html="item.icon"></svg>																<span class="autocomplete-list-text">																	<span data-md-highlight-text="ctrl.Ricerca.searchStringRicercaTxt" data-md-highlight-flags="i">{{item.contentName}}</span>																	<em>{{item.category}}</em>																</span>															</div>														</a>														<div class="search-start" data-ng-if="item.searchButton && item.searchButton != ''hidden''" data-ng-click="item.contentName = ctrl.Ricerca.searchStringRicercaTxt" onclick="document.getElementById(''search-button'').click();">															<svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-search"></use></svg>															<@wp.i18n key="CAGLIARI_PORTAL_SEARCH_THROUGHOUT_SITE" />														</div>														<div class="search-start-hidden" data-ng-if="item.searchButton == ''hidden''"> </div>													</div>												</md-item-template>											</md-autocomplete>											<svg class="icon ico-prefix"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-search"></use></svg>										</div>										<div data-ng-hide="ctrl.Ricerca.getAutocompleteIsOpened()">											<div class="form-filtro">												<a class="btn btn-default btn-trasparente" data-ng-class="ctrl.Ricerca.class_tutti(''categorie'')" data-ng-click="ctrl.Ricerca.toggleAll(''categorie'')"><@wp.i18n key="CAGLIARI_PORTAL_ALL" /></a>												<a class="btn btn-default btn-trasparente" data-ng-class="ctrl.Ricerca.class_categorie(ctrl.Ricerca.get_ammins_selected(), ctrl.Ricerca.get_ammins_chk())" data-ng-click="ctrl.Ricerca.ammins_toggleAll()"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-account_balance"></use></svg> <@wp.i18n key="CAGLIARI_PORTAL_AMMINISTRAZIONE" /></a>												<a class="btn btn-default btn-trasparente" data-ng-class="ctrl.Ricerca.class_categorie(ctrl.Ricerca.get_servizi_selected(), ctrl.Ricerca.get_servizi_chk())" data-ng-click="ctrl.Ricerca.servizi_toggleAll()"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-settings"></use></svg> <@wp.i18n key="CAGLIARI_PORTAL_SERVIZI" /></a>												<a class="btn btn-default btn-trasparente" data-ng-class="ctrl.Ricerca.class_categorie(ctrl.Ricerca.get_novita_selected(), ctrl.Ricerca.get_novita_chk())" data-ng-click="ctrl.Ricerca.novita_toggleAll()"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-event"></use></svg> <@wp.i18n key="CAGLIARI_PORTAL_NOVITA" /></a>												<a class="btn btn-default btn-trasparente" data-ng-class="ctrl.Ricerca.class_categorie(ctrl.Ricerca.get_docs_selected(), ctrl.Ricerca.get_docs_chk())" data-ng-click="ctrl.Ricerca.docs_toggleAll()"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-description"></use></svg> <@wp.i18n key="CAGLIARI_PORTAL_DOCUMENTI" /></a>												<a class="btn btn-default btn-trasparente" data-ng-click="ctrl.Ricerca.setActive(); ctrl.General.setFiltriMode(true, false)" aria-label="<@wp.i18n key="CAGLIARI_PORTAL_FILTER_BY" />&nbsp;<@wp.i18n key="CAGLIARI_PORTAL_CATEGORIES" />">...</a>											</div>											<div class="form-blockopz">												<h2><@wp.i18n key="CAGLIARI_CONTENT_ARGUMENTS" /></h2>												<a href="" data-ng-class="(ctrl.Ricerca.get_args_selected().length > 0 && ctrl.Ricerca.get_args_selected().length !== ctrl.Ricerca.get_argomenti().length)? ''badge badge-pill badge-argomenti'' : ''badge badge-pill badge-argomenti active''" data-ng-click="ctrl.Ricerca.toggleAll(''argomenti'')">													<@wp.i18n key="CAGLIARI_CONTENT_ALLARGUMENTS" />												</a>												<div data-ng-if="ctrl.Ricerca.get_args_selected().length !== ctrl.Ricerca.get_argomenti().length" data-ng-show="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_args_selected())" data-ng-repeat="item in ctrl.Ricerca.get_argomenti()" class="badge badge-pill badge-argomenti active">													<span data-ng-bind-html="item.name">-</span>													<a data-ng-click="ctrl.Ricerca.unchk(item.code, ctrl.Ricerca.get_args_selected())" class="btn-badge-close">														<svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-clear"></use></svg>													</a>												</div>												<a href="" class="badge badge-pill badge-argomenti puntini" data-ng-click="ctrl.Ricerca.setActive(''argomenti''); ctrl.General.setFiltriMode(true, false)" aria-label="<@wp.i18n key="CAGLIARI_PORTAL_FILTER_BY" />&nbsp;<@wp.i18n key="CAGLIARI_CONTENT_ARGUMENTS" />">...</a>											</div>											<div class="form-blockopz">												<h2><@wp.i18n key="CAGLIARI_CONTENT_OPTIONS" /></h2>												<a href="" data-ng-class="(ctrl.Ricerca.activeChk || ctrl.Ricerca.dataInizio || ctrl.Ricerca.dataFine)? ''badge badge-pill badge-argomenti'' : ''badge badge-pill badge-argomenti active''" data-ng-click="ctrl.Ricerca.activeChk = false; ctrl.Ricerca.clean_date()">													<@wp.i18n key="CAGLIARI_CONTENT_ALLOPTIONS" />												</a>												<div data-ng-if="ctrl.Ricerca.activeChk" class="badge badge-pill badge-argomenti active">													<@wp.i18n key="CAGLIARI_CONTENT_ACTIVE_CONTENT" />&nbsp;<a data-ng-click="ctrl.Ricerca.activeChk = false" class="btn-badge-close"><svg class="icon">													<use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-clear"></use></svg></a>												</div>												<div data-ng-if="ctrl.Ricerca.dataInizio && ctrl.Ricerca.dataFine" class="badge badge-pill badge-argomenti active">													<@wp.i18n key="CAGLIARI_PORTAL_FROM" />&nbsp;{{ ctrl.Ricerca.dataInizio }}&nbsp;<@wp.i18n key="CAGLIARI_PORTAL_TO" />&nbsp;{{ ctrl.Ricerca.dataFine }} <a data-ng-click="ctrl.Ricerca.clean_date()" class="btn-badge-close"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-clear"></use></svg></a>												</div>												<div data-ng-if="ctrl.Ricerca.dataInizio && !ctrl.Ricerca.dataFine" class="badge badge-pill badge-argomenti active">													<@wp.i18n key="CAGLIARI_PORTAL_FROM" />&nbsp;{{ ctrl.Ricerca.dataInizio }}&nbsp;<a data-ng-click="ctrl.Ricerca.clean_date()" class="btn-badge-close"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-clear"></use></svg></a>												</div>												<div data-ng-if="!ctrl.Ricerca.dataInizio && ctrl.Ricerca.dataFine" class="badge badge-pill badge-argomenti active">													<@wp.i18n key="CAGLIARI_PORTAL_UNTIL" />&nbsp;{{ ctrl.Ricerca.dataFine }} <a data-ng-click="ctrl.Ricerca.clean_date()" class="btn-badge-close"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-clear"></use></svg></a>												</div>												<a href="" class="badge badge-pill badge-argomenti puntini" data-ng-click="ctrl.Ricerca.setActive(''opzioni''); ctrl.General.setFiltriMode(true, false)" aria-label="<@wp.i18n key="SEARCH_FILTERS_BUTTON" />">...</a>											</div>											<div class="form-blockopz">												<div data-ng-if="getCookie(''search'') == ''''">													<h2><@wp.i18n key="CAGLIARI_PORTAL_TIPS" /></h2>													<ul>														<li><a href="#" title="Vai alla pagina: Uffici comunali"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-account_balance"></use></svg> Uffici comunali</a></li>														<li><a href="#" title="Vai alla pagina: Servizi demografici"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-settings"></use></svg> Servizi demografici</a></li>														<li><a href="#" title="Vai alla pagina: SUAP"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-account_balance"></use></svg> SUAP</a></li>														<li><a href="#" title="Vai alla pagina: Sostegno alle famiglie"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-settings"></use></svg> Sostegno alle famiglie</a></li>														<li><a href="#" title="Vai alla pagina: Segnalazioni"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-settings"></use></svg> Segnalazioni</a></li>													</ul>												</div>												<div data-ng-if="getCookie(''search'')">													<h2><@wp.i18n key="CAGLIARI_PORTAL_POPULAR_SEARCHES" /></h2>													<ul>														<li><a href="#" title="Vai alla pagina: SUAPE"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-settings"></use></svg> SUAPE </a></li>														<li><a href="#" title="Vai alla pagina: Pagamento tassa di cicolazione"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-settings"></use></svg> Pagamento tassa di cicolazione </a></li>														<li><a href="#" title="Vai alla pagina: Modulo iscrizione Asilo nido"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-description"></use></svg> Modulo iscrizione Asilo nido </a></li>														<li><a href="#" title="Vai alla pagina: Municipi di Cagliari"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-account_balance"></use></svg> Municipi di Cagliari </a></li>														<li><a href="#" title="Vai alla pagina: Pagamento Imposte comunali"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-settings"></use></svg> Albo pretorio </a></li>														<li><a href="#" title="Vai alla pagina: Albo pretorio"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-description"></use></svg> Albo pretorio </a></li>													</ul>												</div>											</div>											<button id="search-button" class="search-start" >												<svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-search"></use></svg>												<@wp.i18n key="CAGLIARI_PORTAL_SEARCH_THROUGHOUT_SITE" />											</button>										</div>										<input type="hidden" id="amministrazione" name="amministrazione" value="{{ctrl.Ricerca.get_ammins_selected()}}">										<input type="hidden" id="servizi" name="servizi" value="{{ctrl.Ricerca.get_servizi_selected()}}">										<input type="hidden" id="novita" name="novita" value="{{ctrl.Ricerca.get_novita_selected()}}">										<input type="hidden" id="documenti" name="documenti" value="{{ctrl.Ricerca.get_docs_selected()}}">										<input type="hidden" id="argomenti" name="argomenti" value="{{ctrl.Ricerca.get_args_selected()}}">										<input type="hidden" id="attivi" name="attivi" value="{{ctrl.Ricerca.activeChk}}">										<input type="hidden" id="inizio" name="inizio" value="{{ctrl.Ricerca.dataInizio}}">										<input type="hidden" id="fine" name="fine" value="{{ctrl.Ricerca.dataFine}}">									</div>								</div>																<#list systemCategories?sort_by("value") as systemCategory>									<data-ng-container data-ng-init="ctrl.Ricerca.setArgomenti(''${systemCategory.value?replace("''", "\\''")}'', ''${systemCategory.key}'')"></data-ng-container>								</#list>								<@wp.nav var="pagina" spec="code(homepage).subtree(2)">									<#if (pagina.level == 1)>										<#assign padre = pagina.code />									</#if>									<#if (pagina.level == 2)>											<#assign categoria = pagina.title />										<#assign categoriaCode = pagina.code />										<data-ng-container data-ng-init="ctrl.Ricerca.setCategorie(''${categoria}'', ''${categoriaCode}'', ''${padre}'')"></data-ng-container>									</#if>								</@wp.nav>																<div role="tabpanel" data-ng-show="ctrl.General.getFiltriMode().filtri" class="ng-hide" data-ng-init="showallcat = false; showallarg = false">									<div class="tab-content">										<div role="tabpanel" data-ng-class="ctrl.Ricerca.get_categoriaTab() == ''active''? ''tab-pane active'' : ''tab-pane''" id="categoriaTab" data-ng-init="ctrl.Ricerca.chkCategoryCode(''${code}'')">											<div class="row">												<div class="offset-md-1 col-md-10 col-sm-12">													<div class="search-filter-ckgroup">														<md-checkbox aria-label="<@wp.i18n key="CAGLIARI_PORTAL_AMMINISTRAZIONE" />"															data-ng-if="ctrl.General.getFiltriMode().categoria == ''''"															data-ng-checked="ctrl.Ricerca.ammins_isChecked()"															md-indeterminate="ctrl.Ricerca.ammins_isIndeterminate()"															data-ng-click="ctrl.Ricerca.ammins_toggleAll()"><label class="search-filter-ckgroup-title"><@wp.i18n key="CAGLIARI_PORTAL_AMMINISTRAZIONE" /></label>														</md-checkbox>														<div class="flex-100" data-ng-repeat="item in ctrl.Ricerca.get_ammins_chk()"															data-ng-if="ctrl.General.getFiltriMode().categoria == ''Amministrazione'' || ctrl.General.getFiltriMode().categoria == ''''">															<md-checkbox data-ng-checked="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_ammins_selected())" data-ng-click="ctrl.Ricerca.toggle(item.code, ctrl.Ricerca.get_ammins_selected())" 	aria-label="{{item.name}}">																<label data-ng-bind-html="item.name"></label>															</md-checkbox>														</div>													</div>													<div class="search-filter-ckgroup">														<md-checkbox aria-label="<@wp.i18n key="CAGLIARI_PORTAL_SERVIZI" />"															data-ng-if="ctrl.General.getFiltriMode().categoria == ''''"															data-ng-checked="ctrl.Ricerca.servizi_isChecked()"															md-indeterminate="ctrl.Ricerca.servizi_isIndeterminate()"															data-ng-click="ctrl.Ricerca.servizi_toggleAll()"><label class="search-filter-ckgroup-title"><@wp.i18n key="CAGLIARI_PORTAL_SERVIZI" /></label>														</md-checkbox>														<div class="flex-100" data-ng-repeat="item in ctrl.Ricerca.get_servizi_chk()"															data-ng-if="ctrl.General.getFiltriMode().categoria == ''Servizi'' || ctrl.General.getFiltriMode().categoria == ''''">															<div data-ng-if = "$index <= 2">																<md-checkbox data-ng-checked="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_servizi_selected())" data-ng-click="ctrl.Ricerca.toggle(item.code, ctrl.Ricerca.get_servizi_selected())" aria-label="{{item.name}}">																<label data-ng-bind-html="item.name"></label>																</md-checkbox>															</div>															<div data-ng-if = "$index >= 3">																									<div data-ng-show="showallcat || ctrl.General.getFiltriMode().categoria != ''''">																	<md-checkbox data-ng-checked="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_servizi_selected())" data-ng-click="ctrl.Ricerca.toggle(item.code, ctrl.Ricerca.get_servizi_selected())" aria-label="{{item.name}}">																	<label data-ng-bind-html="item.name"></label>																	</md-checkbox>																</div>															</div>														</div>														<a href="" data-ng-hide="showallcat || ctrl.General.getFiltriMode().categoria != ''''" data-ng-click="showallcat=true"><b><@wp.i18n key="CAGLIARI_PORTAL_SHOW_ALL" /></b></a>													</div>													<div data-ng-show="showallcat || ctrl.General.getFiltriMode().categoria != ''''">														<div class="search-filter-ckgroup">															<md-checkbox aria-label="<@wp.i18n key="CAGLIARI_PORTAL_NOVITA" />"																data-ng-if="ctrl.General.getFiltriMode().categoria == ''''"																data-ng-checked="ctrl.Ricerca.novita_isChecked()"																md-indeterminate="ctrl.Ricerca.novita_isIndeterminate()"																data-ng-click="ctrl.Ricerca.novita_toggleAll()"><label class="search-filter-ckgroup-title"><@wp.i18n key="CAGLIARI_PORTAL_NOVITA" /></label>															</md-checkbox>															<div class="flex-100" data-ng-repeat="item in ctrl.Ricerca.get_novita_chk()"																data-ng-if="ctrl.General.getFiltriMode().categoria == ''NovitÃƒ '' || ctrl.General.getFiltriMode().categoria == ''''">																<md-checkbox data-ng-checked="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_novita_selected())" data-ng-click="ctrl.Ricerca.toggle(item.code, ctrl.Ricerca.get_novita_selected())" aria-label="{{item.name}}">																<label data-ng-bind-html="item.name"></label>																</md-checkbox>															</div>														</div>														<div class="search-filter-ckgroup">															<md-checkbox aria-label="<@wp.i18n key="CAGLIARI_PORTAL_DOCUMENTI" />"																data-ng-if="ctrl.General.getFiltriMode().categoria == ''''"																data-ng-checked="ctrl.Ricerca.docs_isChecked()"																md-indeterminate="ctrl.Ricerca.docs_isIndeterminate()"																data-ng-click="ctrl.Ricerca.docs_toggleAll()"><label class="search-filter-ckgroup-title"><@wp.i18n key="CAGLIARI_PORTAL_DOCUMENTI" /></label>															</md-checkbox>															<div class="flex-100" data-ng-repeat="item in ctrl.Ricerca.get_docs_chk()"																data-ng-if="ctrl.General.getFiltriMode().categoria == ''Documenti'' || ctrl.General.getFiltriMode().categoria == ''''">																<md-checkbox data-ng-checked="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_docs_selected())" data-ng-click="ctrl.Ricerca.toggle(item.code, ctrl.Ricerca.get_docs_selected())" aria-label="{{item.name}}">																<label data-ng-bind-html="item.name"></label>																</md-checkbox>															</div>															<a href="" data-ng-hide="ctrl.General.getFiltriMode().categoria != ''''" data-ng-click="showallcat=false"><b><@wp.i18n key="CAGLIARI_PORTAL_SHOW_LESS" /></b></a>														</div>													</div>												</div>											</div>										</div>										<div role="tabpanel" data-ng-class="ctrl.Ricerca.get_argomentoTab() == ''active''? ''tab-pane active'' : ''tab-pane''" id="argomentoTab">											<div class="row">												<div class="offset-md-1 col-md-10 col-sm-12">													<div class="search-filter-ckgroup">														<div class="flex-100" data-ng-repeat="item in ctrl.Ricerca.get_argomenti()">															<div data-ng-if = "$index <= 11">																<md-checkbox data-ng-checked="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_args_selected())" data-ng-click="ctrl.Ricerca.toggle(item.code, ctrl.Ricerca.get_args_selected()); ctrl.Ricerca.getContents()" aria-label="{{item.name}}">																<label data-ng-bind-html="item.name"></label></md-checkbox>															</div>															<div data-ng-if = "$index >= 12">																									<div data-ng-show="showallarg">																	<md-checkbox data-ng-checked="ctrl.Ricerca.exists(item, ctrl.Ricerca.get_args_selected())" data-ng-click="ctrl.Ricerca.toggle(item, ctrl.Ricerca.get_args_selected()); ctrl.Ricerca.getContents()" aria-label="{{item.name}}">																	<label data-ng-bind-html="item.name"></label></md-checkbox>																</div>															</div>														</div>														<a href="" data-ng-hide="showallarg" data-ng-click="showallarg=true"><b><@wp.i18n key="CAGLIARI_PORTAL_SHOW_ALL" /></b></a>														<a href="" data-ng-show="showallarg" data-ng-click="showallarg=false"><b><@wp.i18n key="CAGLIARI_PORTAL_SHOW_LESS" /></b></a>													</div>												</div>											</div>										</div>										<div role="tabpanel" data-ng-class="ctrl.Ricerca.get_opzioniTab() == ''active''? ''tab-pane active'' : ''tab-pane''" id="opzioniTab">											<div class="row">												<div class="offset-lg-1 col-lg-4 col-md-6 col-sm-12">													<div class="form-check form-check-group">														<div class="toggles">															<label for="active_chk">																<@wp.i18n key="CAGLIARI_PORTAL_SEARCH_ONLY_ACTIVE_CONTENT" />																<input type="checkbox" id="active_chk" data-ng-model="ctrl.Ricerca.activeChk">																<span class="lever"></span>															</label>														</div>													</div>													<p class="small"><@wp.i18n key="CAGLIARI_PORTAL_SEARCH_ONLY_ACTIVE_CONTENT_INFO" /></p>												</div>												<div class="col-md-3 col-sm-12 search-filter-dategroup">													<div class="form-group">														<label for="datepicker_start"><@wp.i18n key="CAGLIARI_PORTAL_START_DATE" /></label>														<input type="text" class="form-control" id="datepicker_start" data-ng-model="ctrl.Ricerca.dataInizio" placeholder="gg/mm/aaaa" />														<button aria-label="<@wp.i18n key="CAGLIARI_PORTAL_OPEN_CALENDAR" />" type="button" class="ico-sufix" onclick="$(''#datepicker_start'').focus();" onkeypress="$(''#datepicker_start'').focus();"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-event"></use></svg></button>													</div>												</div>												<div class="col-md-3 col-sm-12 search-filter-dategroup">													<div class="form-group">														<label for="datepicker_end"><@wp.i18n key="CAGLIARI_PORTAL_END_DATE" /></label>														<input type="text" class="form-control" id="datepicker_end" data-ng-model="ctrl.Ricerca.dataFine" placeholder="gg/mm/aaaa" />														<button aria-label="<@wp.i18n key="CAGLIARI_PORTAL_OPEN_CALENDAR" />" type="button" class="ico-sufix" onclick="$(''#datepicker_end'').focus();" onkeypress="$(''#datepicker_end'').focus();"><svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-event"></use></svg></button>													</div>												</div>											</div>										</div>									</div>								</div>							</div>						</div>					</form>				</div>			</div>		</div>	</div></div>',0);

INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_GOTO','en','Go to the page:');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_GOTO','it','Vai alla pagina:');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_ACCESS_PORTAL','en','Access to the web portal');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_ACCESS_PORTAL','it','Accesso al portale');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_ACCESS_TITLE','en','Access to online services');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_ACCESS_TITLE','it','Accesso ai servizi online');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_ACCESS_USERNAME_PLACEHOLDER','en','enter your username');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_ACCESS_USERNAME_PLACEHOLDER','it','inserisci il tuo username');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_ACCESS_PASSWORD_PLACEHOLDER','en','enter the password');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_ACCESS_PASSWORD_PLACEHOLDER','it','inserisci la password');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_MENU_TITLE','en','Main menu');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_MENU_TITLE','it','Menu principale');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_INTESTAZIONE_SOCIAL_FB','en','Follow us on Facebook');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_INTESTAZIONE_SOCIAL_FB','it','Seguici su Facebook');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_INTESTAZIONE_SOCIAL_TW','en','Follow us on Twitter');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_INTESTAZIONE_SOCIAL_TW','it','Seguici su Twitter');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_INTESTAZIONE_SOCIAL_MD','en','Follow us on Medium');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_INTESTAZIONE_SOCIAL_MD','it','Seguici su Medium');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_INTESTAZIONE_IMGALT','en','Logo of the Municipality of Cagliari');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_INTESTAZIONE_IMGALT','it','Logo del Comune di Cagliari');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_INTESTAZIONE_TEXT','en','Municipality of Cagliari');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_INTESTAZIONE_TEXT','it','Comune di Cagliari');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_INTESTAZIONE_SEARCH','en','Search');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_INTESTAZIONE_SEARCH','it','Cerca');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_LINKUTILI_TITLE','en','Section Links');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_LINKUTILI_TITLE','it','Sezione Link Utili');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_INTESTAZIONE_SOCIAL','en','Follow us');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_INTESTAZIONE_SOCIAL','it','Seguici su');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_BACKTOTOP','en','Torna su');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_BACKTOTOP','it','Torna su');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_BROWSERUPDATE','en','È necessario aggiornare il browser');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_BROWSERUPDATE','it','È necessario aggiornare il browser');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_SKIPLINK_GOFOOTER','en','Vai al footer');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_SKIPLINK_GOFOOTER','it','Vai al footer');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_SKIPLINK_GONAVIGATION','en','Vai al menu di navigazione');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_SKIPLINK_GONAVIGATION','it','Vai al menu di navigazione');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_SKIPLINK_GOCONTENT','en','Vai ai contenuti');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_SKIPLINK_GOCONTENT','it','Vai ai contenuti');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_PREHEADER_LOGIN','en','Accedi all’area personale');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_PREHEADER_LOGIN','it','Accedi all’area personale');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_SEARCH_TITLE','en','Search');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_SEARCH_TITLE','it','Cerca');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_FILTER_TITLE','en','Filtri');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_FILTER_TITLE','it','Filtri');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_LOGIN_SPID','en','Login with SPID');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_LOGIN_SPID','it','Entra con SPID');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_VEDIAZIONI','it','Vedi azioni');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_SEEACTION','it','Vedi azioni');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_THECONTENT','it','il contenuto');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_DOWNLOAD','it','Scarica');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRINT','it','Stampa');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_LISTEN','it','Ascolta');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_SEND','it','Invia');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_SHAREMENU','it','Menu Condividi');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_SHAREON','it','Condividi su');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_GOTOARGUMENT','it','Vai all''argomento');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_DATE','it','Data');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_READINGTIME','it','Tempo di lettura');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_SHARE','it','Condividi');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_GOTOPARAGRAPH','it','Vai al paragrafo');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_ALLNEWS','it','Tutte le novità');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_GOTOARGUMENT','it','Vai all''argomento');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_GOTONEWS','it','Vai alla notizia');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_GOTOPAGE','it','Vai alla pagina');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_GALLERY','it','Galleria');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_ATTACHMENTS','it','Allegati');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_THEFILE','it','il file');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_RELATED','it','Contenuti correlati');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_MOREINFO','it','Ulteriori informazioni');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_LASTUPDATE','it','Ultimo aggiornamento');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_SEEPREVIOUSVERSIONS','it','Consulta versioni precedenti');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_NEWS','it','Notizie');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_EVENTS','it','Eventi');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_NOARGUMENTS','it','Nessun argomento trovato');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_EXPLORE_ARGUMENT','it','Esplora argomento');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_PAGINATION_NAV','it','Navigazione i contenuti');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_SEEALL','it','Vedi tutti');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_MANAGEDBY','it','Questa pagina è gestita da');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PEOPLE','it','Persone');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_MANDATESTARTDATE','it','Data di inizio mandato');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_MANDATE','it','Mandato');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_DELEGATIONS','it','Deleghe');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_CONTACTS','it','Contatti');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_STAFF','it','Staff');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PARTOFAMMTRASPSECTION','it','Questa pagina fa parte della sezione Amministrazione trasparente');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_SKILLS','it','Competenze');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_SEE','it','Vedi');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_TIME&ADDRESS','it','Orario e indirizzo');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_STRUCTURE','it','Struttura');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_ARTICULATION','it','Articolazione');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_SERVICESAVAILABLE','it','Servizi disponibili');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_DEPENDON','it','Dipende da');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_GIUNTACONSIGLIO','it','Giunta e consiglio');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_ALLAMMINISTRAZIONE','it','Tutta l''amministrazione');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_AMMINISTRAZIONE','it','Amministrazione');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_ALLSERVIZI','it','Tutti i servizi');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_SERVIZI','it','Servizi');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_GOTOSITE','it','Vai al sito');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_EXTERNALSITE','it','Sito esterno');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_GOTOAPPOINTMENT','it','Vai all''appuntamento');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_EVENTLISTEMPTY','it','Nessun evento in programma');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_ARGUMENTS_OTHER','it','Altri argomenti');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_MANAGE_ARG','it','Questo argomento è gestito da');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_VISITSITE','it','Visita il sito');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_MANAGE_THEMES','it','Questo sito tematico è gestito da');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_SHOWMORE','it','Scopri di più');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_ALL','it','Tutto');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_PREVIOUSPAGE','it','Pagina precedente');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_NEXT PAGE','it','Pagina successiva');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_NEXTPAGE','it','Pagina successiva');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_INTESTAZIONE_SOCIAL_YT','en','Seguici su YouTube');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_INTESTAZIONE_SOCIAL_YT','it','Seguici su YouTube');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_INTESTAZIONE_SOCIAL_TG','en','Seguici su Telegram');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_INTESTAZIONE_SOCIAL_TG','it','Seguici su Telegram');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_NORELATED','it','Nessun contenuto correlato');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_NOVITA','it','Novità');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_TIPOLOGY','it','Tipologia');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_EXPIRING','it','In scadenza');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_INPROGRESS','it','In corso');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_WITHFINALRESULT','it','Con esito definitivo');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_OBJECT','it','Oggetto');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_ENDDATE','it','Data scadenza');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_TYPE','it','Tipologia');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_COMMUNICATION','en','Comunicazioni');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_COMMUNICATION','it','Comunicazioni');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_PREHEADER_LINKTITLE','en','Collegamento a sito esterno - Sito della Regione Sardegna - nuova finestra');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_PREHEADER_LINKTITLE','it','Collegamento a sito esterno - Sito della Regione Sardegna - nuova finestra');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_EXT_SITE','en','Collegamento a sito esterno');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_EXT_SITE','it','Collegamento a sito esterno');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_NEW_WIN','en','nuova finestra');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_NEW_WIN','it','nuova finestra');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_READINGTIME_MIN','it','Minuti');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_TURISMO_LINKTITLE','en','Collegamento a sito esterno - Sito CagliariTurismo - nuova finestra');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_PHONE','it','Telefono');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_WRITEEMAIL','it','Scrivi una email');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_WRITEEMAILTO','it','Scrivi una email a');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_RATING_stelle','en','Valuta');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_RATING_stelle','it','Valuta');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_TURISMO_LINKTITLE','it','Collegamento a sito esterno - Sito CagliariTurismo - nuova finestra');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_SPORT_LINKTITLE','en','Collegamento a sito esterno - Sito CagliariSportiva - nuova finestra');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_SPORT_LINKTITLE','it','Collegamento a sito esterno - Sito CagliariSportiva - nuova finestra');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_DEADLINES','it','Scadenze');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_MUNICIPALCOUNCILS','it','Consigli comunali');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_RESERVEDAREA_MESSAGES','en','Messaggi');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_RESERVEDAREA_MESSAGES','it','Messaggi');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_RESERVEDAREA_PAYMENTS','en','Pagamenti');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_RESERVEDAREA_PAYMENTS','it','Pagamenti');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_RESERVEDAREA_DOCUMENTS','en','Documenti');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_RESERVEDAREA_DOCUMENTS','it','Documenti');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_RESERVEDAREA_PREFERENCES','en','Preferenze');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_RESERVEDAREA_PREFERENCES','it','Preferenze');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_RESERVEDAREA_PROFILE','en','Profilo');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_RESERVEDAREA_PROFILE','it','Profilo');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_RESERVEDAREA_LOGOUT','en','Esci');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_RESERVEDAREA_LOGOUT','it','Esci');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_GOTOMAP','it','Vai alla mappa');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_EMAIL','en','Email');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_EMAIL','it','Email');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_RATING_send','en','Invia valutazione');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_RATING_send','it','Invia valutazione');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_RATING_title','en','Recensione');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_RATING_title','it','Recensione');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_RATING_stelle_su','en','stelle su');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_RATING_stelle_su','it','stelle su');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_ARGUMENTS','en','Argomenti');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_ARGUMENTS','it','Argomenti');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_GOTOEVENTS','en','Vai all''evento');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_GOTOEVENTS','it','Vai all''evento');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_FILTER_BY','en','Filtra per');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_PEC','en','PEC');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_PEC','it','PEC');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRESSRELEASES','en','Comunicati stampa');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PRESSRELEASES','it','Comunicati stampa');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_LOGO_PONMETRO','en','Logo PON Metro');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_LOGO_PONMETRO','it','Logo PON Metro');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_WEBSITE','en','Sito web');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_WEBSITE','it','Sito web');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_MIGHT_INTERESTED','en','Potrebbero interessarti');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_MIGHT_INTERESTED','it','Potrebbero interessarti');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PAGE_INDEX','en','Indice della pagina');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PAGE_INDEX','it','Indice della pagina');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_ARGUMENTS_MOST','en','Più consultati');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_ARGUMENTS_MOST','it','Più consultati');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_ALLARGUMENTS','en','Tutti gli argomenti');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_ALLARGUMENTS','it','Tutti gli argomenti');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_ALLNOVITA','en','Tutte le novità');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_ALLNOVITA','it','Tutte le novità');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_PROTEZIONE_CIVILE','en','Regione Sardegna - Protezione civile');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_PROTEZIONE_CIVILE','it','Regione Sardegna - Protezione civile');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_READMORE','en','Leggi di più');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_READMORE','it','Leggi di più');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_EXPAND','en','Espandi');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_EXPAND','it','Espandi');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_CLOSE','en','Chiudi');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_CLOSE','it','Chiudi');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_RSS_TITLE','en','Tutti i feed RSS');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_RSS_TITLE','it','Tutti i feed RSS');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_GOTO_FEED','en','Vai al feed');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_GOTO_FEED','it','Vai al feed');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_ACTIVE_CONTENT','en','Contenuti attivi');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_ACTIVE_CONTENT','it','Contenuti attivi');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_ALLOPTIONS','en','Tutte le opzioni');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_ALLOPTIONS','it','Tutte le opzioni');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_FILTER_BY','it','Filtra per');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_CLOSE_SEARCH_FILTERS','en','Chiudi filtri di ricerca');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_CLOSE_SEARCH_FILTERS','it','Chiudi filtri di ricerca');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_OPEN_CALENDAR','en','Apri calendario');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_OPEN_CALENDAR','it','Apri calendario');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_END_DATE','en','Data fine');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_END_DATE','it','Data fine');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_START_DATE','en','Data inizio');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_START_DATE','it','Data inizio');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_SEARCH_ONLY_ACTIVE_CONTENT_INFO','en','Verranno esclusi dalla ricerca i contenuti archiviati e non più validi come gli eventi terminati o i bandi scaduti.');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_SEARCH_ONLY_ACTIVE_CONTENT_INFO','it','Verranno esclusi dalla ricerca i contenuti archiviati e non più validi come gli eventi terminati o i bandi scaduti.');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_SEARCH_ONLY_ACTIVE_CONTENT','en','Cerca solo tra i contenuti attivi');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_SEARCH_ONLY_ACTIVE_CONTENT','it','Cerca solo tra i contenuti attivi');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_SEARCH_THROUGHOUT_SITE','en','Cerca in tutto il sito');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_SEARCH_THROUGHOUT_SITE','it','Cerca in tutto il sito');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_POPULAR_SEARCHES','en','Ricerche frequenti');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_POPULAR_SEARCHES','it','Ricerche frequenti');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_TIPS','en','Suggerimenti');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_TIPS','it','Suggerimenti');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_UNTIL','en','Fino al');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_UNTIL','it','Fino al');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_TO','en','al');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_TO','it','al');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_FROM','en','Dal');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_FROM','it','Dal');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_SEARCH_INFORMATION_PEOPLE_SERVICES','en','Cerca informazioni, persone, servizi');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_SEARCH_INFORMATION_PEOPLE_SERVICES','it','Cerca informazioni, persone, servizi');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_DESCENDING_TITLE','en','Titolo decrescente');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_DESCENDING_TITLE','it','Titolo decrescente');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_INCREASING_TITLE','en','Titolo crescente');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_INCREASING_TITLE','it','Titolo crescente');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_PUBLICATION_DATE','en','Data pubblicazione');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_PUBLICATION_DATE','it','Data pubblicazione');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_CHOOSE_OPTION','en','Scegli una opzione');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_CHOOSE_OPTION','it','Scegli una opzione');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_POPULARITY','en','Popolarità');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_POPULARITY','it','Popolarità');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_SORT_BY','en','Ordina per');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_SORT_BY','it','Ordina per');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_SHOW_LESS','en','Mostra meno');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_SHOW_LESS','it','Mostra meno');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_SHOW_ALL','en','Mostra tutto');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_SHOW_ALL','it','Mostra tutto');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_OPTIONS','en','Opzioni');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_OPTIONS','it','Opzioni');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_CATEGORIES','en','Categorie');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_CATEGORIES','it','Categorie');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_CONFIRM','en','Conferma');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_CONFIRM','it','Conferma');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_RESULTS','en','risultati');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_RESULTS','it','risultati');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_FOUND','en','Trovati');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_FOUND','it','Trovati');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_OTHERSECTION','en','Altre sezioni');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_OTHERSECTION','it','Altre sezioni');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_WIDGET_SEARCH_RESULT_TITLE','en','La ricerca ha restituito');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_WIDGET_SEARCH_RESULT_TITLE','it','La ricerca ha restituito');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_WIDGET_SEARCH_RESULT_RESULTS','en','risultati');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_WIDGET_SEARCH_RESULT_RESULTS','it','risultati');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_LOADING','en','Caricamento');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_LOADING','it','Caricamento');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_SEARCH_RESULT','en','Risultato della ricerca');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_SEARCH_RESULT','it','Risultato della ricerca');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_RESERVEDAREA','en','Area personale');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_RESERVEDAREA','it','Area personale');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_SEARCH_RESULT_PAGINATION','en','Navigazione tra i risultati della ricerca');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_SEARCH_RESULT_PAGINATION','it','Navigazione tra i risultati della ricerca');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_NOELEMENTS','en','Nessun elemento trovato');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_NOELEMENTS','it','Nessun elemento trovato');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_TYPES','en','Tipologie');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_TYPES','it','Tipologie');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_INEVIDENCE','en','In evidenza');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_INEVIDENCE','it','In evidenza');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_SEEPAGE','en','comune.cagliari.it');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_SEEPAGE','it','comune.cagliari.it');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_TABLE_PRS_AREA','en','Area');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_LOGO_UE','en','Logo Unione Europea');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_LOGO_UE','it','Logo Unione Europea');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_LOGO_REPUBBLICA','en','Logo Repubblica Italiana');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_LOGO_REPUBBLICA','it','Logo Repubblica Italiana');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_SITO_PONMETRO','en','Sito PON Metro');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_SITO_PONMETRO','it','Sito PON Metro');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_TXT_PROGETTO','en','“Progetto cofinanziato dall’Unione europea - Fondi Strutturali e di Investimento Europei | Programma Operativo Città Metropolitane 2014-2020”');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_TXT_PROGETTO','it','“Progetto cofinanziato dall’Unione europea - Fondi Strutturali e di Investimento Europei | Programma Operativo Città Metropolitane 2014-2020”');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_TABLE_PRS_NOMINATIVO','en','Nominativo');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_TABLE_PRS_NOMINATIVO','it','Nominativo');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_TABLE_PRS_AREA','it','Area');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_TABLE_PRS_CARICA','en','Carica');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_TABLE_PRS_CARICA','it','Carica');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_OPEN_ACCORDION','en','Espandi voce');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_OPEN_ACCORDION','it','Espandi voce');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_FROMARGUMENTSEVIDENCE','en','da Argomenti in evidenza');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_FROMARGUMENTSEVIDENCE','it','da Argomenti in evidenza');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_CULTURE','en','Cultura');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_CULTURE','it','Cultura');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_TOURISM','en','Turismo');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_TOURISM','it','Turismo');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_WIDGET_HELP_TITLE','en','Hai bisogno di aiuto?');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_WIDGET_HELP_TITLE','it','Hai bisogno di aiuto?');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_WIDGET_AP_HELP_LINK_NUM_VERDE','en','Numero verde');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_WIDGET_AP_HELP_LINK_NUM_VERDE','it','Numero verde');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_WIDGET_HELP_DESC','en','Contatta il Comune per avere ulteriori informazioni sull’ufficio che stai cercando oppure per avere assistenza per una pratica o un servizio.');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_WIDGET_HELP_DESC','it','Contatta il Comune per avere ulteriori informazioni sull’ufficio che stai cercando oppure per avere assistenza per una pratica o un servizio.');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_WIDGET_HELP_CITTADINI','en','Per i cittadini');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_WIDGET_HELP_CITTADINI','it','Per i cittadini');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_WIDGET_HELP_PA','en','Per le PA');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_WIDGET_HELP_PA','it','Per le PA');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_WIDGET_HELP_COMPILA','en','Compila il modulo online:');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_WIDGET_HELP_COMPILA','it','Compila il modulo online:');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_ACCEPTS_COOKIES','en','Si, acconsento');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_SITO_PONMETRO_ARIA','en','Collegamento a sito esterno - Sito PON Metro - nuova finestra');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_SITO_PONMETRO_ARIA','it','Collegamento a sito esterno - Sito PON Metro - nuova finestra');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_SITO_REPUBBLICA','en','Sito Agenzia per la Coesione Territoriale');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_SITO_REPUBBLICA','it','Sito Agenzia per la Coesione Territoriale');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_SITO_REPUBBLICA_ARIA','en','Collegamento a sito esterno - Sito Agenzia per la Coesione Territoriale - nuova finestra');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_SITO_REPUBBLICA_ARIA','it','Collegamento a sito esterno - Sito Agenzia per la Coesione Territoriale - nuova finestra');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_SITO_UE','en','Sito Unione Europea');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_SITO_UE','it','Sito Unione Europea');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_SITO_UE_ARIA','en','Collegamento a sito esterno - Sito Unione Europea - nuova finestra');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_SITO_UE_ARIA','it','Collegamento a sito esterno - Sito Unione Europea - nuova finestra');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_TABLE_SEEALL','en','Vedi tutto');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_TABLE_SEEALL','it','Vedi tutto');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_TABLE_SEELESS','en','Vedi meno');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_TABLE_SEELESS','it','Vedi meno');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_TABLE_INC_ATTO','en','Atto');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_TABLE_INC_ATTO','it','Atto');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_TABLE_INC_OGGETTO','en','Oggetto');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_TABLE_INC_OGGETTO','it','Oggetto');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_TABLE_INC_COMPENSO','en','Compenso');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_TABLE_INC_COMPENSO','it','Compenso');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_TABLE_INC_DURATA','en','Durata');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_TABLE_INC_DURATA','it','Durata');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_TABLE_INC_PUB','en','Pubblicazione');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_TABLE_INC_PUB','it','Pubblicazione');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_TABLE_INC_SOGGETTO','en','Soggetto');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_TABLE_INC_SOGGETTO','it','Soggetto');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_TABLE_PRS_INCARICO','en','Incarico');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_TABLE_PRS_INCARICO','it','Incarico');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_TABLE_PRS_INIZIOCARICA','en','Inizio carica');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_TABLE_PRS_INIZIOCARICA','it','Inizio carica');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_TABLE_PRS_FINECARICA','en','Fine carica');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_TABLE_PRS_FINECARICA','it','Fine carica');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_SPORT','en','Sport');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_SPORT','it','Sport');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_ACCEPTS_COOKIES','it','Si, acconsento');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_DECLINES_COOKIES','en','No, non acconsento');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_DECLINES_COOKIES','it','No, non acconsento');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_COOKIES_QUESTION','en','Acconsenti all''utilizzo di cookie di terze parti?');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_COOKIES_QUESTION','it','Acconsenti all''utilizzo di cookie di terze parti?');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_PRIVACYPOLICY','en','privacy policy');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_PRIVACYPOLICY','it','privacy policy');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_WIDGET_HELP_PA_TITLE','en','Assistenza per le PA');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_WIDGET_HELP_PA_TITLE','it','Assistenza per le PA');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_WIDGET_HELP_CITTADINI_TITLE','en','Assistenza per i cittadini');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_WIDGET_HELP_CITTADINI_TITLE','it','Assistenza per i cittadini');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_COOKIES_TEXT','en','Questo sito utilizza cookie tecnici, analytics e di terze parti. Se non acconsenti all''utilizzo dei cookie di terze parti, alcune di queste funzionalità potrebbero essere non disponibili. Per maggiori informazioni consulta la');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_COOKIES_TEXT','it','Questo sito utilizza cookie tecnici, analytics e di terze parti. Se non acconsenti all''utilizzo dei cookie di terze parti, alcune di queste funzionalità potrebbero essere non disponibili. Per maggiori informazioni consulta la');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_OPEN_CALENDAR','it','Apri calendario');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_OPEN_CALENDAR','en','Apri calendario');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_PREHEADER_COVID19_ARIA','en','Coronavirus  (Covid-19)');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_UNAUTHORIZED_RESIDENT','en','Attenzione, per accedere al servizio effettuare l''accesso con identificativo SPID da  -Accedi all''area personale-   Il servizio è riservato ai cittadini residenti nel comune di Cagliari.');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_UNAUTHORIZED_RESIDENT','it','Attenzione, per accedere al servizio effettuare l''accesso con identificativo SPID da  -Accedi all''area personale-   Il servizio è riservato ai cittadini residenti nel comune di Cagliari.');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_PREHEADER_COVID19_ARIA','it','Coronavirus  (Covid-19)');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_SELECT_ALL','it','Tutti');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_SELECT_ALL','en','All');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_NORESULTS','it','Nessun risultato trovato');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_NORESULTS','en','No result found');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_MESSAGE_LABEL','it','Messaggio');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_MESSAGE_LABEL','en','Message');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONFIRM_BUTTON','it','Conferma');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONFIRM_BUTTON','en','Confirm');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_SERVICE_ACCESS_ERROR','en','Errore di accesso al servizio');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_SERVICE_ACCESS_ERROR','it','Errore di accesso al servizio');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_SERVICE_ACCESS_ERROR_MESSAGE','en','Attenzione, non si possiedono i permessi necessari per accedere al servizio.');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_SERVICE_ACCESS_ERROR_MESSAGE','it','Attenzione, non si possiedono i permessi necessari per accedere al servizio.');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_SERVICE_ACCESS_ERROR_GOTO_LOGIN','en','Vai alla pagina di Login');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_SERVICE_ACCESS_ERROR_GOTO_LOGIN','it','Vai alla pagina di Login');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_IMAGE_ZOOM','en','Clicca per ingrandire');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_IMAGE_ZOOM','it','Clicca per ingrandire');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_INSERT','en','Insert');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_INSERT','it','Inserisci');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_UNAUTHORIZED_CITIZEN_TITLE','en','Servizio riservato');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_UNAUTHORIZED_CITIZEN_TITLE','it','Servizio riservato');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_UNAUTHORIZED_RESIDENT_TITLE','en','Servizio riservato');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_UNAUTHORIZED_RESIDENT_TITLE','it','Servizio riservato');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_AMMTRASP_TEXT','en','I dati personali pubblicati sono riutilizzabili solo ai sensi dell''articolo 7 del decreto legislativo 33/2013');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_AMMTRASP_TEXT','it','I dati personali pubblicati sono riutilizzabili solo ai sensi dell''articolo 7 del decreto legislativo 33/2013');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_CONTACT_TITLE','en','Contatti');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_CONTACT_TITLE','it','Contatti');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_CONTACT_ADDRESS','en','Via Roma 145 - 09124 Cagliari');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_CONTACT_ADDRESS','it','Via Roma 145 - 09124 Cagliari');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_CONTACT_CODFISC','en','Codice fiscale');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_CONTACT_CODFISC','it','Codice fiscale');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_CONTACT_PIVA','en','Partita IVA');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_CONTACT_PIVA','it','Partita IVA');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_CONTACT_PIVA_ABBR','en','P. IVA');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_CONTACT_PIVA_ABBR','it','P. IVA');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_CONTACT_PIVA_VAL','en','00147990923');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_CONTACT_PIVA_VAL','it','00147990923');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_CONTACT_IBAN_ACRO','en','International Bank Account Number - Numero di Conto Corrente Bancario Internazionale');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_CONTACT_IBAN_ACRO','it','International Bank Account Number - Numero di Conto Corrente Bancario Internazionale');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_CONTACT_IBAN_VAL','en','IT26S 01015 04800 000070687691');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_CONTACT_IBAN_VAL','it','IT26S 01015 04800 000070687691');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_CONTACT_URP','en','URP - Ufficio Relazioni con il Pubblico');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_CONTACT_URP','it','URP - Ufficio Relazioni con il Pubblico');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_CONTACT_CENTRAL','en','Centralino unico: 070 6771');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_CONTACT_CENTRAL','it','Centralino unico: 070 6771');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_CONTACT_GREENNUM','en','Numero verde: 800 016 058');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_CONTACT_GREENNUM','it','Numero verde: 800 016 058');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_CONTACT_FOLLOW','en','Seguici su');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_CONTACT_FOLLOW','it','Seguici su');

INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_OPTION','en','Option');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_OPTION','it','Opzioni');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_MORETHAN','en','più di');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_MORETHAN','it','più di');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_PREHEADER_OLDSITE','en','Vecchio sito');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_PREHEADER_OLDSITE','it','Vecchio sito');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_PREHEADER_OLDSITE_ARIA','en','Collegamento a sito esterno - Vecchio sito web - nuova finestra');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_PREHEADER_OLDSITE_ARIA','it','Collegamento a sito esterno - Vecchio sito web - nuova finestra');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_WIDGET_AP_HELP_LINK_TITLE','en','Manda una mail a direzionetecnica.web@comune.cagliari.it');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_WIDGET_AP_HELP_LINK_TITLE','it','Manda una mail a direzionetecnica.web@comune.cagliari.it');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_WIDGET_AP_HELP_DESC','en','Contatta il Comune per avere ulteriori informazioni dal servizio di Assistenza.');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_WIDGET_AP_HELP_DESC','it','Contatta il Comune per avere ulteriori informazioni dal servizio di Assistenza.');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_WIDGET_AP_INTRO','en','Hai effettuato regolarmente l''accesso e sei nella tua area personale. <br />Puoi accedere ai servizi riservati presenti nel portale.');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_WIDGET_AP_INTRO','it','Hai effettuato regolarmente l''accesso e sei nella tua area personale. <br />Puoi accedere ai servizi riservati presenti nel portale.');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_GOTOSERVICE','en','Accedi al servizio');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_GOTOSERVICE','it','Accedi al servizio');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_SERVICE_RESIDENT','en','Servizio residenti');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_SERVICE_RESIDENT','it','Servizio residenti');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_SERVICE_CITIZEN','en','Servizio');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_SERVICE_CITIZEN','it','Servizio');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_PREHEADER_TEXT_TITLE','en','Regione Autonoma della Sardegna');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_PREHEADER_TEXT_TITLE','it','Regione Autonoma della Sardegna');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_PREHEADER_TEXT','en','Regione <span class=""hidden-xs"">Autonoma della</span> Sardegna');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_PREHEADER_TEXT','it','Regione <span class=""hidden-xs"">Autonoma della</span> Sardegna');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_DOCUMENTI','en','Documenti e dati');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_DOCUMENTI','it','Documenti e dati');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_ALLDOCUMENTI','en','Tutti i documenti e dati');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_ALLDOCUMENTI','it','Tutti i documenti e dati');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_ALLAMMINISTRAZIONE_TRASPARENTE','en','Amministrazione Trasparente');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_ALLAMMINISTRAZIONE_TRASPARENTE','it','Amministrazione Trasparente');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_CONTACT_NUMBERS','en','Numeri utili');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_CONTACT_NUMBERS','it','Numeri utili');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_YOUAREIN','en','Ti trovi in');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_YOUAREIN','it','Ti trovi in');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_DOWNLOADFILE','en','Scarica il file');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_DOWNLOADFILE','it','Scarica il file');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_FILE','en','File');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_FILE','it','File');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_DATEESITO','en','Data esito');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_DATEESITO','it','Data esito');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PUBDATE','en','Data pubblicazione');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_PUBDATE','it','Data pubblicazione');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_DATESCAD','en','Data scadenza');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_CONTENT_DATESCAD','it','Data scadenza');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_GOTODOC','en','Vai al documento');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_GOTODOC','it','Vai al documento');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_INTESTAZIONE_SOCIAL_WA','en','Contattaci su WhatsApp');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_INTESTAZIONE_SOCIAL_WA','it','Contattaci su WhatsApp');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_CONTACT_WHTPP','en','SMS e WhatsApp: +39 123 4567890');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_FOOTER_CONTACT_WHTPP','it','SMS e WhatsApp: +39 123 4567890');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_TITLE','en','Comune di Cagliari');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_TITLE','it','Comune di Cagliari');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_DESCRIPTION','en','Institutional website');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_DESCRIPTION','it','Sito istituzionale');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_PREHEADER_COVID19','en','Coronavirus');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_HEADER_PREHEADER_COVID19','it','Coronavirus');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_UNAUTHORIZED_CITIZEN','en','Attenzione, per accedere al servizio effettuare l''accesso con identificativo SPID da -Accedi all''area personale- ');
INSERT INTO localstrings (keycode,langcode,stringvalue) VALUES ('CAGLIARI_PORTAL_UNAUTHORIZED_CITIZEN','it','Attenzione, per accedere al servizio effettuare l''accesso con identificativo SPID da -Accedi all''area personale- ');
