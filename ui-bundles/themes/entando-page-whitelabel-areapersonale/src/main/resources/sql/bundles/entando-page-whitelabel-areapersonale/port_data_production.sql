INSERT INTO pagemodels (code,descr,frames,plugincode,templategui) VALUES ('cagliari_reservedpage','Cagliari - Area personale','<?xml version="1.0" encoding="UTF-8"?>
<frames>
	<frame pos="0">
		<descr>Menu orizzontale</descr>
		<sketch x1="0" y1="0" x2="5" y2="0" />
		<defaultWidget code="cagliari_widget_header_horizontalmenu">
			<properties>
				<property key="navSpec">code(amministrazione) + code(servizi) + code(documenti)+code(aggiornamenti)</property>
			</properties>
		</defaultWidget>
	</frame>
	<frame pos="1">
		<descr>Menu utility</descr>
		<sketch x1="6" y1="0" x2="11" y2="0" />
		<defaultWidget code="cagliari_widget_menu_link_utili" />
	</frame>
	<frame pos="2" main="true">
		<descr>Contenuto principale</descr>
		<sketch x1="0" y1="1" x2="11" y2="1" />
	</frame>
	<frame pos="3">
		<descr>Altri contenuti 1</descr>
		<sketch x1="0" y1="2" x2="11" y2="2" />
	</frame>
	<frame pos="4">
		<descr>Altri contenuti 2</descr>
		<sketch x1="0" y1="3" x2="11" y2="3" />
	</frame>
	<frame pos="5">
		<descr>Altri contenuti con section</descr>
		<sketch x1="0" y1="4" x2="11" y2="4" />
	</frame>
	<frame pos="6">
		<descr>Altri contenuti senza section 1</descr>
		<sketch x1="0" y1="5" x2="11" y2="5" />
	</frame>
	<frame pos="7">
		<descr>Altri contenuti senza section 2</descr>
		<sketch x1="0" y1="6" x2="11" y2="6" />
	</frame>
	<frame pos="8">
		<descr>Altri contenuti senza section 3</descr>
		<sketch x1="0" y1="7" x2="11" y2="7" />
	</frame>
	<frame pos="9">
		<descr>Altri contenuti senza section 4</descr>
		<sketch x1="0" y1="8" x2="11" y2="8" />
	</frame>
	<frame pos="10">
		<descr>Footer</descr>
		<sketch x1="0" y1="9" x2="11" y2="9" />
		<defaultWidget code="cagliari_widget_footer_menu">
			<properties>
				<property key="navSpec">code(amministrazione).subtree(1) + code(servizi).subtree(1)+code(novita).subtree(1)+ code(documenti).subtree(1)</property>
			</properties>
		</defaultWidget>
	</frame>
</frames>

',NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<@wp.headInfo type="CSS_CA_INT" info="sezioni.min.css" />
<@wp.headInfo type="CSS_CA_INT" info="interne.min.css" />

<@wp.currentPage param="code" var="currentViewCode" />

<!doctype html>
	<@wp.fragment code="cagliari_template_head" escapeXml=false />
	<body class="push-body" data-ng-app="ponmetroca" data-ng-csp="no-inline-style">
	    <@wp.fragment code="cagliari_template_googletagmanager" escapeXml=false />
		<div class="body_wrapper push_container clearfix" id="page_top">
			<@wp.fragment code="cagliari_template_skiplink" escapeXml=false />
		
			<header id="mainheader" class="navbar-fixed-top bg-blu container-fullwidth">
				<@wp.fragment code="cagliari_template_header_preheader" escapeXml=false />
				<!-- Button Menu -->
 				<button class="navbar-toggle menu-btn pull-left menu-left push-body jPushMenuBtn">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar icon-bar1"></span>
					<span class="icon-bar icon-bar2"></span>
					<span class="icon-bar icon-bar3"></span>
				</button>
				<!--End Button Menu -->
 
 				<!-- Menu -->
 				<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="menup">
 					<div class="cbp-menu-wrapper clearfix">
 						<@c.import url="/WEB-INF/aps/jsp/models/inc/cagliari_template_header_navigation_menu.jsp" />
 						<ul class="utilitymobile">
 						    <@wp.show frame=1 />
 						</ul>
 						<ul class="list-inline socialmobile">
 							<@wp.fragment code="cagliari_template_header_social_mobile" escapeXml=false />
 						</ul>
 					</div>
 				</nav>
 				<!-- End Menu -->
 				
 				<@wp.fragment code="cagliari_template_menu_ap" escapeXml=false />
 				
				<@wp.fragment code="cagliari_template_header_intestazione" escapeXml=false />
				<section class="hidden-xs" id="sub_nav">
                    <h2 class="sr-only">Submenu</h2>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-md-8 pull-left text-left">
                                <ul class="sub_nav clearfix">
				                    <@wp.show frame=0 />
				                </ul>
				            </div>
				            <div class="col-lg-6 col-md-4 pull-right text-right">
				                <ul class="list_link-utili">
				                <@wp.show frame=1 />
				                </ul>
				            </div>
				        </div>
                    </div>
                </section>
			</header>
			<main id="main_container" class="areapersonale <#if (currentViewCode == ''ap'')>areapersonale-home</#if>">
			    <section class="intestazione bg-grigio">
			        <@wp.fragment code="cagliari_areapersonale-intestazione" escapeXml=false />
                </section>
                <@wp.show frame=2 />
                <@wp.show frame=3 />
                <@wp.show frame=4 />
                <section class="areapersonale-sezione">
	                <div class="container">
		                <@wp.show frame=5 />
		            </div>
                </section>
		        <@wp.show frame=6 />
		        <@wp.show frame=7 />
		        <@wp.show frame=8 />
		        <@wp.show frame=9 />
			</main>
			<footer id="footer">
				<div class="container">
					<@wp.fragment code="cagliari_template_footer_intestazione" escapeXml=false />
					<section class="lista-sezioni">
				        <div class="row">
					        <@wp.show frame=10 />
					    </div>
					</section>
				    <section class="lista-linkutili">
				        <div class="row">
					        <@wp.fragment code="cagliari_template_footer_info" escapeXml=false />
					    </div>
					</section>
					<@wp.fragment code="cagliari_template_footer_link" escapeXml=false />
				</div>
			</footer>
		</div>
		<@wp.fragment code="cagliari_template_backtotop" escapeXml=false />
		<@wp.fragment code="cagliari_template_footer_js" escapeXml=false />
	</body>
</html>');
INSERT INTO guifragment (code,widgettypecode,plugincode,gui,defaultgui,locked) VALUES ('cagliari_areapersonale-intestazione',NULL,NULL,'<#assign wp=JspTaglibs["/aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign uc=JspTaglibs["/comuneCagliari-whitelabel"]>
<#assign jpavatar=JspTaglibs["/jpavatar-apsadmin-core"]>

<@wp.currentPage param="code" var="currentViewCode" />
<@wp.currentUserProfileAttribute attributeRoleName="userprofile:firstname" var="utenteName" />
<@wp.currentUserProfileAttribute attributeRoleName="userprofile:sex" var="utenteSex" />

<div id="briciole">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-10 col-md-12">
				<nav class="breadcrumb-container" aria-label="breadcrumb">
					<ol class="breadcrumb">
						<@wp.freemarkerTemplateParameter var="currentViewCode" valueName="currentViewCode" />
						<#assign first=true />
						<#assign lastPageTitle="" />
						<@uc.cagliariPageInfoTag pageCode="${currentViewCode}" info="isPublishOnFly" var="isPublishOnFly"/>
						<@wp.nav spec="current.path" var="currentTarget">
							<#assign currentCode = currentTarget.code />
							<#if !currentTarget.voidPage>
								<#if currentCode == currentViewCode>
									<li class="breadcrumb-item active" aria-current="page"><a>${currentTarget.title}</a></li>
								<#else>
									<li class="breadcrumb-item"><a href="${currentTarget.url}" title="<@wp.i18n key="CAGLIARI_PORTAL_GOTOPAGE" /> ${currentTarget.title}">${currentTarget.title}</a><span class="separator">/</span></li>
									<#if (!first)>
										
									</#if>
								</#if>
							<#else>
								<li class="breadcrumb-item active" aria-current="page"><a>${currentTarget.title}</a></li>
							</#if>
							<#assign lastPageTitle = currentTarget.title />
						</@wp.nav>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</div>
<div class="areapersonale-menu">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="titolo-sezione">
					<h2>Bentornat<#if utenteSex?? && utenteSex == ''F''>a<#else>o</#if><#if utenteName??>, <strong>${utenteName}</strong></#if>!</h2>
					<div class="menu-profilo-ap">
						<a href="<@wp.url page="ap_profilo" />" title="<@wp.i18n key="CAGLIARI_PORTAL_GOTOPAGE" escapeXml=false />: <@wp.pageInfo pageCode="ap_profilo" info="title" />">
							<svg class="icon"><use xlink:href="<@wp.imgURL />ponmetroca.svg#ca-settings"></use></svg>
							<@wp.pageInfo pageCode="ap_profilo" info="title" />
						</a>
						|
						<a href="<@wp.info key="systemParam" paramName="applicationBaseURL" />do/logout.action" title="<@wp.i18n key="CAGLIARI_RESERVEDAREA_LOGOUT" escapeXml=false />">
							<svg class="icon"><use xlink:href="<@wp.imgURL />ponmetroca.svg#ca-exit"></use></svg>
							<@wp.i18n key="CAGLIARI_RESERVEDAREA_LOGOUT" escapeXml=false />
						</a>
					</div>
				</div>
				<div class="menu-sezione-ap">
					<ul class="sub-nav">
						<@wp.nav var="page" spec="code(ap).subtree(1)">
							<#if ((page.code != "ap") && (page.code != "ap_profilo") && (page.code != "ap_impostazioni"))>
								<li>
									<a href="<@wp.url page="${page.code}" />" class="list-item <#if (currentViewCode?contains(page.code))>current</#if>" title="<@wp.i18n key="CAGLIARI_PORTAL_GOTOPAGE" escapeXml=false /> ${page.title}">
										${page.title}
									</a>
								</li>
							</#if>
						</@wp.nav>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>',NULL,0);